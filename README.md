## SAP Business One (SAPB1) Bundle for Symfony2
It will probably work with newer versions of Symfony, but right now I'm working with Symfony 2.8.

This bundle assists with using Symfony to access your SAP Business One SQL data.

Due to how crazy-complex and large the database for SAPB1 is, this will be very slow-going to build so I'm starting with the tables I access the most. I've found that mapping every field in an entity uses a significant amount of memory so I will only be mapping the absolute minimum common fields (in other words, only the ones that I actually use) and I'm willing to add more as needed but to prevent memory issues, I want to keep the entity models using as few fields as possible. To save time, all the fields are actually mapped, but commented out so to make them work again is as simple as uncommenting the field and the getter, and changing the `/* **/ to /** **/` again. The only exception is query groups which will not be mapped at all. The best way to use a QryGroup field is to map them specially with a useful variable name.

SAPB1 has a lot of weird names, so I'll try to also include renames where needed, but I will keep the original field names so that if you actually spend a lot of time querying SAPB1 you will still know what the fields are called.

Due to the naming structure of the tables themselves, I will be trying to name the models something more human while still including the original table name in the entity. Example: ItemsOITM

You will need to extend the models for any tables you intend on using. Although this makes this a less "out of the box" solution, it will allow you to overwrite most anything I've done, and add your own UDFs to any table that have them.

### WARNING
Every model I'm creating is set as read only with a ~~private~~ protected** constructor and no setters. You should not ever, ever, EVER try to update SAPB1 directly through the database.
You should take additional steps to secure against accidentally writing to the database by using a read-only database user and configuring doctrine to ignore these tables when running schema update.

SAPB1 has its own way of doing things and you will probably void your warranty, or service contract, or something. Just don't do it.

** I originally set the constructor to private but it seems to be impossible to test that way. As it is, with a protected constructor, testing is a little hacky.

## Installation

Composer install directions deliberately left out. Will add in later when this is meant to be used.

### Enable the Bundle (Symfony 2.8)

Enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new BetaMFD\SAPBundle\BetaMFDSAPBundle(),
        );

        // ...
    }

    // ...
}
```

### Enable the Bundle In Symfony 3.4+

Sorry, no flex for this yet. Enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:
```php
    BetaMFD\SAPBundle\BetaMFDSAPBundle::class => ['all' => true],
```


### Using The Models

## Item Example
1. Create an entity for your Item Table. Mine is called OITM, but you can call it Item, ItemOITM, OITMItem, or whatever works for you.
1. Extend it from BetaMFD\SAPBundle\Model\ItemOITM

```
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BetaMFD\SAPBundle\Model\ItemOITM;

class OITM extends ItemOITM
{
  //put your custom code here!
}

```


If you need to connect an entity repository:

```

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OITMRepository")
 */
class OITM extends ItemOITM
{

```

## Using multiple models
I'm not sure at this time how to set of config in my bundle (or if I can?) to connect the models automatically. However, if you have many UDFs you will need to do this no matter what.

Will update as I develop and learn more. 

```
# app/config/config.yml

doctrine:
    orm:
        resolve_target_entities:
            BetaMFD\SAPBundle\Model\ItemOITMInterface: AppBundle\Entity\OITM
            #BetaMFD\SAPBundle\Model\ItemOITMInterface: BetaMFD\SAPBundle\Model\ItemOITM #to NOT use your custom class
            BetaMFD\SAPBundle\Model\BusinessPartnerOCRDInterface: AppBundle\Entity\OCRD

```


## Current Models

* BetaMFD\SAPBundle\Model\ItemOITM
* BetaMFD\SAPBundle\Model\BusinessPartnerOCRD
