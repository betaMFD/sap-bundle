<?php

namespace BetaMFD\SAPBundle\Tests\Model;

use BetaMFD\SAPBundle\Model\ItemOITM;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

use DateTime;
class ItemOITMTest extends TestCase
{
    public function testToString()
    {
        $oitm = itemMockA::getMock();
        $this->assertEquals('newItem', $oitm->__toString());
    }

    public function testIsCommitted()
    {
        $oitm = itemMockA::getMock();
        $this->assertEquals('Y', $oitm->getIsCommitted());
        $this->assertTrue($oitm->isCommitted());

        $oitm = itemMockB::getMock();
        $this->assertEquals('N', $oitm->getIsCommitted());
        $this->assertFalse($oitm->isCommitted());
    }

    public function testValid()
    {
        $oitm = itemMockA::getMock();
        $this->assertTrue($oitm->isValid());
        $this->assertTrue($oitm->isActive());

        $oitm = itemMockB::getMock();
        $this->assertFalse($oitm->isValid());
        $this->assertFalse($oitm->isActive());
    }

}

class itemMockA extends ItemOITM
{
    static public function getMock()
    {
        $oitm = new self;
        $oitm->validFor = 'Y';
        $oitm->itemCode = 'newItem';
        $oitm->isCommited = 'Y';
        return $oitm;
    }
}

class itemMockB extends ItemOITM
{
    static public function getMock()
    {
        $oitm = new self;
        $oitm->itemCode = 'newItem';
        $oitm->validFor = 'N';
        $oitm->isCommited = 'N';
        return $oitm;
    }
}
