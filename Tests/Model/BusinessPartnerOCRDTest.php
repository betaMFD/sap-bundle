<?php

namespace BetaMFD\SAPBundle\Tests\Model;

use BetaMFD\SAPBundle\Model\BusinessPartnerOCRD;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class BusinessPartnerOCRDTest extends TestCase
{
    public function testIsCustomer()
    {
        $ocrd = partnerMockA::getMock();
        $this->assertTrue($ocrd->isCustomer());
        $this->assertFalse($ocrd->isSupplier());
        $this->assertFalse($ocrd->isVendor());
    }

    public function testIsVendor()
    {
        $ocrd = partnerMockB::getMock();
        $this->assertFalse($ocrd->isCustomer());
        $this->assertTrue($ocrd->isSupplier());
        $this->assertTrue($ocrd->isVendor());
    }

    public function testGetAmountOverCreditLimit()
    {
        $ocrd = partnerMockA::getMock();
        $this->assertEquals('4000.000000', $ocrd->getAmountOverCreditLimit());
        $ocrd = partnerMockB::getMock();
        $this->assertEquals('0.000000', $ocrd->getAmountOverCreditLimit());
    }

}

class partnerMockA extends BusinessPartnerOCRD
{
    static public function getMock()
    {
        $ocrd = new self;
        $ocrd->cardType = 'C';
        $ocrd->balance = '5000.00';
        $ocrd->creditLine = '1000.00';
        return $ocrd;
    }
}

class partnerMockB extends BusinessPartnerOCRD
{
    static public function getMock()
    {
        $ocrd = new self;
        $ocrd->cardType = 'S';
        $ocrd->balance = '500.00';
        $ocrd->creditLine = '1000.00';
        return $ocrd;
    }
}
