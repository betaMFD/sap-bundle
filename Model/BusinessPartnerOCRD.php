<?php

namespace BetaMFD\SAPBundle\Model;

use BetaMFD\SAPBundle\Model\BusinessPartnerOCRDInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="OCRD")
 * @ORM\Entity(readOnly=true)
 */
abstract class BusinessPartnerOCRD implements BusinessPartnerOCRDInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="CardCode", type="string", length=15)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $cardCode;

    /**
     * @var string
     *
     * @ORM\Column(name="CardName", type="string", length=100, nullable=true)
     */
    protected $cardName;

    /**
     * @var string
     *
     * @ORM\Column(name="CardType", type="string", length=1, nullable=true)
     */
    protected $cardType;

    /**
     * @var integer
     *
     * @ORM\Column(name="GroupCode", type="integer", nullable=true)
     */
    protected $groupCode;

    /*
     * @var string
     *
     * @ORM\Column(name="Cmpprotected", type="string", length=1, nullable=true)
     */
    //protected $cmpprotected;

    /**
     * @var string
     *
     * @ORM\Column(name="Address", type="string", length=100, nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="ZipCode", type="string", length=20, nullable=true)
     */
    protected $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="MailAddres", type="string", length=100, nullable=true)
     */
    protected $mailAddres;

    /**
     * @var string
     *
     * @ORM\Column(name="MailZipCod", type="string", length=20, nullable=true)
     */
    protected $mailZipCod;

    /**
     * @var string
     *
     * @ORM\Column(name="Phone1", type="string", length=20, nullable=true)
     */
    protected $phone1;

    /**
     * @var string
     *
     * @ORM\Column(name="Phone2", type="string", length=20, nullable=true)
     */
    protected $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="Fax", type="string", length=20, nullable=true)
     */
    protected $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="CntctPrsn", type="string", length=90, nullable=true)
     */
    protected $cntctPrsn;

    /**
     * @var string
     *
     * @ORM\Column(name="Notes", type="string", length=100, nullable=true)
     */
    protected $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="Balance", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $balance;

    /*
     * @var string
     *
     * @ORM\Column(name="ChecksBal", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $checksBal;

    /*
     * @var string
     *
     * @ORM\Column(name="DNotesBal", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $dNotesBal;

    /*
     * @var string
     *
     * @ORM\Column(name="OrdersBal", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $ordersBal;

    /**
     * @ORM\ManyToOne(targetEntity="BetaMFD\SAPBundle\Model\PaymentTermOCTGInterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="GroupNum", referencedColumnName="GroupNum")
     * })
     */
    protected $groupNum;

    /**
     * @var string
     *
     * @ORM\Column(name="CreditLine", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $creditLine;

    /*
     * @var string
     *
     * @ORM\Column(name="DebtLine", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $debtLine;

    /*
     * @var string
     *
     * @ORM\Column(name="Discount", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $discount;

    /*
     * @var string
     *
     * @ORM\Column(name="VatStatus", type="string", length=1, nullable=true)
     */
    //protected $vatStatus;

    /*
     * @var string
     *
     * @ORM\Column(name="LicTradNum", type="string", length=32, nullable=true)
     */
    //protected $licTradNum;

    /*
     * @var string
     *
     * @ORM\Column(name="DdctStatus", type="string", length=1, nullable=true)
     */
    //protected $ddctStatus;

    /*
     * @var string
     *
     * @ORM\Column(name="DdctPrcnt", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $ddctPrcnt;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="ValidUntil", type="datetime", nullable=true)
     */
    //protected $validUntil;

    /*
     * @var integer
     *
     * @ORM\Column(name="Chrctrstcs", type="integer", nullable=true)
     */
    //protected $chrctrstcs;

    /*
     * @var integer
     *
     * @ORM\Column(name="ExMatchNum", type="integer", nullable=true)
     */
    //protected $exMatchNum;

    /*
     * @var integer
     *
     * @ORM\Column(name="InMatchNum", type="integer", nullable=true)
     */
    //protected $inMatchNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="ListNum", type="integer", nullable=true)
     */
    protected $listNum;

    /*
     * @var string
     *
     * @ORM\Column(name="DNoteBalFC", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $dNoteBalFC;

    /*
     * @var string
     *
     * @ORM\Column(name="OrderBalFC", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $orderBalFC;

    /*
     * @var string
     *
     * @ORM\Column(name="DNoteBalSy", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $dNoteBalSy;

    /*
     * @var string
     *
     * @ORM\Column(name="OrderBalSy", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $orderBalSy;

    /*
     * @var string
     *
     * @ORM\Column(name="Transfered", type="string", length=1, nullable=true)
     */
    //protected $transfered;

    /*
     * @var string
     *
     * @ORM\Column(name="BalTrnsfrd", type="string", length=1, nullable=true)
     */
    //protected $balTrnsfrd;

    /*
     * @var string
     *
     * @ORM\Column(name="IntrstRate", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $intrstRate;

    /*
     * @var string
     *
     * @ORM\Column(name="Commission", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $commission;

    /*
     * @var integer
     *
     * @ORM\Column(name="CommGrCode", type="integer", nullable=true)
     */
    //protected $commGrCode;

    /*
     * @var string
     *
     * @ORM\Column(name="Free_Text", type="string", length=1073741823, nullable=true)
     */
    //protected $free_Text;

    /**
     * @ORM\ManyToOne(targetEntity="BetaMFD\SAPBundle\Model\SalesPersonOSLPInterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SlpCode", referencedColumnName="SlpCode")
     * })
     */
    protected $slpCode;

    /*
     * @var string
     *
     * @ORM\Column(name="PrevYearAc", type="string", length=1, nullable=true)
     */
    //protected $prevYearAc;

    /**
     * @var string
     *
     * @ORM\Column(name="Currency", type="string", length=3, nullable=true)
     */
    protected $currency;

    /*
     * @var string
     *
     * @ORM\Column(name="RateDifAct", type="string", length=15, nullable=true)
     */
    //protected $rateDifAct;

    /*
     * @var string
     *
     * @ORM\Column(name="BalanceSys", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $balanceSys;

    /*
     * @var string
     *
     * @ORM\Column(name="BalanceFC", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $balanceFC;

    /*
     * @var string
     *
     * @ORM\Column(name="Protected", type="string", length=1, nullable=true)
     */
    //protected $protected;

    /**
     * @var string
     *
     * @ORM\Column(name="Cellular", type="string", length=50, nullable=true)
     */
    protected $cellular;

    /*
     * @var integer
     *
     * @ORM\Column(name="AvrageLate", type="integer", nullable=true)
     */
    //protected $avrageLate;

    /**
     * @var string
     *
     * @ORM\Column(name="City", type="string", length=100, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="County", type="string", length=100, nullable=true)
     */
    protected $county;

    /**
     * @ORM\ManyToOne(targetEntity="BetaMFD\SAPBundle\Model\CountryOCRYInterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Country", referencedColumnName="Code")
     * })
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(name="MailCity", type="string", length=100, nullable=true)
     */
    protected $mailCity;

    /**
     * @var string
     *
     * @ORM\Column(name="MailCounty", type="string", length=100, nullable=true)
     */
    protected $mailCounty;

    /**
     * @ORM\ManyToOne(targetEntity="BetaMFD\SAPBundle\Model\CountryOCRYInterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MailCountr", referencedColumnName="Code")
     * })
     */
    protected $mailCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="E_Mail", type="string", length=100, nullable=true)
     */
    protected $email;

    /*
     * @var string
     *
     * @ORM\Column(name="Picture", type="string", length=200, nullable=true)
     */
    //protected $picture;

    /*
     * @var string
     *
     * @ORM\Column(name="DflAccount", type="string", length=50, nullable=true)
     */
    //protected $dflAccount;

    /*
     * @var string
     *
     * @ORM\Column(name="DflBranch", type="string", length=50, nullable=true)
     */
    //protected $dflBranch;

    /*
     * @var string
     *
     * @ORM\Column(name="BankCode", type="string", length=30, nullable=true)
     */
    //protected $bankCode;

    /*
     * @var string
     *
     * @ORM\Column(name="AddID", type="string", length=64, nullable=true)
     */
    //protected $addID;

    /*
     * @var string
     *
     * @ORM\Column(name="Pager", type="string", length=30, nullable=true)
     */
    //protected $pager;

    /*
     * @var string
     *
     * @ORM\Column(name="FatherCard", type="string", length=15, nullable=true)
     */
    //protected $fatherCard;

    /*
     * @var string
     *
     * @ORM\Column(name="CardFName", type="string", length=100, nullable=true)
     */
    //protected $cardFName;

    /*
     * @var string
     *
     * @ORM\Column(name="FatherType", type="string", length=1, nullable=true)
     */
    //protected $fatherType;

    /*
     * @var string
     *
     * @ORM\Column(name="DdctOffice", type="string", length=10, nullable=true)
     */
    //protected $ddctOffice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CreateDate", type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UpdateDate", type="datetime", nullable=true)
     */
    protected $updateDate;

    /*
     * @var string
     *
     * @ORM\Column(name="ExportCode", type="string", length=8, nullable=true)
     */
    //protected $exportCode;

    /*
     * @var integer
     *
     * @ORM\Column(name="DscntObjct", type="integer", nullable=true)
     */
    //protected $dscntObjct;

    /*
     * @var string
     *
     * @ORM\Column(name="DscntRel", type="string", length=1, nullable=true)
     */
    //protected $dscntRel;

    /*
     * @var integer
     *
     * @ORM\Column(name="SPGCounter", type="integer", nullable=true)
     */
    //protected $sPGCounter;

    /*
     * @var integer
     *
     * @ORM\Column(name="SPPCounter", type="integer", nullable=true)
     */
    //protected $sPPCounter;

    /*
     * @var string
     *
     * @ORM\Column(name="DdctFileNo", type="string", length=9, nullable=true)
     */
    //protected $ddctFileNo;

    /*
     * @var integer
     *
     * @ORM\Column(name="SCNCounter", type="integer", nullable=true)
     */
    //protected $sCNCounter;

    /*
     * @var string
     *
     * @ORM\Column(name="MinIntrst", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $minIntrst;

    /*
     * @var string
     *
     * @ORM\Column(name="DataSource", type="string", length=1, nullable=true)
     */
    //protected $dataSource;

    /*
     * @var integer
     *
     * @ORM\Column(name="OprCount", type="integer", nullable=true)
     */
    //protected $oprCount;

    /*
     * @var string
     *
     * @ORM\Column(name="ExemptNo", type="string", length=50, nullable=true)
     */
    //protected $exemptNo;

    /*
     * @var integer
     *
     * @ORM\Column(name="Priority", type="integer", nullable=true)
     */
    //protected $priority;

    /*
     * @var integer
     *
     * @ORM\Column(name="CreditCard", type="integer", nullable=true)
     */
    //protected $creditCard;

    /*
     * @var string
     *
     * @ORM\Column(name="CrCardNum", type="string", length=64, nullable=true)
     */
    //protected $crCardNum;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="CardValid", type="datetime", nullable=true)
     */
    //protected $cardValid;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign", type="integer", nullable=true)
     */
    //protected $userSign;

    /*
     * @var string
     *
     * @ORM\Column(name="LocMth", type="string", length=1, nullable=true)
     */
    //protected $locMth;

    /**
     * @var string
     *
     * @ORM\Column(name="validFor", type="string", length=1, nullable=true)
     */
    protected $validFor;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="validFrom", type="datetime", nullable=true)
     */
    //protected $validFrom;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="validTo", type="datetime", nullable=true)
     */
    //protected $validTo;

    /**
     * @var string
     *
     * @ORM\Column(name="frozenFor", type="string", length=1, nullable=true)
     */
    protected $frozenFor;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="frozenFrom", type="datetime", nullable=true)
     */
    //protected $frozenFrom;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="frozenTo", type="datetime", nullable=true)
     */
    //protected $frozenTo;

    /*
     * @var string
     *
     * @ORM\Column(name="sEmployed", type="string", length=1, nullable=true)
     */
    //protected $sEmployed;

    /*
     * @var integer
     *
     * @ORM\Column(name="MTHCounter", type="integer", nullable=true)
     */
    //protected $mTHCounter;

    /*
     * @var integer
     *
     * @ORM\Column(name="BNKCounter", type="integer", nullable=true)
     */
    //protected $bNKCounter;

    /*
     * @var integer
     *
     * @ORM\Column(name="DdgKey", type="integer", nullable=true)
     */
    //protected $ddgKey;

    /*
     * @var integer
     *
     * @ORM\Column(name="DdtKey", type="integer", nullable=true)
     */
    //protected $ddtKey;

    /*
     * @var string
     *
     * @ORM\Column(name="ValidComm", type="string", length=30, nullable=true)
     */
    //protected $validComm;

    /**
     * @var string
     *
     * @ORM\Column(name="FrozenComm", type="string", length=30, nullable=true)
     */
    protected $frozenComm;

    /*
     * @var string
     *
     * @ORM\Column(name="chainStore", type="string", length=1, nullable=true)
     */
    //protected $chainStore;

    /*
     * @var string
     *
     * @ORM\Column(name="DiscInRet", type="string", length=1, nullable=true)
     */
    //protected $discInRet;

    /**
     * @var string
     *
     * @ORM\Column(name="State1", type="string", length=3, nullable=true)
     */
    protected $state1;

    /**
     * @var string
     *
     * @ORM\Column(name="State2", type="string", length=3, nullable=true)
     */
    protected $state2;

    /*
     * @var string
     *
     * @ORM\Column(name="VatGroup", type="string", length=8, nullable=true)
     */
    //protected $vatGroup;

    /*
     * @var integer
     *
     * @ORM\Column(name="LogInstanc", type="integer", nullable=true)
     */
    //protected $logInstanc;

    /*
     * @var string
     *
     * @ORM\Column(name="ObjType", type="string", length=20, nullable=true)
     */
    //protected $objType;

    /*
     * @var string
     *
     * @ORM\Column(name="Indicator", type="string", length=2, nullable=true)
     */
    //protected $indicator;

    /*
     * @var integer
     *
     * @ORM\Column(name="ShipType", type="integer", nullable=true)
     */
    //protected $shipType;

    /*
     * @var string
     *
     * @ORM\Column(name="DebPayAcct", type="string", length=15, nullable=true)
     */
    //protected $debPayAcct;

    /**
     * @var string
     *
     * @ORM\Column(name="ShipToDef", type="string", length=50, nullable=true)
     */
    protected $shipToDef;

    /**
     * @var string
     *
     * @ORM\Column(name="Block", type="string", length=100, nullable=true)
     */
    protected $block;

    /**
     * @var string
     *
     * @ORM\Column(name="MailBlock", type="string", length=100, nullable=true)
     */
    protected $mailBlock;

    /*
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=32, nullable=true)
     */
    //protected $password;

    /*
     * @var string
     *
     * @ORM\Column(name="ECVatGroup", type="string", length=8, nullable=true)
     */
    //protected $eCVatGroup;

    /*
     * @var string
     *
     * @ORM\Column(name="Deleted", type="string", length=1, nullable=true)
     */
    //protected $deleted;

    /*
     * @var string
     *
     * @ORM\Column(name="IBAN", type="string", length=50, nullable=true)
     */
    //protected $iBAN;

    /*
     * @var integer
     *
     * @ORM\Column(name="DocEntry", type="integer", nullable=false)
     */
    //protected $docEntry;

    /*
     * @var integer
     *
     * @ORM\Column(name="FormCode", type="integer", nullable=true)
     */
    //protected $formCode;

    /*
     * @var string
     *
     * @ORM\Column(name="Box1099", type="string", length=20, nullable=true)
     */
    //protected $box1099;

    /**
     * @var string
     *
     * @ORM\Column(name="PymCode", type="string", length=15, nullable=true)
     */
    protected $paymentMethod;

    /*
     * @var string
     *
     * @ORM\Column(name="BackOrder", type="string", length=1, nullable=true)
     */
    //protected $backOrder;

    /*
     * @var string
     *
     * @ORM\Column(name="PartDelivr", type="string", length=1, nullable=true)
     */
    //protected $partDelivr;

    /*
     * @var integer
     *
     * @ORM\Column(name="DunnLevel", type="integer", nullable=true)
     */
    //protected $dunnLevel;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="DunnDate", type="datetime", nullable=true)
     */
    //protected $dunnDate;

    /*
     * @var string
     *
     * @ORM\Column(name="BlockDunn", type="string", length=1, nullable=true)
     */
    //protected $blockDunn;

    /*
     * @var string
     *
     * @ORM\Column(name="BankCountr", type="string", length=3, nullable=true)
     */
    //protected $bankCountr;

    /*
     * @var string
     *
     * @ORM\Column(name="CollecAuth", type="string", length=1, nullable=true)
     */
    //protected $collecAuth;

    /*
     * @var string
     *
     * @ORM\Column(name="DME", type="string", length=5, nullable=true)
     */
    //protected $dME;

    /*
     * @var string
     *
     * @ORM\Column(name="InstrucKey", type="string", length=30, nullable=true)
     */
    //protected $instrucKey;

    /*
     * @var string
     *
     * @ORM\Column(name="SinglePaym", type="string", length=1, nullable=true)
     */
    //protected $singlePaym;

    /*
     * @var string
     *
     * @ORM\Column(name="ISRBillId", type="string", length=9, nullable=true)
     */
    //protected $iSRBillId;

    /*
     * @var string
     *
     * @ORM\Column(name="PaymBlock", type="string", length=1, nullable=true)
     */
    //protected $paymBlock;

    /*
     * @var string
     *
     * @ORM\Column(name="RefDetails", type="string", length=20, nullable=true)
     */
    //protected $refDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="HouseBank", type="string", length=30, nullable=true)
     */
    protected $houseBank;

    /*
     * @var string
     *
     * @ORM\Column(name="OwnerIdNum", type="string", length=15, nullable=true)
     */
    //protected $ownerIdNum;

    /*
     * @var integer
     *
     * @ORM\Column(name="PyBlckDesc", type="integer", nullable=true)
     */
    //protected $pyBlckDesc;

    /*
     * @var string
     *
     * @ORM\Column(name="HousBnkCry", type="string", length=3, nullable=true)
     */
    //protected $housBnkCry;

    /*
     * @var string
     *
     * @ORM\Column(name="HousBnkAct", type="string", length=50, nullable=true)
     */
    //protected $housBnkAct;

    /*
     * @var string
     *
     * @ORM\Column(name="HousBnkBrn", type="string", length=50, nullable=true)
     */
    //protected $housBnkBrn;

    /*
     * @var string
     *
     * @ORM\Column(name="ProjectCod", type="string", length=20, nullable=true)
     */
    //protected $projectCod;

    /*
     * @var integer
     *
     * @ORM\Column(name="SysMatchNo", type="integer", nullable=true)
     */
    //protected $sysMatchNo;

    /*
     * @var string
     *
     * @ORM\Column(name="VatIdUnCmp", type="string", length=32, nullable=true)
     */
    //protected $vatIdUnCmp;

    /*
     * @var string
     *
     * @ORM\Column(name="AgentCode", type="string", length=32, nullable=true)
     */
    //protected $agentCode;

    /*
     * @var integer
     *
     * @ORM\Column(name="TolrncDays", type="integer", nullable=true)
     */
    //protected $tolrncDays;

    /*
     * @var string
     *
     * @ORM\Column(name="SelfInvoic", type="string", length=1, nullable=true)
     */
    //protected $selfInvoic;

    /*
     * @var string
     *
     * @ORM\Column(name="DeferrTax", type="string", length=1, nullable=true)
     */
    //protected $deferrTax;

    /*
     * @var string
     *
     * @ORM\Column(name="LetterNum", type="string", length=20, nullable=true)
     */
    //protected $letterNum;

    /*
     * @var string
     *
     * @ORM\Column(name="MaxAmount", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $maxAmount;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="FromDate", type="datetime", nullable=true)
     */
    //protected $fromDate;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="ToDate", type="datetime", nullable=true)
     */
    //protected $toDate;

    /*
     * @var string
     *
     * @ORM\Column(name="WTLiable", type="string", length=1, nullable=true)
     */
    //protected $wTLiable;

    /*
     * @var string
     *
     * @ORM\Column(name="CrtfcateNO", type="string", length=20, nullable=true)
     */
    //protected $crtfcateNO;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="ExpireDate", type="datetime", nullable=true)
     */
    //protected $expireDate;

    /*
     * @var string
     *
     * @ORM\Column(name="NINum", type="string", length=20, nullable=true)
     */
    //protected $nINum;

    /*
     * @var string
     *
     * @ORM\Column(name="AccCritria", type="string", length=1, nullable=true)
     */
    //protected $accCritria;

    /*
     * @var string
     *
     * @ORM\Column(name="WTCode", type="string", length=4, nullable=true)
     */
    //protected $wTCode;

    /*
     * @var string
     *
     * @ORM\Column(name="Equ", type="string", length=1, nullable=true)
     */
    //protected $equ;

    /*
     * @var string
     *
     * @ORM\Column(name="HldCode", type="string", length=20, nullable=true)
     */
    //protected $hldCode;

    /*
     * @var string
     *
     * @ORM\Column(name="ConnBP", type="string", length=15, nullable=true)
     */
    //protected $connBP;

    /*
     * @var integer
     *
     * @ORM\Column(name="MltMthNum", type="integer", nullable=true)
     */
    //protected $mltMthNum;

    /*
     * @var string
     *
     * @ORM\Column(name="TypWTReprt", type="string", length=1, nullable=true)
     */
    //protected $typWTReprt;

    /*
     * @var string
     *
     * @ORM\Column(name="VATRegNum", type="string", length=32, nullable=true)
     */
    //protected $vATRegNum;

    /*
     * @var string
     *
     * @ORM\Column(name="RepName", type="string", length=15, nullable=true)
     */
    //protected $repName;

    /*
     * @var string
     *
     * @ORM\Column(name="Industry", type="string", length=1073741823, nullable=true)
     */
    //protected $industry;

    /*
     * @var string
     *
     * @ORM\Column(name="Business", type="string", length=1073741823, nullable=true)
     */
    //protected $business;

    /*
     * @var string
     *
     * @ORM\Column(name="WTTaxCat", type="string", length=1073741823, nullable=true)
     */
    //protected $wTTaxCat;

    /*
     * @var string
     *
     * @ORM\Column(name="IsDomestic", type="string", length=1, nullable=true)
     */
    //protected $isDomestic;

    /*
     * @var string
     *
     * @ORM\Column(name="IsResident", type="string", length=1, nullable=true)
     */
    //protected $isResident;

    /*
     * @var string
     *
     * @ORM\Column(name="AutoCalBCG", type="string", length=1, nullable=true)
     */
    //protected $autoCalBCG;

    /*
     * @var string
     *
     * @ORM\Column(name="OtrCtlAcct", type="string", length=15, nullable=true)
     */
    //protected $otrCtlAcct;

    /*
     * @var string
     *
     * @ORM\Column(name="AliasName", type="string", length=1073741823, nullable=true)
     */
    //protected $aliasName;

    /*
     * @var string
     *
     * @ORM\Column(name="Building", type="string", length=1073741823, nullable=true)
     */
    //protected $building;

    /*
     * @var string
     *
     * @ORM\Column(name="MailBuildi", type="string", length=1073741823, nullable=true)
     */
    //protected $mailBuildi;

    /*
     * @var string
     *
     * @ORM\Column(name="BoEPrsnt", type="string", length=15, nullable=true)
     */
    //protected $boEPrsnt;

    /*
     * @var string
     *
     * @ORM\Column(name="BoEDiscnt", type="string", length=15, nullable=true)
     */
    //protected $boEDiscnt;

    /*
     * @var string
     *
     * @ORM\Column(name="BoEOnClct", type="string", length=15, nullable=true)
     */
    //protected $boEOnClct;

    /*
     * @var string
     *
     * @ORM\Column(name="UnpaidBoE", type="string", length=15, nullable=true)
     */
    //protected $unpaidBoE;

    /*
     * @var string
     *
     * @ORM\Column(name="ITWTCode", type="string", length=4, nullable=true)
     */
    //protected $iTWTCode;

    /*
     * @var string
     *
     * @ORM\Column(name="DunTerm", type="string", length=25, nullable=true)
     */
    //protected $dunTerm;

    /*
     * @var string
     *
     * @ORM\Column(name="ChannlBP", type="string", length=15, nullable=true)
     */
    //protected $channlBP;

    /*
     * @var integer
     *
     * @ORM\Column(name="DfTcnician", type="integer", nullable=true)
     */
    //protected $dfTcnician;

    /*
     * @var integer
     *
     * @ORM\Column(name="Territory", type="integer", nullable=true)
     */
    //protected $territory;

    /**
     * @var string
     *
     * @ORM\Column(name="BillToDef", type="string", length=50, nullable=true)
     */
    protected $billToDef;

    /*
     * @var string
     *
     * @ORM\Column(name="DpmClear", type="string", length=15, nullable=true)
     */
    //protected $dpmClear;

    /**
     * @var string
     *
     * @ORM\Column(name="IntrntSite", type="string", length=100, nullable=true)
     */
    protected $intrntSite;

    /*
     * @var integer
     *
     * @ORM\Column(name="LangCode", type="integer", nullable=true)
     */
    //protected $langCode;

    /*
     * @var integer
     *
     * @ORM\Column(name="HousActKey", type="integer", nullable=true)
     */
    //protected $housActKey;

    /*
     * @var string
     *
     * @ORM\Column(name="Profession", type="string", length=50, nullable=true)
     */
    //protected $profession;

    /*
     * @var integer
     *
     * @ORM\Column(name="CDPNum", type="integer", nullable=true)
     */
    //protected $cDPNum;

    /*
     * @var integer
     *
     * @ORM\Column(name="DflBankKey", type="integer", nullable=true)
     */
    //protected $dflBankKey;

    /*
     * @var string
     *
     * @ORM\Column(name="BCACode", type="string", length=3, nullable=true)
     */
    //protected $bCACode;

    /*
     * @var string
     *
     * @ORM\Column(name="UseShpdGd", type="string", length=1, nullable=true)
     */
    //protected $useShpdGd;

    /*
     * @var string
     *
     * @ORM\Column(name="RegNum", type="string", length=32, nullable=true)
     */
    //protected $regNum;

    /*
     * @var string
     *
     * @ORM\Column(name="VerifNum", type="string", length=32, nullable=true)
     */
    //protected $verifNum;

    /*
     * @var string
     *
     * @ORM\Column(name="BankCtlKey", type="string", length=2, nullable=true)
     */
    //protected $bankCtlKey;

    /*
     * @var string
     *
     * @ORM\Column(name="HousCtlKey", type="string", length=2, nullable=true)
     */
    //protected $housCtlKey;

    /*
     * @var string
     *
     * @ORM\Column(name="AddrType", type="string", length=100, nullable=true)
     */
    //protected $addrType;

    /*
     * @var string
     *
     * @ORM\Column(name="InsurOp347", type="string", length=1, nullable=true)
     */
    //protected $insurOp347;

    /*
     * @var string
     *
     * @ORM\Column(name="MailAddrTy", type="string", length=100, nullable=true)
     */
    //protected $mailAddrTy;

    /*
     * @var string
     *
     * @ORM\Column(name="StreetNo", type="string", length=100, nullable=true)
     */
    //protected $streetNo;

    /*
     * @var string
     *
     * @ORM\Column(name="MailStrNo", type="string", length=100, nullable=true)
     */
    //protected $mailStrNo;

    /*
     * @var string
     *
     * @ORM\Column(name="TaxRndRule", type="string", length=1, nullable=true)
     */
    //protected $taxRndRule;

    /*
     * @var integer
     *
     * @ORM\Column(name="VendTID", type="integer", nullable=true)
     */
    //protected $vendTID;

    /*
     * @var string
     *
     * @ORM\Column(name="ThreshOver", type="string", length=1, nullable=true)
     */
    //protected $threshOver;

    /*
     * @var string
     *
     * @ORM\Column(name="SurOver", type="string", length=1, nullable=true)
     */
    //protected $surOver;

    /*
     * @var string
     *
     * @ORM\Column(name="VendorOcup", type="string", length=15, nullable=true)
     */
    //protected $vendorOcup;

    /*
     * @var string
     *
     * @ORM\Column(name="OpCode347", type="string", length=1, nullable=true)
     */
    //protected $opCode347;

    /*
     * @var string
     *
     * @ORM\Column(name="DpmIntAct", type="string", length=15, nullable=true)
     */
    //protected $dpmIntAct;

    /*
     * @var string
     *
     * @ORM\Column(name="ResidenNum", type="string", length=1, nullable=true)
     */
    //protected $residenNum;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign2", type="integer", nullable=true)
     */
    //protected $userSign2;

    /*
     * @var string
     *
     * @ORM\Column(name="PlngGroup", type="string", length=10, nullable=true)
     */
    //protected $plngGroup;

    /*
     * @var string
     *
     * @ORM\Column(name="VatIDNum", type="string", length=32, nullable=true)
     */
    //protected $vatIDNum;

    /*
     * @var string
     *
     * @ORM\Column(name="Affiliate", type="string", length=1, nullable=true)
     */
    //protected $affiliate;

    /*
     * @var string
     *
     * @ORM\Column(name="MivzExpSts", type="string", length=1, nullable=true)
     */
    //protected $mivzExpSts;

    /*
     * @var string
     *
     * @ORM\Column(name="HierchDdct", type="string", length=1, nullable=true)
     */
    //protected $hierchDdct;

    /*
     * @var string
     *
     * @ORM\Column(name="CertWHT", type="string", length=1, nullable=true)
     */
    //protected $certWHT;

    /*
     * @var string
     *
     * @ORM\Column(name="CertBKeep", type="string", length=1, nullable=true)
     */
    //protected $certBKeep;

    /*
     * @var string
     *
     * @ORM\Column(name="WHShaamGrp", type="string", length=1, nullable=true)
     */
    //protected $wHShaamGrp;

    /*
     * @var integer
     *
     * @ORM\Column(name="IndustryC", type="integer", nullable=true)
     */
    //protected $industryC;

    /*
     * @var integer
     *
     * @ORM\Column(name="DatevAcct", type="integer", nullable=true)
     */
    //protected $datevAcct;

    /*
     * @var string
     *
     * @ORM\Column(name="DatevFirst", type="string", length=1, nullable=true)
     */
    //protected $datevFirst;

    /*
     * @var string
     *
     * @ORM\Column(name="GTSRegNum", type="string", length=20, nullable=true)
     */
    //protected $gTSRegNum;

    /*
     * @var string
     *
     * @ORM\Column(name="GTSBankAct", type="string", length=80, nullable=true)
     */
    //protected $gTSBankAct;

    /*
     * @var string
     *
     * @ORM\Column(name="GTSBilAddr", type="string", length=80, nullable=true)
     */
    //protected $gTSBilAddr;

    /*
     * @var string
     *
     * @ORM\Column(name="HsBnkSwift", type="string", length=50, nullable=true)
     */
    //protected $hsBnkSwift;

    /*
     * @var string
     *
     * @ORM\Column(name="HsBnkIBAN", type="string", length=50, nullable=true)
     */
    //protected $hsBnkIBAN;

    /*
     * @var string
     *
     * @ORM\Column(name="DflSwift", type="string", length=50, nullable=true)
     */
    //protected $dflSwift;

    /*
     * @var string
     *
     * @ORM\Column(name="AutoPost", type="string", length=1, nullable=true)
     */
    //protected $autoPost;

    /*
     * @var string
     *
     * @ORM\Column(name="IntrAcc", type="string", length=15, nullable=true)
     */
    //protected $intrAcc;

    /*
     * @var string
     *
     * @ORM\Column(name="FeeAcc", type="string", length=15, nullable=true)
     */
    //protected $feeAcc;

    /*
     * @var integer
     *
     * @ORM\Column(name="CpnNo", type="integer", nullable=true)
     */
    //protected $cpnNo;

    /*
     * @var integer
     *
     * @ORM\Column(name="NTSWebSite", type="integer", nullable=true)
     */
    //protected $nTSWebSite;

    /*
     * @var string
     *
     * @ORM\Column(name="DflIBAN", type="string", length=50, nullable=true)
     */
    //protected $dflIBAN;

    /*
     * @var integer
     *
     * @ORM\Column(name="Series", type="integer", nullable=true)
     */
    //protected $series;

    /*
     * @var integer
     *
     * @ORM\Column(name="Number", type="integer", nullable=true)
     */
    //protected $number;

    /*
     * @var integer
     *
     * @ORM\Column(name="EDocExpFrm", type="integer", nullable=true)
     */
    //protected $eDocExpFrm;

    /*
     * @var string
     *
     * @ORM\Column(name="TaxIdIdent", type="string", length=1, nullable=true)
     */
    //protected $taxIdIdent;

    /*
     * @var string
     *
     * @ORM\Column(name="Attachment", type="string", length=1073741823, nullable=true)
     */
    //protected $attachment;

    /*
     * @var integer
     *
     * @ORM\Column(name="AtcEntry", type="integer", nullable=true)
     */
    //protected $atcEntry;

    /*
     * @var string
     *
     * @ORM\Column(name="DiscRel", type="string", length=1, nullable=true)
     */
    //protected $discRel;

    /*
     * @var string
     *
     * @ORM\Column(name="NoDiscount", type="string", length=1, nullable=true)
     */
    //protected $noDiscount;

    /*
     * @var string
     *
     * @ORM\Column(name="SCAdjust", type="string", length=1, nullable=true)
     */
    //protected $sCAdjust;

    /*
     * @var integer
     *
     * @ORM\Column(name="DflAgrmnt", type="integer", nullable=true)
     */
    //protected $dflAgrmnt;

    /*
     * @var string
     *
     * @ORM\Column(name="GlblLocNum", type="string", length=50, nullable=true)
     */
    //protected $glblLocNum;

    /*
     * @var string
     *
     * @ORM\Column(name="SenderID", type="string", length=50, nullable=true)
     */
    //protected $senderID;

    /*
     * @var string
     *
     * @ORM\Column(name="RcpntID", type="string", length=50, nullable=true)
     */
    //protected $rcpntID;

    /*
     * @var integer
     *
     * @ORM\Column(name="MainUsage", type="integer", nullable=true)
     */
    //protected $mainUsage;

    /*
     * @var string
     *
     * @ORM\Column(name="SefazCheck", type="string", length=1, nullable=true)
     */
    //protected $sefazCheck;

    /*
     * @var string
     *
     * @ORM\Column(name="SefazReply", type="string", length=254, nullable=true)
     */
    //protected $sefazReply;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="SefazDate", type="datetime", nullable=true)
     */
    //protected $sefazDate;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="DateFrom", type="datetime", nullable=true)
     */
    //protected $dateFrom;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="DateTill", type="datetime", nullable=true)
     */
    //protected $dateTill;

    /*
     * @var string
     *
     * @ORM\Column(name="RelCode", type="string", length=2, nullable=true)
     */
    //protected $relCode;

    /*
     * @var string
     *
     * @ORM\Column(name="OKATO", type="string", length=11, nullable=true)
     */
    //protected $oKATO;

    /*
     * @var string
     *
     * @ORM\Column(name="OKTMO", type="string", length=12, nullable=true)
     */
    //protected $oKTMO;

    /*
     * @var string
     *
     * @ORM\Column(name="KBKCode", type="string", length=20, nullable=true)
     */
    //protected $kBKCode;

    /*
     * @var string
     *
     * @ORM\Column(name="TypeOfOp", type="string", length=1, nullable=true)
     */
    //protected $typeOfOp;

    /*
     * @var integer
     *
     * @ORM\Column(name="OwnerCode", type="integer", nullable=true)
     */
    //protected $ownerCode;

    /*
     * @var string
     *
     * @ORM\Column(name="MandateID", type="string", length=35, nullable=true)
     */
    //protected $mandateID;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="SignDate", type="datetime", nullable=true)
     */
    //protected $signDate;

    /*
     * @var integer
     *
     * @ORM\Column(name="Remark1", type="integer", nullable=true)
     */
    //protected $remark1;

    /*
     * @var string
     *
     * @ORM\Column(name="ConCerti", type="string", length=20, nullable=true)
     */
    //protected $conCerti;

    /*
     * @var integer
     *
     * @ORM\Column(name="TpCusPres", type="integer", nullable=true)
     */
    //protected $tpCusPres;

    /*
     * @var string
     *
     * @ORM\Column(name="RoleTypCod", type="string", length=2, nullable=true)
     */
    //protected $roleTypCod;

    protected function __construct() {}

    /**
     * Get cardCode
     *
     * @return string
     */
    public function getCardCode()
    {
        return $this->cardCode;
    }

    /**
     * Get cardName
     *
     * @return string
     */
    public function getCardName()
    {
        return $this->cardName;
    }

    /**
     * Get cardType
     *
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    public function isCustomer()
    {
        return $this->getCardType() == 'C';
    }

    public function isSupplier()
    {
        return $this->getCardType() == 'S';
    }

    public function isVendor()
    {
        return $this->isSupplier();
    }

    /**
     * Get groupCode
     *
     * @return integer
     */
    public function getGroupCode()
    {
        return $this->groupCode;
    }

    /*
     * Get cmpprotected
     *
     * @return string
     */
    //public function getCmpprotected()
    //{
    //    return $this->cmpprotected;
    //}

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Get mailAddres
     *
     * @return string
     */
    public function getMailAddres()
    {
        return $this->mailAddres;
    }

    /**
     * Get mailZipCod
     *
     * @return string
     */
    public function getMailZipCod()
    {
        return $this->mailZipCod;
    }

    /**
     * Get phone1
     *
     * @return string
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get cntctPrsn
     *
     * @return string
     */
    public function getCntctPrsn()
    {
        return $this->cntctPrsn;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Get balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    public function getAmountOverCreditLimit()
    {
        $amount = bcsub($this->getBalance(), $this->getCreditLine(), 6);
        return $amount > 0 ? $amount : '0.000000';
    }

    /*
     * Get checksBal
     *
     * @return string
     */
    //public function getChecksBal()
    //{
    //    return $this->checksBal;
    //}

    /*
     * Get dNotesBal
     *
     * @return string
     */
    //public function getDNotesBal()
    //{
    //    return $this->dNotesBal;
    //}

    /*
     * Get ordersBal
     *
     * @return string
     */
    //public function getOrdersBal()
    //{
    //    return $this->ordersBal;
    //}

    /**
     * Get groupNum
     *
     * @return integer
     */
    public function getGroupNum()
    {
        return $this->groupNum;
    }

    /**
     * Get creditLine
     *
     * @return string
     */
    public function getCreditLine()
    {
        return $this->creditLine;
    }

    /*
     * Get debtLine
     *
     * @return string
     */
    //public function getDebtLine()
    //{
    //    return $this->debtLine;
    //}

    /*
     * Get discount
     *
     * @return string
     */
    //public function getDiscount()
    //{
    //    return $this->discount;
    //}

    /*
     * Get vatStatus
     *
     * @return string
     */
    //public function getVatStatus()
    //{
    //    return $this->vatStatus;
    //}

    /*
     * Get licTradNum
     *
     * @return string
     */
    //public function getLicTradNum()
    //{
    //    return $this->licTradNum;
    //}

    /*
     * Get ddctStatus
     *
     * @return string
     */
    //public function getDdctStatus()
    //{
    //    return $this->ddctStatus;
    //}

    /*
     * Get ddctPrcnt
     *
     * @return string
     */
    //public function getDdctPrcnt()
    //{
    //    return $this->ddctPrcnt;
    //}

    /*
     * Get validUntil
     *
     * @return \DateTime
     */
    //public function getValidUntil()
    //{
    //    return $this->validUntil;
    //}

    /*
     * Get chrctrstcs
     *
     * @return integer
     */
    //public function getChrctrstcs()
    //{
    //    return $this->chrctrstcs;
    //}

    /*
     * Get exMatchNum
     *
     * @return integer
     */
    //public function getExMatchNum()
    //{
    //    return $this->exMatchNum;
    //}

    /*
     * Get inMatchNum
     *
     * @return integer
     */
    //public function getInMatchNum()
    //{
    //    return $this->inMatchNum;
    //}

    /**
     * Get listNum
     *
     * @return integer
     */
    public function getListNum()
    {
        return $this->listNum;
    }

    /*
     * Get dNoteBalFC
     *
     * @return string
     */
    //public function getDNoteBalFC()
    //{
    //    return $this->dNoteBalFC;
    //}

    /*
     * Get orderBalFC
     *
     * @return string
     */
    //public function getOrderBalFC()
    //{
    //    return $this->orderBalFC;
    //}

    /*
     * Get dNoteBalSy
     *
     * @return string
     */
    //public function getDNoteBalSy()
    //{
    //    return $this->dNoteBalSy;
    //}

    /*
     * Get orderBalSy
     *
     * @return string
     */
    //public function getOrderBalSy()
    //{
    //    return $this->orderBalSy;
    //}

    /*
     * Get transfered
     *
     * @return string
     */
    //public function getTransfered()
    //{
    //    return $this->transfered;
    //}

    /*
     * Get balTrnsfrd
     *
     * @return string
     */
    //public function getBalTrnsfrd()
    //{
    //    return $this->balTrnsfrd;
    //}

    /*
     * Get intrstRate
     *
     * @return string
     */
    //public function getIntrstRate()
    //{
    //    return $this->intrstRate;
    //}

    /*
     * Get commission
     *
     * @return string
     */
    //public function getCommission()
    //{
    //    return $this->commission;
    //}

    /*
     * Get commGrCode
     *
     * @return integer
     */
    //public function getCommGrCode()
    //{
    //    return $this->commGrCode;
    //}

    /*
     * Get freeText
     *
     * @return string
     */
    //public function getFreeText()
    //{
    //    return $this->free_Text;
    //}

    /**
     * Get slpCode
     *
     * @return integer
     */
    public function getSlpCode()
    {
        return $this->slpCode;
    }

    /*
     * Get prevYearAc
     *
     * @return string
     */
    //public function getPrevYearAc()
    //{
    //    return $this->prevYearAc;
    //}

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /*
     * Get rateDifAct
     *
     * @return string
     */
    //public function getRateDifAct()
    //{
    //    return $this->rateDifAct;
    //}

    /*
     * Get balanceSys
     *
     * @return string
     */
    //public function getBalanceSys()
    //{
    //    return $this->balanceSys;
    //}

    /*
     * Get balanceFC
     *
     * @return string
     */
    //public function getBalanceFC()
    //{
    //    return $this->balanceFC;
    //}

    /*
     * Get protected
     *
     * @return string
     */
    //public function getProtected()
    //{
    //    return $this->protected;
    //}

    /**
     * Get cellular
     *
     * @return string
     */
    public function getCellular()
    {
        return $this->cellular;
    }

    /*
     * Get avrageLate
     *
     * @return integer
     */
    //public function getAvrageLate()
    //{
    //    return $this->avrageLate;
    //}

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Get country
     *
     * @return CountryOCRY
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get mailCity
     *
     * @return string
     */
    public function getMailCity()
    {
        return $this->mailCity;
    }

    /**
     * Get mailCounty
     *
     * @return string
     */
    public function getMailCounty()
    {
        return $this->mailCounty;
    }

    /**
     * Get mailCountr
     *
     * @return CountryOCRY
     */
    public function getMailCountry()
    {
        return $this->mailCountry;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get eMail
     *
     * @return string
     */
    public function getE_Mail()
    {
        return $this->getEMail();
    }

    /*
     * Get picture
     *
     * @return string
     */
    //public function getPicture()
    //{
    //    return $this->picture;
    //}

    /*
     * Get dflAccount
     *
     * @return string
     */
    //public function getDflAccount()
    //{
    //    return $this->dflAccount;
    //}

    /*
     * Get dflBranch
     *
     * @return string
     */
    //public function getDflBranch()
    //{
    //    return $this->dflBranch;
    //}

    /*
     * Get bankCode
     *
     * @return string
     */
    //public function getBankCode()
    //{
    //    return $this->bankCode;
    //}

    /*
     * Get addID
     *
     * @return string
     */
    //public function getAddID()
    //{
    //    return $this->addID;
    //}

    /*
     * Get pager
     *
     * @return string
     */
    //public function getPager()
    //{
    //    return $this->pager;
    //}

    /*
     * Get fatherCard
     *
     * @return string
     */
    //public function getFatherCard()
    //{
    //    return $this->fatherCard;
    //}

    /*
     * Get cardFName
     *
     * @return string
     */
    //public function getCardFName()
    //{
    //    return $this->cardFName;
    //}

    /*
     * Get fatherType
     *
     * @return string
     */
    //public function getFatherType()
    //{
    //    return $this->fatherType;
    //}

    /*
     * Get ddctOffice
     *
     * @return string
     */
    //public function getDdctOffice()
    //{
    //    return $this->ddctOffice;
    //}

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /*
     * Get exportCode
     *
     * @return string
     */
    //public function getExportCode()
    //{
    //    return $this->exportCode;
    //}

    /*
     * Get dscntObjct
     *
     * @return integer
     */
    //public function getDscntObjct()
    //{
    //    return $this->dscntObjct;
    //}

    /*
     * Get dscntRel
     *
     * @return string
     */
    //public function getDscntRel()
    //{
    //    return $this->dscntRel;
    //}

    /*
     * Get sPGCounter
     *
     * @return integer
     */
    //public function getSPGCounter()
    //{
    //    return $this->sPGCounter;
    //}

    /*
     * Get sPPCounter
     *
     * @return integer
     */
    //public function getSPPCounter()
    //{
    //    return $this->sPPCounter;
    //}

    /*
     * Get ddctFileNo
     *
     * @return string
     */
    //public function getDdctFileNo()
    //{
    //    return $this->ddctFileNo;
    //}

    /*
     * Get sCNCounter
     *
     * @return integer
     */
    //public function getSCNCounter()
    //{
    //    return $this->sCNCounter;
    //}

    /*
     * Get minIntrst
     *
     * @return string
     */
    //public function getMinIntrst()
    //{
    //    return $this->minIntrst;
    //}

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource()
    //{
    //    return $this->dataSource;
    //}

    /*
     * Get oprCount
     *
     * @return integer
     */
    //public function getOprCount()
    //{
    //    return $this->oprCount;
    //}

    /*
     * Get exemptNo
     *
     * @return string
     */
    //public function getExemptNo()
    //{
    //    return $this->exemptNo;
    //}

    /*
     * Get priority
     *
     * @return integer
     */
    //public function getPriority()
    //{
    //    return $this->priority;
    //}

    /*
     * Get creditCard
     *
     * @return integer
     */
    //public function getCreditCard()
    //{
    //    return $this->creditCard;
    //}

    /*
     * Get crCardNum
     *
     * @return string
     */
    //public function getCrCardNum()
    //{
    //    return $this->crCardNum;
    //}

    /*
     * Get cardValid
     *
     * @return \DateTime
     */
    //public function getCardValid()
    //{
    //    return $this->cardValid;
    //}

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign()
    //{
    //    return $this->userSign;
    //}

    /*
     * Get locMth
     *
     * @return string
     */
    //public function getLocMth()
    //{
    //    return $this->locMth;
    //}

    /**
     * Get validFor
     *
     * @return string
     */
    public function getValidFor()
    {
        return $this->validFor;
    }

    /*
     * Get validFrom
     *
     * @return \DateTime
     */
    //public function getValidFrom()
    //{
    //    return $this->validFrom;
    //}

    /*
     * Get validTo
     *
     * @return \DateTime
     */
    //public function getValidTo()
    //{
    //    return $this->validTo;
    //}

    /**
     * Get frozenFor
     *
     * @return string
     */
    public function getFrozenFor()
    {
        return $this->frozenFor;
    }

    /*
     * Get frozenFrom
     *
     * @return \DateTime
     */
    //public function getFrozenFrom()
    //{
    //    return $this->frozenFrom;
    //}

    /*
     * Get frozenTo
     *
     * @return \DateTime
     */
    //public function getFrozenTo()
    //{
    //    return $this->frozenTo;
    //}

    /*
     * Get sEmployed
     *
     * @return string
     */
    //public function getSEmployed()
    //{
    //    return $this->sEmployed;
    //}

    /*
     * Get mTHCounter
     *
     * @return integer
     */
    //public function getMTHCounter()
    //{
    //    return $this->mTHCounter;
    //}

    /*
     * Get bNKCounter
     *
     * @return integer
     */
    //public function getBNKCounter()
    //{
    //    return $this->bNKCounter;
    //}

    /*
     * Get ddgKey
     *
     * @return integer
     */
    //public function getDdgKey()
    //{
    //    return $this->ddgKey;
    //}

    /*
     * Get ddtKey
     *
     * @return integer
     */
    //public function getDdtKey()
    //{
    //    return $this->ddtKey;
    //}

    /*
     * Get validComm
     *
     * @return string
     */
    //public function getValidComm()
    //{
    //    return $this->validComm;
    //}

    /**
     * Get frozenComm
     *
     * @return string
     */
    public function getFrozenComm()
    {
        return $this->frozenComm;
    }

    /*
     * Get chainStore
     *
     * @return string
     */
    //public function getChainStore()
    //{
    //    return $this->chainStore;
    //}

    /*
     * Get discInRet
     *
     * @return string
     */
    //public function getDiscInRet()
    //{
    //    return $this->discInRet;
    //}

    /**
     * Get state1
     *
     * @return string
     */
    public function getState1()
    {
        return $this->state1;
    }

    /**
     * Get state2
     *
     * @return string
     */
    public function getState2()
    {
        return $this->state2;
    }

    /*
     * Get vatGroup
     *
     * @return string
     */
    //public function getVatGroup()
    //{
    //    return $this->vatGroup;
    //}

    /*
     * Get logInstanc
     *
     * @return integer
     */
    //public function getLogInstanc()
    //{
    //    return $this->logInstanc;
    //}

    /*
     * Get objType
     *
     * @return string
     */
    //public function getObjType()
    //{
    //    return $this->objType;
    //}

    /*
     * Get indicator
     *
     * @return string
     */
    //public function getIndicator()
    //{
    //    return $this->indicator;
    //}

    /*
     * Get shipType
     *
     * @return integer
     */
    //public function getShipType()
    //{
    //    return $this->shipType;
    //}

    /*
     * Get debPayAcct
     *
     * @return string
     */
    //public function getDebPayAcct()
    //{
    //    return $this->debPayAcct;
    //}

    /**
     * Get shipToDef
     *
     * @return string
     */
    public function getShipToDef()
    {
        return $this->shipToDef;
    }

    /**
     * Get block
     *
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Get mailBlock
     *
     * @return string
     */
    public function getMailBlock()
    {
        return $this->mailBlock;
    }

    /*
     * Get password
     *
     * @return string
     */
    //public function getPassword()
    //{
    //    return $this->password;
    //}

    /*
     * Get eCVatGroup
     *
     * @return string
     */
    //public function getECVatGroup()
    //{
    //    return $this->eCVatGroup;
    //}

    /*
     * Get deleted
     *
     * @return string
     */
    //public function getDeleted()
    //{
    //    return $this->deleted;
    //}

    /*
     * Get iBAN
     *
     * @return string
     */
    //public function getIBAN()
    //{
    //    return $this->iBAN;
    //}

    /*
     * Get docEntry
     *
     * @return integer
     */
    //public function getDocEntry()
    //{
    //    return $this->docEntry;
    //}

    /*
     * Get formCode
     *
     * @return integer
     */
    //public function getFormCode()
    //{
    //    return $this->formCode;
    //}

    /*
     * Get box1099
     *
     * @return string
     */
    //public function getBox1099()
    //{
    //    return $this->box1099;
    //}

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /*
     * Get backOrder
     *
     * @return string
     */
    //public function getBackOrder()
    //{
    //    return $this->backOrder;
    //}

    /*
     * Get partDelivr
     *
     * @return string
     */
    //public function getPartDelivr()
    //{
    //    return $this->partDelivr;
    //}

    /*
     * Get dunnLevel
     *
     * @return integer
     */
    //public function getDunnLevel()
    //{
    //    return $this->dunnLevel;
    //}

    /*
     * Get dunnDate
     *
     * @return \DateTime
     */
    //public function getDunnDate()
    //{
    //    return $this->dunnDate;
    //}

    /*
     * Get blockDunn
     *
     * @return string
     */
    //public function getBlockDunn()
    //{
    //    return $this->blockDunn;
    //}

    /*
     * Get bankCountr
     *
     * @return string
     */
    //public function getBankCountr()
    //{
    //    return $this->bankCountr;
    //}

    /*
     * Get collecAuth
     *
     * @return string
     */
    //public function getCollecAuth()
    //{
    //    return $this->collecAuth;
    //}

    /*
     * Get dME
     *
     * @return string
     */
    //public function getDME()
    //{
    //    return $this->dME;
    //}

    /*
     * Get instrucKey
     *
     * @return string
     */
    //public function getInstrucKey()
    //{
    //    return $this->instrucKey;
    //}

    /*
     * Get singlePaym
     *
     * @return string
     */
    //public function getSinglePaym()
    //{
    //    return $this->singlePaym;
    //}

    /*
     * Get iSRBillId
     *
     * @return string
     */
    //public function getISRBillId()
    //{
    //    return $this->iSRBillId;
    //}

    /*
     * Get paymBlock
     *
     * @return string
     */
    //public function getPaymBlock()
    //{
    //    return $this->paymBlock;
    //}

    /*
     * Get refDetails
     *
     * @return string
     */
    //public function getRefDetails()
    //{
    //    return $this->refDetails;
    //}

    /**
     * Get houseBank
     *
     * @return string
     */
    public function getHouseBank()
    {
        return $this->houseBank;
    }

    /*
     * Get ownerIdNum
     *
     * @return string
     */
    //public function getOwnerIdNum()
    //{
    //    return $this->ownerIdNum;
    //}

    /*
     * Get pyBlckDesc
     *
     * @return integer
     */
    //public function getPyBlckDesc()
    //{
    //    return $this->pyBlckDesc;
    //}

    /*
     * Get housBnkCry
     *
     * @return string
     */
    //public function getHousBnkCry()
    //{
    //    return $this->housBnkCry;
    //}

    /*
     * Get housBnkAct
     *
     * @return string
     */
    //public function getHousBnkAct()
    //{
    //    return $this->housBnkAct;
    //}

    /*
     * Get housBnkBrn
     *
     * @return string
     */
    //public function getHousBnkBrn()
    //{
    //    return $this->housBnkBrn;
    //}

    /*
     * Get projectCod
     *
     * @return string
     */
    //public function getProjectCod()
    //{
    //    return $this->projectCod;
    //}

    /*
     * Get sysMatchNo
     *
     * @return integer
     */
    //public function getSysMatchNo()
    //{
    //    return $this->sysMatchNo;
    //}

    /*
     * Get vatIdUnCmp
     *
     * @return string
     */
    //public function getVatIdUnCmp()
    //{
    //    return $this->vatIdUnCmp;
    //}

    /*
     * Get agentCode
     *
     * @return string
     */
    //public function getAgentCode()
    //{
    //    return $this->agentCode;
    //}

    /*
     * Get tolrncDays
     *
     * @return integer
     */
    //public function getTolrncDays()
    //{
    //    return $this->tolrncDays;
    //}

    /*
     * Get selfInvoic
     *
     * @return string
     */
    //public function getSelfInvoic()
    //{
    //    return $this->selfInvoic;
    //}

    /*
     * Get deferrTax
     *
     * @return string
     */
    //public function getDeferrTax()
    //{
    //    return $this->deferrTax;
    //}

    /*
     * Get letterNum
     *
     * @return string
     */
    //public function getLetterNum()
    //{
    //    return $this->letterNum;
    //}

    /*
     * Get maxAmount
     *
     * @return string
     */
    //public function getMaxAmount()
    //{
    //    return $this->maxAmount;
    //}

    /*
     * Get fromDate
     *
     * @return \DateTime
     */
    //public function getFromDate()
    //{
    //    return $this->fromDate;
    //}

    /*
     * Get toDate
     *
     * @return \DateTime
     */
    //public function getToDate()
    //{
    //    return $this->toDate;
    //}

    /*
     * Get wTLiable
     *
     * @return string
     */
    //public function getWTLiable()
    //{
    //    return $this->wTLiable;
    //}

    /*
     * Get crtfcateNO
     *
     * @return string
     */
    //public function getCrtfcateNO()
    //{
    //    return $this->crtfcateNO;
    //}

    /*
     * Get expireDate
     *
     * @return \DateTime
     */
    //public function getExpireDate()
    //{
    //    return $this->expireDate;
    //}

    /*
     * Get nINum
     *
     * @return string
     */
    //public function getNINum()
    //{
    //    return $this->nINum;
    //}

    /*
     * Get accCritria
     *
     * @return string
     */
    //public function getAccCritria()
    //{
    //    return $this->accCritria;
    //}

    /*
     * Get wTCode
     *
     * @return string
     */
    //public function getWTCode()
    //{
    //    return $this->wTCode;
    //}

    /*
     * Get equ
     *
     * @return string
     */
    //public function getEqu()
    //{
    //    return $this->equ;
    //}

    /*
     * Get hldCode
     *
     * @return string
     */
    //public function getHldCode()
    //{
    //    return $this->hldCode;
    //}

    /*
     * Get connBP
     *
     * @return string
     */
    //public function getConnBP()
    //{
    //    return $this->connBP;
    //}

    /*
     * Get mltMthNum
     *
     * @return integer
     */
    //public function getMltMthNum()
    //{
    //    return $this->mltMthNum;
    //}

    /*
     * Get typWTReprt
     *
     * @return string
     */
    //public function getTypWTReprt()
    //{
    //    return $this->typWTReprt;
    //}

    /*
     * Get vATRegNum
     *
     * @return string
     */
    //public function getVATRegNum()
    //{
    //    return $this->vATRegNum;
    //}

    /*
     * Get repName
     *
     * @return string
     */
    //public function getRepName()
    //{
    //    return $this->repName;
    //}

    /*
     * Get industry
     *
     * @return string
     */
    //public function getIndustry()
    //{
    //    return $this->industry;
    //}

    /*
     * Get business
     *
     * @return string
     */
    //public function getBusiness()
    //{
    //    return $this->business;
    //}

    /*
     * Get wTTaxCat
     *
     * @return string
     */
    //public function getWTTaxCat()
    //{
    //    return $this->wTTaxCat;
    //}

    /*
     * Get isDomestic
     *
     * @return string
     */
    //public function getIsDomestic()
    //{
    //    return $this->isDomestic;
    //}

    /*
     * Get isResident
     *
     * @return string
     */
    //public function getIsResident()
    //{
    //    return $this->isResident;
    //}

    /*
     * Get autoCalBCG
     *
     * @return string
     */
    //public function getAutoCalBCG()
    //{
    //    return $this->autoCalBCG;
    //}

    /*
     * Get otrCtlAcct
     *
     * @return string
     */
    //public function getOtrCtlAcct()
    //{
    //    return $this->otrCtlAcct;
    //}

    /*
     * Get aliasName
     *
     * @return string
     */
    //public function getAliasName()
    //{
    //    return $this->aliasName;
    //}

    /*
     * Get building
     *
     * @return string
     */
    //public function getBuilding()
    //{
    //    return $this->building;
    //}

    /*
     * Get mailBuildi
     *
     * @return string
     */
    //public function getMailBuildi()
    //{
    //    return $this->mailBuildi;
    //}

    /*
     * Get boEPrsnt
     *
     * @return string
     */
    //public function getBoEPrsnt()
    //{
    //    return $this->boEPrsnt;
    //}

    /*
     * Get boEDiscnt
     *
     * @return string
     */
    //public function getBoEDiscnt()
    //{
    //    return $this->boEDiscnt;
    //}

    /*
     * Get boEOnClct
     *
     * @return string
     */
    //public function getBoEOnClct()
    //{
    //    return $this->boEOnClct;
    //}

    /*
     * Get unpaidBoE
     *
     * @return string
     */
    //public function getUnpaidBoE()
    //{
    //    return $this->unpaidBoE;
    //}

    /*
     * Get iTWTCode
     *
     * @return string
     */
    //public function getITWTCode()
    //{
    //    return $this->iTWTCode;
    //}

    /*
     * Get dunTerm
     *
     * @return string
     */
    //public function getDunTerm()
    //{
    //    return $this->dunTerm;
    //}

    /*
     * Get channlBP
     *
     * @return string
     */
    //public function getChannlBP()
    //{
    //    return $this->channlBP;
    //}

    /*
     * Get dfTcnician
     *
     * @return integer
     */
    //public function getDfTcnician()
    //{
    //    return $this->dfTcnician;
    //}

    /*
     * Get territory
     *
     * @return integer
     */
    //public function getTerritory()
    //{
    //    return $this->territory;
    //}

    /**
     * Get billToDef
     *
     * @return string
     */
    public function getBillToDef()
    {
        return $this->billToDef;
    }

    /*
     * Get dpmClear
     *
     * @return string
     */
    //public function getDpmClear()
    //{
    //    return $this->dpmClear;
    //}

    /**
     * Get intrntSite
     *
     * @return string
     */
    public function getIntrntSite()
    {
        return $this->intrntSite;
    }

    /*
     * Get langCode
     *
     * @return integer
     */
    //public function getLangCode()
    //{
    //    return $this->langCode;
    //}

    /*
     * Get housActKey
     *
     * @return integer
     */
    //public function getHousActKey()
    //{
    //    return $this->housActKey;
    //}

    /*
     * Get profession
     *
     * @return string
     */
    //public function getProfession()
    //{
    //    return $this->profession;
    //}

    /*
     * Get cDPNum
     *
     * @return integer
     */
    //public function getCDPNum()
    //{
    //    return $this->cDPNum;
    //}

    /*
     * Get dflBankKey
     *
     * @return integer
     */
    //public function getDflBankKey()
    //{
    //    return $this->dflBankKey;
    //}

    /*
     * Get bCACode
     *
     * @return string
     */
    //public function getBCACode()
    //{
    //    return $this->bCACode;
    //}

    /*
     * Get useShpdGd
     *
     * @return string
     */
    //public function getUseShpdGd()
    //{
    //    return $this->useShpdGd;
    //}

    /*
     * Get regNum
     *
     * @return string
     */
    //public function getRegNum()
    //{
    //    return $this->regNum;
    //}

    /*
     * Get verifNum
     *
     * @return string
     */
    //public function getVerifNum()
    //{
    //    return $this->verifNum;
    //}

    /*
     * Get bankCtlKey
     *
     * @return string
     */
    //public function getBankCtlKey()
    //{
    //    return $this->bankCtlKey;
    //}

    /*
     * Get housCtlKey
     *
     * @return string
     */
    //public function getHousCtlKey()
    //{
    //    return $this->housCtlKey;
    //}

    /*
     * Get addrType
     *
     * @return string
     */
    //public function getAddrType()
    //{
    //    return $this->addrType;
    //}

    /*
     * Get insurOp347
     *
     * @return string
     */
    //public function getInsurOp347()
    //{
    //    return $this->insurOp347;
    //}

    /*
     * Get mailAddrTy
     *
     * @return string
     */
    //public function getMailAddrTy()
    //{
    //    return $this->mailAddrTy;
    //}

    /*
     * Get streetNo
     *
     * @return string
     */
    //public function getStreetNo()
    //{
    //    return $this->streetNo;
    //}

    /*
     * Get mailStrNo
     *
     * @return string
     */
    //public function getMailStrNo()
    //{
    //    return $this->mailStrNo;
    //}

    /*
     * Get taxRndRule
     *
     * @return string
     */
    //public function getTaxRndRule()
    //{
    //    return $this->taxRndRule;
    //}

    /*
     * Get vendTID
     *
     * @return integer
     */
    //public function getVendTID()
    //{
    //    return $this->vendTID;
    //}

    /*
     * Get threshOver
     *
     * @return string
     */
    //public function getThreshOver()
    //{
    //    return $this->threshOver;
    //}

    /*
     * Get surOver
     *
     * @return string
     */
    //public function getSurOver()
    //{
    //    return $this->surOver;
    //}

    /*
     * Get vendorOcup
     *
     * @return string
     */
    //public function getVendorOcup()
    //{
    //    return $this->vendorOcup;
    //}

    /*
     * Get opCode347
     *
     * @return string
     */
    //public function getOpCode347()
    //{
    //    return $this->opCode347;
    //}

    /*
     * Get dpmIntAct
     *
     * @return string
     */
    //public function getDpmIntAct()
    //{
    //    return $this->dpmIntAct;
    //}

    /*
     * Get residenNum
     *
     * @return string
     */
    //public function getResidenNum()
    //{
    //    return $this->residenNum;
    //}

    /*
     * Get userSign2
     *
     * @return integer
     */
    //public function getUserSign2()
    //{
    //    return $this->userSign2;
    //}

    /*
     * Get plngGroup
     *
     * @return string
     */
    //public function getPlngGroup()
    //{
    //    return $this->plngGroup;
    //}

    /*
     * Get vatIDNum
     *
     * @return string
     */
    //public function getVatIDNum()
    //{
    //    return $this->vatIDNum;
    //}

    /*
     * Get affiliate
     *
     * @return string
     */
    //public function getAffiliate()
    //{
    //    return $this->affiliate;
    //}

    /*
     * Get mivzExpSts
     *
     * @return string
     */
    //public function getMivzExpSts()
    //{
    //    return $this->mivzExpSts;
    //}

    /*
     * Get hierchDdct
     *
     * @return string
     */
    //public function getHierchDdct()
    //{
    //    return $this->hierchDdct;
    //}

    /*
     * Get certWHT
     *
     * @return string
     */
    //public function getCertWHT()
    //{
    //    return $this->certWHT;
    //}

    /*
     * Get certBKeep
     *
     * @return string
     */
    //public function getCertBKeep()
    //{
    //    return $this->certBKeep;
    //}

    /*
     * Get wHShaamGrp
     *
     * @return string
     */
    //public function getWHShaamGrp()
    //{
    //    return $this->wHShaamGrp;
    //}

    /*
     * Get industryC
     *
     * @return integer
     */
    //public function getIndustryC()
    //{
    //    return $this->industryC;
    //}

    /*
     * Get datevAcct
     *
     * @return integer
     */
    //public function getDatevAcct()
    //{
    //    return $this->datevAcct;
    //}

    /*
     * Get datevFirst
     *
     * @return string
     */
    //public function getDatevFirst()
    //{
    //    return $this->datevFirst;
    //}

    /*
     * Get gTSRegNum
     *
     * @return string
     */
    //public function getGTSRegNum()
    //{
    //    return $this->gTSRegNum;
    //}

    /*
     * Get gTSBankAct
     *
     * @return string
     */
    //public function getGTSBankAct()
    //{
    //    return $this->gTSBankAct;
    //}

    /*
     * Get gTSBilAddr
     *
     * @return string
     */
    //public function getGTSBilAddr()
    //{
    //    return $this->gTSBilAddr;
    //}

    /*
     * Get hsBnkSwift
     *
     * @return string
     */
    //public function getHsBnkSwift()
    //{
    //    return $this->hsBnkSwift;
    //}

    /*
     * Get hsBnkIBAN
     *
     * @return string
     */
    //public function getHsBnkIBAN()
    //{
    //    return $this->hsBnkIBAN;
    //}

    /*
     * Get dflSwift
     *
     * @return string
     */
    //public function getDflSwift()
    //{
    //    return $this->dflSwift;
    //}

    /*
     * Get autoPost
     *
     * @return string
     */
    //public function getAutoPost()
    //{
    //    return $this->autoPost;
    //}

    /*
     * Get intrAcc
     *
     * @return string
     */
    //public function getIntrAcc()
    //{
    //    return $this->intrAcc;
    //}

    /*
     * Get feeAcc
     *
     * @return string
     */
    //public function getFeeAcc()
    //{
    //    return $this->feeAcc;
    //}

    /*
     * Get cpnNo
     *
     * @return integer
     */
    //public function getCpnNo()
    //{
    //    return $this->cpnNo;
    //}

    /*
     * Get nTSWebSite
     *
     * @return integer
     */
    //public function getNTSWebSite()
    //{
    //    return $this->nTSWebSite;
    //}

    /*
     * Get dflIBAN
     *
     * @return string
     */
    //public function getDflIBAN()
    //{
    //    return $this->dflIBAN;
    //}

    /*
     * Get series
     *
     * @return integer
     */
    //public function getSeries()
    //{
    //    return $this->series;
    //}

    /*
     * Get number
     *
     * @return integer
     */
    //public function getNumber()
    //{
    //    return $this->number;
    //}

    /*
     * Get eDocExpFrm
     *
     * @return integer
     */
    //public function getEDocExpFrm()
    //{
    //    return $this->eDocExpFrm;
    //}

    /*
     * Get taxIdIdent
     *
     * @return string
     */
    //public function getTaxIdIdent()
    //{
    //    return $this->taxIdIdent;
    //}

    /*
     * Get attachment
     *
     * @return string
     */
    //public function getAttachment()
    //{
    //    return $this->attachment;
    //}

    /*
     * Get atcEntry
     *
     * @return integer
     */
    //public function getAtcEntry()
    //{
    //    return $this->atcEntry;
    //}

    /*
     * Get discRel
     *
     * @return string
     */
    //public function getDiscRel()
    //{
    //    return $this->discRel;
    //}

    /*
     * Get noDiscount
     *
     * @return string
     */
    //public function getNoDiscount()
    //{
    //    return $this->noDiscount;
    //}

    /*
     * Get sCAdjust
     *
     * @return string
     */
    //public function getSCAdjust()
    //{
    //    return $this->sCAdjust;
    //}

    /*
     * Get dflAgrmnt
     *
     * @return integer
     */
    //public function getDflAgrmnt()
    //{
    //    return $this->dflAgrmnt;
    //}

    /*
     * Get glblLocNum
     *
     * @return string
     */
    //public function getGlblLocNum()
    //{
    //    return $this->glblLocNum;
    //}

    /*
     * Get senderID
     *
     * @return string
     */
    //public function getSenderID()
    //{
    //    return $this->senderID;
    //}

    /*
     * Get rcpntID
     *
     * @return string
     */
    //public function getRcpntID()
    //{
    //    return $this->rcpntID;
    //}

    /*
     * Get mainUsage
     *
     * @return integer
     */
    //public function getMainUsage()
    //{
    //    return $this->mainUsage;
    //}

    /*
     * Get sefazCheck
     *
     * @return string
     */
    //public function getSefazCheck()
    //{
    //    return $this->sefazCheck;
    //}

    /*
     * Get sefazReply
     *
     * @return string
     */
    //public function getSefazReply()
    //{
    //    return $this->sefazReply;
    //}

    /*
     * Get sefazDate
     *
     * @return \DateTime
     */
    //public function getSefazDate()
    //{
    //    return $this->sefazDate;
    //}

    /*
     * Get dateFrom
     *
     * @return \DateTime
     */
    //public function getDateFrom()
    //{
    //    return $this->dateFrom;
    //}

    /*
     * Get dateTill
     *
     * @return \DateTime
     */
    //public function getDateTill()
    //{
    //    return $this->dateTill;
    //}

    /*
     * Get relCode
     *
     * @return string
     */
    //public function getRelCode()
    //{
    //    return $this->relCode;
    //}

    /*
     * Get oKATO
     *
     * @return string
     */
    //public function getOKATO()
    //{
    //    return $this->oKATO;
    //}

    /*
     * Get oKTMO
     *
     * @return string
     */
    //public function getOKTMO()
    //{
    //    return $this->oKTMO;
    //}

    /*
     * Get kBKCode
     *
     * @return string
     */
    //public function getKBKCode()
    //{
    //    return $this->kBKCode;
    //}

    /*
     * Get typeOfOp
     *
     * @return string
     */
    //public function getTypeOfOp()
    //{
    //    return $this->typeOfOp;
    //}

    /*
     * Get ownerCode
     *
     * @return integer
     */
    //public function getOwnerCode()
    //{
    //    return $this->ownerCode;
    //}

    /*
     * Get mandateID
     *
     * @return string
     */
    //public function getMandateID()
    //{
    //    return $this->mandateID;
    //}

    /*
     * Get signDate
     *
     * @return \DateTime
     */
    //public function getSignDate()
    //{
    //    return $this->signDate;
    //}

    /*
     * Get remark1
     *
     * @return integer
     */
    //public function getRemark1()
    //{
    //    return $this->remark1;
    //}

    /*
     * Get conCerti
     *
     * @return string
     */
    //public function getConCerti()
    //{
    //    return $this->conCerti;
    //}

    /*
     * Get tpCusPres
     *
     * @return integer
     */
    //public function getTpCusPres()
    //{
    //    return $this->tpCusPres;
    //}

    /*
     * Get roleTypCod
     *
     * @return string
     */
    //public function getRoleTypCod()
    //{
    //    return $this->roleTypCod;
    //}
}
