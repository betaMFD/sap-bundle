<?php

namespace BetaMFD\SAPBundle\Model;

use BetaMFD\SAPBundle\Model\PaymentMethodOPYMInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="OPYM")
 * @ORM\Entity(readOnly=true)
 */
abstract class PaymentMethodOPYM implements PaymentMethodOPYMInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="PayMethCod", type="string", length=15, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $payMethodCode;

    /**
     * @var string
     *
     * @ORM\Column(name="Descript", type="string", length=100, nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Type", type="string", length=1, nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="BankTransf", type="string", length=1, nullable=true)
     */
    protected $bankTransf;

    /*
     * @var string
     *
     * @ORM\Column(name="Address", type="string", length=1, nullable=true)
     */
    //protected $address;

    /*
     * @var string
     *
     * @ORM\Column(name="BankDet", type="string", length=1, nullable=true)
     */
    //protected $bankDet;

    /*
     * @var string
     *
     * @ORM\Column(name="CllctAutor", type="string", length=1, nullable=true)
     */
    //protected $cllctAutor;

    /*
     * @var string
     *
     * @ORM\Column(name="FrgnPmntBl", type="string", length=1, nullable=true)
     */
    //protected $frgnPmntBl;

    /*
     * @var string
     *
     * @ORM\Column(name="FrgnBnkBl", type="string", length=1, nullable=true)
     */
    //protected $frgnBnkBl;

    /*
     * @var string
     *
     * @ORM\Column(name="CurrRestr", type="string", length=1, nullable=true)
     */
    //protected $currRestr;

    /*
     * @var string
     *
     * @ORM\Column(name="PostOffBnk", type="string", length=1, nullable=true)
     */
    //protected $postOffBnk;

    /*
     * @var string
     *
     * @ORM\Column(name="MaxAmount", type="decimal", scale=6, precision=10, nullable=true)
     */
    //protected $maxAmount;

    /*
     * @var string
     *
     * @ORM\Column(name="MinAmount", type="decimal", scale=6, precision=10, nullable=true)
     */
    //protected $minAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="BnkDflt", type="string", length=30, nullable=true)
     */
    protected $bank;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign", type="integer", nullable=true)
     */
    //protected $userSign;

    /*
     * @var string
     *
     * @ORM\Column(name="DataSource", type="string", length=1, nullable=true)
     */
    //protected $dataSource;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CreateDate", type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\ManyToOne(targetEntity="BetaMFD\SAPBundle\Model\CountryOCRYInterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="BankCountr", referencedColumnName="Code", nullable=true)
     * })
     */
    protected $bankCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="DflAccount", type="string", length=50, nullable=true)
     */
    protected $defaultAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="GLAccount", type="string", length=15, nullable=true)
     */
    protected $glAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="Branch", type="string", length=50, nullable=true)
     */
    protected $branch;

    /*
     * @var string
     *
     * @ORM\Column(name="KeyCode", type="string", length=6, nullable=true)
     */
    //protected $keyCode;

    /*
     * @var string
     *
     * @ORM\Column(name="TrnsType", type="string", length=2, nullable=true)
     */
    //protected $trnsType;

    /*
     * @var string
     *
     * @ORM\Column(name="Format", type="string", length=11, nullable=true)
     */
    //protected $format;

    /*
     * @var string
     *
     * @ORM\Column(name="AgtCollect", type="string", length=1, nullable=true)
     */
    //protected $agtCollect;

    /*
     * @var string
     *
     * @ORM\Column(name="SendAccept", type="string", length=1, nullable=true)
     */
    //protected $sendAccept;

    /*
     * @var string
     *
     * @ORM\Column(name="GrpByDate", type="string", length=1, nullable=true)
     */
    //protected $grpByDate;

    /*
     * @var string
     *
     * @ORM\Column(name="DepNorm", type="string", length=8, nullable=true)
     */
    //protected $depNorm;

    /*
     * @var string
     *
     * @ORM\Column(name="DebitMemo", type="string", length=1, nullable=true)
     */
    //protected $debitMemo;

    /*
     * @var string
     *
     * @ORM\Column(name="GroupPmRef", type="string", length=1, nullable=true)
     */
    //protected $groupPmRef;

    /*
     * @var string
     *
     * @ORM\Column(name="GroupInv", type="string", length=1, nullable=true)
     */
    //protected $groupInv;

    /*
     * @var string
     *
     * @ORM\Column(name="ValDateSel", type="string", length=1, nullable=true)
     */
    //protected $valDateSel;

    /*
     * @var integer
     *
     * @ORM\Column(name="PaymTerms", type="integer", nullable=true)
     */
    //protected $paymTerms;

    /*
     * @var string
     *
     * @ORM\Column(name="IntrimAcct", type="string", length=1, nullable=true)
     */
    //protected $intrimAcct;

    /*
     * @var integer
     *
     * @ORM\Column(name="BnkActKey", type="integer", nullable=true)
     */
    //protected $bnkActKey;

    /*
     * @var string
     *
     * @ORM\Column(name="DocType", type="string", length=2, nullable=true)
     */
    //protected $docType;

    /*
     * @var string
     *
     * @ORM\Column(name="Accepted", type="string", length=1, nullable=true)
     */
    //protected $accepted;

    /*
     * @var string
     *
     * @ORM\Column(name="PtfID", type="string", length=3, nullable=true)
     */
    //protected $ptfID;

    /*
     * @var string
     *
     * @ORM\Column(name="PtfCode", type="string", length=2, nullable=true)
     */
    //protected $ptfCode;

    /*
     * @var string
     *
     * @ORM\Column(name="PtfNum", type="string", length=4, nullable=true)
     */
    //protected $ptfNum;

    /*
     * @var string
     *
     * @ORM\Column(name="CurCode", type="string", length=2, nullable=true)
     */
    //protected $curCode;

    /*
     * @var string
     *
     * @ORM\Column(name="Instruct1", type="string", length=2, nullable=true)
     */
    //protected $instruct1;

    /*
     * @var string
     *
     * @ORM\Column(name="Instruct2", type="string", length=2, nullable=true)
     */
    //protected $instruct2;

    /*
     * @var string
     *
     * @ORM\Column(name="PaymntPlc", type="string", length=128, nullable=true)
     */
    //protected $paymntPlc;

    /*
     * @var string
     *
     * @ORM\Column(name="BoeDll", type="string", length=50, nullable=true)
     */
    //protected $boeDll;

    /*
     * @var string
     *
     * @ORM\Column(name="BankCtlKey", type="string", length=2, nullable=true)
     */
    //protected $bankCtlKey;

    /*
     * @var string
     *
     * @ORM\Column(name="BcgPcnt", type="decimal", scale=6, precision=10, nullable=true)
     */
    //protected $bcgPcnt;

    /**
     * @var string
     *
     * @ORM\Column(name="Active", type="string", length=1, nullable=true)
     */
    protected $active;

    /*
     * @var string
     *
     * @ORM\Column(name="GrpByBank", type="string", length=1, nullable=true)
     */
    //protected $grpByBank;

    /*
     * @var string
     *
     * @ORM\Column(name="GrpByCur", type="string", length=1, nullable=true)
     */
    //protected $grpByCur;

    /*
     * @var string
     *
     * @ORM\Column(name="DflIBAN", type="string", length=50, nullable=true)
     */
    //protected $dflIBAN;

    /*
     * @var string
     *
     * @ORM\Column(name="DflSwift", type="string", length=50, nullable=true)
     */
    //protected $dflSwift;

    /*
     * @var string
     *
     * @ORM\Column(name="BoeReport", type="string", length=8, nullable=true)
     */
    //protected $boeReport;

    /*
     * @var string
     *
     * @ORM\Column(name="CancInstr", type="string", length=2, nullable=true)
     */
    //protected $cancInstr;

    /*
     * @var string
     *
     * @ORM\Column(name="OccurCode", type="string", length=2, nullable=true)
     */
    //protected $occurCode;

    /*
     * @var string
     *
     * @ORM\Column(name="MovmntCode", type="string", length=10, nullable=true)
     */
    //protected $movmntCode;

    /*
     * @var string
     *
     * @ORM\Column(name="NegPymCode", type="string", length=15, nullable=true)
     */
    //protected $negPymCode;

    /*
     * @var string
     *
     * @ORM\Column(name="DirectDbt", type="string", length=5, nullable=true)
     */
    //protected $directDbt;

    /*
     * @var string
     *
     * @ORM\Column(name="IssueIndic", type="string", length=1, nullable=true)
     */
    //protected $issueIndic;

    /*
     * @var string
     *
     * @ORM\Column(name="PrintSEPA", type="string", length=1, nullable=true)
     */
    //protected $printSEPA;


    /**
     * Get the value of Pay Method Code
     *
     * @return string
     */
    public function getPayMethodCode()
    {
        return $this->payMethodCode;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the value of Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the value of Bank Transf
     *
     * @return string
     */
    public function getBankTransf()
    {
        return $this->bankTransf;
    }

    /*
     * Get the value of Address
     *
     * @return string
     */
    //public function getAddress()
    //{
    //    return $this->address;
    //}

    /*
     * Get the value of Cank Det
     *
     * @return string
     */
    //public function getCankDet()
    //{
    //    return $this->cankDet;
    //}

    /*
     * Get the value of Cllct Autor
     *
     * @return string
     */
    //public function getCllctAutor()
    //{
    //    return $this->cllctAutor;
    //}

    /*
     * Get the value of Frgn Pmnt Bl
     *
     * @return string
     */
    //public function getFrgnPmntBl()
    //{
    //    return $this->frgnPmntBl;
    //}

    /*
     * Get the value of Frgn Bnk Bl
     *
     * @return string
     */
    //public function getFrgnBnkBl()
    //{
    //    return $this->frgnBnkBl;
    //}

    /*
     * Get the value of Curr Restr
     *
     * @return string
     */
    //public function getCurrRestr()
    //{
    //    return $this->currRestr;
    //}

    /*
     * Get the value of Post Off Bnk
     *
     * @return string
     */
    //public function getPostOffBnk()
    //{
    //    return $this->postOffBnk;
    //}

    /*
     * Get the value of Max Amount
     *
     * @return string
     */
    //public function getMaxAmount()
    //{
    //    return $this->maxAmount;
    //}

    /*
     * Get the value of Min Amount
     *
     * @return string
     */
    //public function getMinAmount()
    //{
    //    return $this->minAmount;
    //}

    /**
     * Get the value of Bank
     *
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /*
     * Get the value of User Sign
     *
     * @return integer
     */
    //public function getUserSign()
    //{
    //    return $this->userSign;
    //}

    /*
     * Get the value of Data Source
     *
     * @return string
     */
    //public function getDataSource()
    //{
    //    return $this->dataSource;
    //}

    /**
     * Get the value of Create Date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Get the value of Bank Country
     *
     * @return mixed
     */
    public function getBankCountry()
    {
        return $this->bankCountry;
    }

    /**
     * Get the value of Default Account
     *
     * @return string
     */
    public function getDefaultAccount()
    {
        return $this->defaultAccount;
    }

    /**
     * Get the value of Gl Account
     *
     * @return string
     */
    public function getGlAccount()
    {
        return $this->glAccount;
    }

    /**
     * Get the value of Branch
     *
     * @return string
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /*
     * Get the value of Key Code
     *
     * @return string
     */
    //public function getKeyCode()
    //{
    //    return $this->keyCode;
    //}

    /*
     * Get the value of Trns Type
     *
     * @return string
     */
    //public function getTrnsType()
    //{
    //    return $this->trnsType;
    //}

    /*
     * Get the value of Format
     *
     * @return string
     */
    //public function getFormat()
    //{
    //    return $this->format;
    //}

    /*
     * Get the value of Agt Collect
     *
     * @return string
     */
    //public function getAgtCollect()
    //{
    //    return $this->agtCollect;
    //}

    /*
     * Get the value of Send Accept
     *
     * @return string
     */
    //public function getSendAccept()
    //{
    //    return $this->sendAccept;
    //}

    /*
     * Get the value of Grp By Date
     *
     * @return string
     */
    //public function getGrpByDate()
    //{
    //    return $this->grpByDate;
    //}

    /*
     * Get the value of Dep Norm
     *
     * @return string
     */
    //public function getDepNorm()
    //{
    //    return $this->depNorm;
    //}

    /*
     * Get the value of Debit Memo
     *
     * @return string
     */
    //public function getDebitMemo()
    //{
    //    return $this->debitMemo;
    //}

    /*
     * Get the value of Group Pm Ref
     *
     * @return string
     */
    //public function getGroupPmRef()
    //{
    //    return $this->groupPmRef;
    //}

    /*
     * Get the value of Group Inv
     *
     * @return string
     */
    //public function getGroupInv()
    //{
    //    return $this->groupInv;
    //}

    /*
     * Get the value of Val Date Sel
     *
     * @return string
     */
    //public function getValDateSel()
    //{
    //    return $this->valDateSel;
    //}

    /*
     * Get the value of Paym Terms
     *
     * @return integer
     */
    //public function getPaymTerms()
    //{
    //    return $this->paymTerms;
    //}

    /*
     * Get the value of Intrim Acct
     *
     * @return string
     */
    //public function getIntrimAcct()
    //{
    //    return $this->intrimAcct;
    //}

    /*
     * Get the value of Bnk Act Key
     *
     * @return integer
     */
    //public function getBnkActKey()
    //{
    //    return $this->bnkActKey;
    //}

    /*
     * Get the value of Doc Type
     *
     * @return string
     */
    //public function getDocType()
    //{
    //    return $this->docType;
    //}

    /*
     * Get the value of Accepted
     *
     * @return string
     */
    //public function getAccepted()
    //{
    //    return $this->accepted;
    //}

    /*
     * Get the value of Ptf
     *
     * @return string
     */
    //public function getPtfID()
    //{
    //    return $this->ptfID;
    //}

    /*
     * Get the value of Ptf Code
     *
     * @return string
     */
    //public function getPtfCode()
    //{
    //    return $this->ptfCode;
    //}

    /*
     * Get the value of Ptf Num
     *
     * @return string
     */
    //public function getPtfNum()
    //{
    //    return $this->ptfNum;
    //}

    /*
     * Get the value of Cur Code
     *
     * @return string
     */
    //public function getCurCode()
    //{
    //    return $this->curCode;
    //}

    /*
     * Get the value of Instruct
     *
     * @return string
     */
    //public function getInstruct1()
    //{
    //    return $this->instruct1;
    //}

    /*
     * Get the value of Instruct
     *
     * @return string
     */
    //public function getInstruct2()
    //{
    //    return $this->instruct2;
    //}

    /*
     * Get the value of Paymnt Plc
     *
     * @return string
     */
    //public function getPaymntPlc()
    //{
    //    return $this->paymntPlc;
    //}

    /*
     * Get the value of Boe Dll
     *
     * @return string
     */
    //public function getBoeDll()
    //{
    //    return $this->boeDll;
    //}

    /*
     * Get the value of Bank Ctl Key
     *
     * @return string
     */
    //public function getBankCtlKey()
    //{
    //    return $this->bankCtlKey;
    //}

    /*
     * Get the value of Bcg Pcnt
     *
     * @return string
     */
    //public function getBcgPcnt()
    //{
    //    return $this->bcgPcnt;
    //}

    /**
     * Get the value of Active
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /*
     * Get the value of Grp By Bank
     *
     * @return string
     */
    //public function getGrpByBank()
    //{
    //    return $this->grpByBank;
    //}

    /*
     * Get the value of Grp By Cur
     *
     * @return string
     */
    //public function getGrpByCur()
    //{
    //    return $this->grpByCur;
    //}

    /*
     * Get the value of Dfl
     *
     * @return string
     */
    //public function getDflIBAN()
    //{
    //    return $this->dflIBAN;
    //}

    /*
     * Get the value of Dfl Swift
     *
     * @return string
     */
    //public function getDflSwift()
    //{
    //    return $this->dflSwift;
    //}

    /*
     * Get the value of Boe Report
     *
     * @return string
     */
    //public function getBoeReport()
    //{
    //    return $this->boeReport;
    //}

    /*
     * Get the value of Canc Instr
     *
     * @return string
     */
    //public function getCancInstr()
    //{
    //    return $this->cancInstr;
    //}

    /*
     * Get the value of Occur Code
     *
     * @return string
     */
    //public function getOccurCode()
    //{
    //    return $this->occurCode;
    //}

    /*
     * Get the value of Movmnt Code
     *
     * @return string
     */
    //public function getMovmntCode()
    //{
    //    return $this->movmntCode;
    //}

    /*
     * Get the value of Neg Pym Code
     *
     * @return string
     */
    //public function getNegPymCode()
    //{
    //    return $this->negPymCode;
    //}

    /*
     * Get the value of Direct Dbt
     *
     * @return string
     */
    //public function getDirectDbt()
    //{
    //    return $this->directDbt;
    //}

    /*
     * Get the value of Issue Indic
     *
     * @return string
     */
    //public function getIssueIndic()
    //{
    //    return $this->issueIndic;
    //}

    /*
     * Get the value of Print
     *
     * @return string
     */
    //public function getPrintSEPA()
    //{
    //    return $this->printSEPA;
    //}
}
