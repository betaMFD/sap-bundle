<?php

namespace BetaMFD\SAPBundle\Model;

use BetaMFD\SAPBundle\Model\PaymentTermOCTGInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentTermOCTG
 *
 * @ORM\Table(name="OCTG")
 * @ORM\Entity(readOnly=true)
 */
abstract class PaymentTermOCTG implements PaymentTermOCTGInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="GroupNum", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $groupNum;

    /**
     * @var string
     *
     * @ORM\Column(name="PymntGroup", type="string", length=100, nullable=false)
     */
    protected $paymentGroup;

    /*
     * @var string
     *
     * @ORM\Column(name="PayDuMonth", type="string", length=1, nullable=true)
     */
    //protected $payDueMonth;

    /*
     * @var integer
     *
     * @ORM\Column(name="ExtraMonth", type="integer", nullable=true)
     */
    //protected $extraMonth;

    /**
     * @var integer
     *
     * @ORM\Column(name="ExtraDays", type="integer", nullable=true)
     */
    protected $extraDays;

    /*
     * @var integer
     *
     * @ORM\Column(name="PaymntsNum", type="integer", nullable=true)
     */
    //protected $paymentsNum;

    /*
     * @var string
     *
     * @ORM\Column(name="CredLimit", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $creditLimit;

    /*
     * @var string
     *
     * @ORM\Column(name="VolumDscnt", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $volumeDiscount;

    /*
     * @var string
     *
     * @ORM\Column(name="LatePyChrg", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $latePaymentCharge;

    /*
     * @var string
     *
     * @ORM\Column(name="ObligLimit", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $obligLimit;

    /*
     * @var integer
     *
     * @ORM\Column(name="ListNum", type="integer", nullable=true)
     */
    //protected $listNum;

    /*
     * @var string
     *
     * @ORM\Column(name="Payments", type="string", length=1, nullable=true)
     */
    //protected $payments;

    /*
     * @var integer
     *
     * @ORM\Column(name="NumOfPmnts", type="integer", nullable=true)
     */
    //protected $numOfPayments;

    /*
     * @var string
     *
     * @ORM\Column(name="Payment1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $payment1;

    /*
     * @var string
     *
     * @ORM\Column(name="DataSource", type="string", length=1, nullable=true)
     */
    //protected $dataSource;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign", type="integer", nullable=true)
     */
    //protected $userSign;

    /*
     * @var string
     *
     * @ORM\Column(name="OpenRcpt", type="string", length=1, nullable=true)
     */
    //protected $openRcpt;

    /*
     * @var string
     *
     * @ORM\Column(name="DiscCode", type="string", length=20, nullable=true)
     */
    //protected $discCode;

    /*
     * @var string
     *
     * @ORM\Column(name="DunningCod", type="string", length=20, nullable=true)
     */
    //protected $dunningCode;

    /*
     * @var string
     *
     * @ORM\Column(name="BslineDate", type="string", length=1, nullable=true)
     */
    //protected $baslineDate;

    /*
     * @var integer
     *
     * @ORM\Column(name="InstNum", type="integer", nullable=true)
     */
    //protected $instNum;

    /*
     * @var integer
     *
     * @ORM\Column(name="TolDays", type="integer", nullable=true)
     */
    //protected $tolDays;

    /*
     * @var string
     *
     * @ORM\Column(name="VATFirst", type="string", length=1, nullable=true)
     */
    //protected $vatFirst;

    /*
     * @var string
     *
     * @ORM\Column(name="CrdMthd", type="string", length=1, nullable=true)
     */
    //protected $creditMethod;


    /**
     * Constructor
     */
    protected function __construct() {}

    public function __toString()
    {
        return $this->getPaymentGroup();
    }

    /**
     * Get groupNum
     *
     * @return integer
     */
    public function getGroupNum()
    {
        return $this->groupNum;
    }

    /**
     * Get paymentGroup
     *
     * @return string
     */
    public function getPaymentGroup()
    {
        return $this->paymentGroup;
    }

    /*
     * Get payDueMonth
     *
     * @return string
     */
    //public function getPayDueMonth()
    //{
    //    return $this->payDueMonth;
    //}

    /*
     * Get extraMonth
     *
     * @return integer
     */
    //public function getExtraMonth()
    //{
    //    return $this->extraMonth;
    //}

    /**
     * Get extraDays
     *
     * @return integer
     */
    public function getExtraDays()
    {
        return $this->extraDays;
    }

    /*
     * Get paymentsNum
     *
     * @return integer
     */
    //public function getPaymentsNum()
    //{
    //    return $this->paymentsNum;
    //}

    /*
     * Get creditLimit
     *
     * @return string
     */
    //public function getCreditLimit()
    //{
    //    return $this->creditLimit;
    //}

    /*
     * Get volumeDiscount
     *
     * @return string
     */
    //public function getVolumeDiscount()
    //{
    //    return $this->volumeDiscount;
    //}

    /*
     * Get latePaymentCharge
     *
     * @return string
     */
    //public function getLatePaymentCharge()
    //{
    //    return $this->latePaymentCharge;
    //}

    /*
     * Get obligLimit
     *
     * @return string
     */
    //public function getObligLimit()
    //{
    //    return $this->obligLimit;
    //}

    /*
     * Get listNum
     *
     * @return integer
     */
    //public function getListNum()
    //{
    //    return $this->listNum;
    //}

    /*
     * Get payments
     *
     * @return string
     */
    //public function getPayments()
    //{
    //    return $this->payments;
    //}

    /*
     * Get numOfPayments
     *
     * @return integer
     */
    //public function getNumOfPayments()
    //{
    //    return $this->numOfPayments;
    //}

    /*
     * Get payment1
     *
     * @return string
     */
    //public function getPayment1()
    //{
    //    return $this->payment1;
    //}

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource()
    //{
    //    return $this->dataSource;
    //}

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign()
    //{
    //    return $this->userSign;
    //}

    /*
     * Get openRcpt
     *
     * @return string
     */
    //public function getOpenRcpt()
    //{
    //    return $this->openRcpt;
    //}

    /*
     * Get discCode
     *
     * @return string
     */
    //public function getDiscCode()
    //{
    //    return $this->discCode;
    //}

    /*
     * Get dunningCode
     *
     * @return string
     */
    //public function getDunningCode()
    //{
    //    return $this->dunningCode;
    //}

    /*
     * Get baslineDate
     *
     * @return string
     */
    //public function getBaslineDate()
    //{
    //    return $this->baslineDate;
    //}

    /*
     * Get instNum
     *
     * @return integer
     */
    //public function getInstNum()
    //{
    //    return $this->instNum;
    //}

    /*
     * Get tolDays
     *
     * @return integer
     */
    //public function getTolDays()
    //{
    //    return $this->tolDays;
    //}

    /*
     * Get vatFirst
     *
     * @return string
     */
    //public function getVatFirst()
    //{
    //    return $this->vatFirst;
    //}

    /*
     * Get creditMethod
     *
     * @return string
     */
    //public function getCreditMethod()
    //{
    //    return $this->creditMethod;
    //}
}
