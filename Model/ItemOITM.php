<?php

namespace BetaMFD\SAPBundle\Model;

use BetaMFD\SAPBundle\Model\ItemOITMInterface;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Table(name="OITM")
 * @ORM\Entity(readOnly=true)
 */
abstract class ItemOITM implements ItemOITMInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="ItemCode", type="string", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $itemCode;

    /**
     * @var string
     *
     * @ORM\Column(name="ItemName", type="string", length=100, nullable=true)
     */
    protected $itemName;

    /*
     * @var string
     *
     * @ORM\Column(name="FrgnName", type="string", length=100, nullable=true)
     */
    //protected $frgnName;

    /*
     * @var integer
     *
     * @ORM\Column(name="ItmsGrpCod", type="integer", nullable=true)
     */
    //protected $itmsGrpCod;

    /*
     * @var integer
     *
     * @ORM\Column(name="CstGrpCode", type="integer", nullable=true)
     */
    //protected $cstGrpCode;

    /*
     * @var string
     *
     * @ORM\Column(name="VatGourpSa", type="string", length=8, nullable=true)
     */
    //protected $vatGourpSa;

    /*
     * @var string
     *
     * @ORM\Column(name="CodeBars", type="string", length=16, nullable=true)
     */
    //protected $codeBars;

    /*
     * @var string
     *
     * @ORM\Column(name="VATLiable", type="string", length=1, nullable=true)
     */
    //protected $vatLiable;

    /*
     * @var string
     *
     * @ORM\Column(name="PrchseItem", type="string", length=1, nullable=true)
     */
    //protected $prchseItem;

    /*
     * @var string
     *
     * @ORM\Column(name="SellItem", type="string", length=1, nullable=true)
     */
    //protected $sellItem;

    /*
     * @var string
     *
     * @ORM\Column(name="InvntItem", type="string", length=1, nullable=true)
     */
    //protected $invntItem;

    /**
     * @var string
     *
     * @ORM\Column(name="OnHand", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $onHand;

    /**
     * @var string
     *
     * @ORM\Column(name="IsCommited", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $isCommited;

    /**
     * @var string
     *
     * @ORM\Column(name="OnOrder", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $onOrder;

    /*
     * @var string
     *
     * @ORM\Column(name="IncomeAcct", type="string", length=15, nullable=true)
     */
    //protected $incomeAcct;

    /*
     * @var string
     *
     * @ORM\Column(name="ExmptIncom", type="string", length=15, nullable=true)
     */
    //protected $exmptIncom;

    /**
     * @var string
     *
     * @ORM\Column(name="MaxLevel", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $maxLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="DfltWH", type="string", length=8, nullable=true)
     */
    protected $dfltWH;

    /**
     * @var OCRD
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\SAPBundle\Model\BusinessPartnerOCRDInterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CardCode", referencedColumnName="CardCode")
     * })
     */
    protected $cardCode;

    /**
     * @var string
     *
     * @ORM\Column(name="SuppCatNum", type="string", length=17, nullable=true)
     */
    protected $suppCatNum;

    /*
     * @var string
     *
     * @ORM\Column(name="BuyUnitMsr", type="string", length=100, nullable=true)
     */
    //protected $buyUnitMsr;

    /*
     * @var string
     *
     * @ORM\Column(name="NumInBuy", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $numInBuy;

    /*
     * @var string
     *
     * @ORM\Column(name="ReorderQty", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $reorderQty;

    /**
     * @var string
     *
     * @ORM\Column(name="MinLevel", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $minLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="LstEvlPric", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $lstEvlPric;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="LstEvlDate", type="datetime", nullable=true)
     */
    //protected $lstEvlDate;

    /*
     * @var string
     *
     * @ORM\Column(name="CustomPer", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $customPer;

    /*
     * @var string
     *
     * @ORM\Column(name="Canceled", type="string", length=1, nullable=true)
     */
    //protected $canceled;

    /*
     * @var integer
     *
     * @ORM\Column(name="MnufctTime", type="integer", nullable=true)
     */
    //protected $mnufctTime;

    /*
     * @var string
     *
     * @ORM\Column(name="WholSlsTax", type="string", length=1, nullable=true)
     */
    //protected $wholSlsTax;

    /*
     * @var string
     *
     * @ORM\Column(name="RetilrTax", type="string", length=1, nullable=true)
     */
    //protected $retilrTax;

    /*
     * @var string
     *
     * @ORM\Column(name="SpcialDisc", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $spcialDisc;

    /*
     * @var integer
     *
     * @ORM\Column(name="DscountCod", type="integer", nullable=true)
     */
    //protected $dscountCod;

    /*
     * @var string
     *
     * @ORM\Column(name="TrackSales", type="string", length=1, nullable=true)
     */
    //protected $trackSales;

    /*
     * @var string
     *
     * @ORM\Column(name="SalUnitMsr", type="string", length=100, nullable=true)
     */
    //protected $salUnitMsr;

    /*
     * @var string
     *
     * @ORM\Column(name="NumInSale", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $numInSale;

    /*
     * @var string
     *
     * @ORM\Column(name="Consig", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $consig;

    /*
     * @var integer
     *
     * @ORM\Column(name="QueryGroup", type="integer", nullable=true)
     */
    //protected $queryGroup;

    /*
     * @var string
     *
     * @ORM\Column(name="Counted", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $counted;

    /*
     * @var string
     *
     * @ORM\Column(name="OpenBlnc", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $openBlnc;

    /*
     * @var string
     *
     * @ORM\Column(name="EvalSystem", type="string", length=1, nullable=true)
     */
    //protected $evalSystem;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign", type="integer", nullable=true)
     */
    //protected $userSign;

    /*
     * @var string
     *
     * @ORM\Column(name="FREE", type="string", length=1, nullable=true)
     */
    //protected $free;

    /*
     * @var string
     *
     * @ORM\Column(name="PicturName", type="string", length=200, nullable=true)
     */
    //protected $picturName;

    /*
     * @var string
     *
     * @ORM\Column(name="Transfered", type="string", length=1, nullable=true)
     */
    //protected $transfered;

    /*
     * @var string
     *
     * @ORM\Column(name="BlncTrnsfr", type="string", length=1, nullable=true)
     */
    //protected $blncTrnsfr;

    /*
     * @var string
     *
     * @ORM\Column(name="UserText", type="string", length=1073741823, nullable=true)
     */
    //protected $userText;

    /*
     * @var string
     *
     * @ORM\Column(name="SerialNum", type="string", length=17, nullable=true)
     */
    //protected $serialNum;

    /*
     * @var string
     *
     * @ORM\Column(name="CommisPcnt", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $commisPcnt;

    /*
     * @var string
     *
     * @ORM\Column(name="CommisSum", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $commisSum;

    /*
     * @var integer
     *
     * @ORM\Column(name="CommisGrp", type="integer", nullable=true)
     */
    //protected $commisGrp;

    /*
     * @var string
     *
     * @ORM\Column(name="TreeType", type="string", length=1, nullable=true)
     */
    //protected $treeType;

    /*
     * @var string
     *
     * @ORM\Column(name="TreeQty", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $treeQty;

    /**
     * @var string
     *
     * @ORM\Column(name="LastPurPrc", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $lastPurPrc;

    /*
     * @var string
     *
     * @ORM\Column(name="LastPurCur", type="string", length=3, nullable=true)
     */
    //protected $lastPurCur;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="LastPurDat", type="datetime", nullable=true)
     */
    //protected $lastPurDat;

    /*
     * @var string
     *
     * @ORM\Column(name="ExitCur", type="string", length=3, nullable=true)
     */
    //protected $exitCur;

    /*
     * @var string
     *
     * @ORM\Column(name="ExitPrice", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $exitPrice;

    /*
     * @var string
     *
     * @ORM\Column(name="ExitWH", type="string", length=8, nullable=true)
     */
    //protected $exitWH;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetItem", type="string", length=1, nullable=true)
     */
    //protected $assetItem;

    /*
     * @var string
     *
     * @ORM\Column(name="WasCounted", type="string", length=1, nullable=true)
     */
    //protected $wasCounted;

    /*
     * @var string
     *
     * @ORM\Column(name="ManSerNum", type="string", length=1, nullable=true)
     */
    //protected $manSerNum;

    /*
     * @var string
     *
     * @ORM\Column(name="SHeight1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sHeight1;

    /*
     * @var integer
     *
     * @ORM\Column(name="SHght1Unit", type="integer", nullable=true)
     */
    //protected $sHght1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="SHeight2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sHeight2;

    /*
     * @var integer
     *
     * @ORM\Column(name="SHght2Unit", type="integer", nullable=true)
     */
    //protected $sHght2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="SWidth1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sWidth1;

    /*
     * @var integer
     *
     * @ORM\Column(name="SWdth1Unit", type="integer", nullable=true)
     */
    //protected $sWdth1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="SWidth2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sWidth2;

    /*
     * @var integer
     *
     * @ORM\Column(name="SWdth2Unit", type="integer", nullable=true)
     */
    //protected $sWdth2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="SLength1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sLength1;

    /*
     * @var integer
     *
     * @ORM\Column(name="SLen1Unit", type="integer", nullable=true)
     */
    //protected $sLen1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="Slength2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $slength2;

    /*
     * @var integer
     *
     * @ORM\Column(name="SLen2Unit", type="integer", nullable=true)
     */
    //protected $sLen2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="SVolume", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sVolume;

    /*
     * @var integer
     *
     * @ORM\Column(name="SVolUnit", type="integer", nullable=true)
     */
    //protected $sVolUnit;

    /*
     * @var string
     *
     * @ORM\Column(name="SWeight1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sWeight1;

    /*
     * @var integer
     *
     * @ORM\Column(name="SWght1Unit", type="integer", nullable=true)
     */
    //protected $sWght1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="SWeight2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $sWeight2;

    /*
     * @var integer
     *
     * @ORM\Column(name="SWght2Unit", type="integer", nullable=true)
     */
    //protected $sWght2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="BHeight1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bHeight1;

    /*
     * @var integer
     *
     * @ORM\Column(name="BHght1Unit", type="integer", nullable=true)
     */
    //protected $bHght1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="BHeight2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bHeight2;

    /*
     * @var integer
     *
     * @ORM\Column(name="BHght2Unit", type="integer", nullable=true)
     */
    //protected $bHght2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="BWidth1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bWidth1;

    /*
     * @var integer
     *
     * @ORM\Column(name="BWdth1Unit", type="integer", nullable=true)
     */
    //protected $bWdth1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="BWidth2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bWidth2;

    /*
     * @var integer
     *
     * @ORM\Column(name="BWdth2Unit", type="integer", nullable=true)
     */
    //protected $bWdth2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="BLength1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bLength1;

    /*
     * @var integer
     *
     * @ORM\Column(name="BLen1Unit", type="integer", nullable=true)
     */
    //protected $bLen1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="Blength2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $blength2;

    /*
     * @var integer
     *
     * @ORM\Column(name="BLen2Unit", type="integer", nullable=true)
     */
    //protected $bLen2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="BVolume", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bVolume;

    /*
     * @var integer
     *
     * @ORM\Column(name="BVolUnit", type="integer", nullable=true)
     */
    //protected $bVolUnit;

    /*
     * @var string
     *
     * @ORM\Column(name="BWeight1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bWeight1;

    /*
     * @var integer
     *
     * @ORM\Column(name="BWght1Unit", type="integer", nullable=true)
     */
    //protected $bWght1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="BWeight2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $bWeight2;

    /*
     * @var integer
     *
     * @ORM\Column(name="BWght2Unit", type="integer", nullable=true)
     */
    //protected $bWght2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="FixCurrCms", type="string", length=3, nullable=true)
     */
    //protected $fixCurrCms;

    /*
     * @var integer
     *
     * @ORM\Column(name="FirmCode", type="integer", nullable=true)
     */
    //protected $firmCode;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="LstSalDate", type="datetime", nullable=true)
     */
    //protected $lstSalDate;

    //Query Groups (QryGroup1, etc) would go here
    // but they have been left out intentionally
    //To use query groups, map them yourself to a reasonably named variable

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CreateDate", type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UpdateDate", type="datetime", nullable=true)
     */
    protected $updateDate;

    /*
     * @var string
     *
     * @ORM\Column(name="ExportCode", type="string", length=20, nullable=true)
     */
    //protected $exportCode;

    /*
     * @var string
     *
     * @ORM\Column(name="SalFactor1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $salFactor1;

    /*
     * @var string
     *
     * @ORM\Column(name="SalFactor2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $salFactor2;

    /*
     * @var string
     *
     * @ORM\Column(name="SalFactor3", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $salFactor3;

    /*
     * @var string
     *
     * @ORM\Column(name="SalFactor4", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $salFactor4;

    /*
     * @var string
     *
     * @ORM\Column(name="PurFactor1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $purFactor1;

    /*
     * @var string
     *
     * @ORM\Column(name="PurFactor2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $purFactor2;

    /*
     * @var string
     *
     * @ORM\Column(name="PurFactor3", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $purFactor3;

    /*
     * @var string
     *
     * @ORM\Column(name="PurFactor4", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $purFactor4;

    /*
     * @var string
     *
     * @ORM\Column(name="SalFormula", type="string", length=40, nullable=true)
     */
    //protected $salFormula;

    /*
     * @var string
     *
     * @ORM\Column(name="PurFormula", type="string", length=40, nullable=true)
     */
    //protected $purFormula;

    /*
     * @var string
     *
     * @ORM\Column(name="VatGroupPu", type="string", length=8, nullable=true)
     */
    //protected $vatGroupPu;

    /**
     * @var string
     *
     * @ORM\Column(name="AvgPrice", type="decimal", precision=10, scale=6, nullable=true)
     */
    protected $avgPrice;

    /*
     * @var string
     *
     * @ORM\Column(name="PurPackMsr", type="string", length=30, nullable=true)
     */
    //protected $purPackMsr;

    /*
     * @var string
     *
     * @ORM\Column(name="PurPackUn", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $purPackUn;

    /*
     * @var string
     *
     * @ORM\Column(name="SalPackMsr", type="string", length=30, nullable=true)
     */
    //protected $salPackMsr;

    /*
     * @var string
     *
     * @ORM\Column(name="SalPackUn", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $salPackUn;

    /*
     * @var integer
     *
     * @ORM\Column(name="SCNCounter", type="integer", nullable=true)
     */
    //protected $sCNCounter;

    /*
     * @var string
     *
     * @ORM\Column(name="ManBtchNum", type="string", length=1, nullable=true)
     */
    //protected $manBtchNum;

    /*
     * @var string
     *
     * @ORM\Column(name="ManOutOnly", type="string", length=1, nullable=true)
     */
    //protected $manOutOnly;

    /*
     * @var string
     *
     * @ORM\Column(name="DataSource", type="string", length=1, nullable=true)
     */
    //protected $dataSource;

    /**
     * @var string
     *
     * @ORM\Column(name="validFor", type="string", length=1, nullable=true)
     */
    protected $validFor;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="validFrom", type="datetime", nullable=true)
     */
    //protected $validFrom;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="validTo", type="datetime", nullable=true)
     */
    //protected $validTo;

    /*
     * @var string
     *
     * @ORM\Column(name="frozenFor", type="string", length=1, nullable=true)
     */
    //protected $frozenFor;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="frozenFrom", type="datetime", nullable=true)
     */
    //protected $frozenFrom;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="frozenTo", type="datetime", nullable=true)
     */
    //protected $frozenTo;

    /*
     * @var string
     *
     * @ORM\Column(name="BlockOut", type="string", length=1, nullable=true)
     */
    //protected $blockOut;

    /*
     * @var string
     *
     * @ORM\Column(name="ValidComm", type="string", length=30, nullable=true)
     */
    //protected $validComm;

    /**
     * @var string
     *
     * @ORM\Column(name="FrozenComm", type="string", length=30, nullable=true)
     */
    protected $frozenComm;

    /*
     * @var integer
     *
     * @ORM\Column(name="LogInstanc", type="integer", nullable=true)
     */
    //protected $logInstanc;

    /*
     * @var string
     *
     * @ORM\Column(name="ObjType", type="string", length=20, nullable=true)
     */
    //protected $objType;

    /**
     * @var string
     *
     * @ORM\Column(name="SWW", type="string", length=16, nullable=true)
     */
    protected $sww;

    /*
     * @var string
     *
     * @ORM\Column(name="Deleted", type="string", length=1, nullable=true)
     */
    //protected $deleted;

    /*
     * @var integer
     *
     * @ORM\Column(name="DocEntry", type="integer", nullable=true)
     */
    //protected $docEntry;

    /*
     * @var string
     *
     * @ORM\Column(name="ExpensAcct", type="string", length=15, nullable=true)
     */
    //protected $expensAcct;

    /*
     * @var string
     *
     * @ORM\Column(name="FrgnInAcct", type="string", length=15, nullable=true)
     */
    //protected $frgnInAcct;

    /*
     * @var integer
     *
     * @ORM\Column(name="ShipType", type="integer", nullable=true)
     */
    //protected $shipType;

    /*
     * @var string
     *
     * @ORM\Column(name="GLMethod", type="string", length=1, nullable=true)
     */
    //protected $gLMethod;

    /*
     * @var string
     *
     * @ORM\Column(name="ECInAcct", type="string", length=15, nullable=true)
     */
    //protected $ecInAcct;

    /*
     * @var string
     *
     * @ORM\Column(name="FrgnExpAcc", type="string", length=15, nullable=true)
     */
    //protected $frgnExpAcc;

    /*
     * @var string
     *
     * @ORM\Column(name="ECExpAcc", type="string", length=15, nullable=true)
     */
    //protected $eCExpAcc;

    /*
     * @var string
     *
     * @ORM\Column(name="TaxType", type="string", length=1, nullable=true)
     */
    //protected $taxType;

    /*
     * @var string
     *
     * @ORM\Column(name="ByWh", type="string", length=1, nullable=true)
     */
    //protected $byWh;

    /*
     * @var string
     *
     * @ORM\Column(name="WTLiable", type="string", length=1, nullable=true)
     */
    //protected $wtLiable;

    /*
     * @var string
     *
     * @ORM\Column(name="ItemType", type="string", length=1, nullable=true)
     */
    //protected $itemType;

    /*
     * @var string
     *
     * @ORM\Column(name="WarrntTmpl", type="string", length=20, nullable=true)
     */
    //protected $warrntTmpl;

    /*
     * @var string
     *
     * @ORM\Column(name="BaseUnit", type="string", length=20, nullable=true)
     */
    //protected $baseUnit;

    /*
     * @var string
     *
     * @ORM\Column(name="CountryOrg", type="string", length=3, nullable=true)
     */
    //protected $countryOrg;

    /*
     * @var string
     *
     * @ORM\Column(name="StockValue", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $stockValue;

    /*
     * @var string
     *
     * @ORM\Column(name="Phantom", type="string", length=1, nullable=true)
     */
    //protected $phantom;

    /*
     * @var string
     *
     * @ORM\Column(name="IssueMthd", type="string", length=1, nullable=true)
     */
    //protected $issueMthd;

    /*
     * @var string
     *
     * @ORM\Column(name="FREE1", type="string", length=1, nullable=true)
     */
    //protected $free1;

    /*
     * @var string
     *
     * @ORM\Column(name="PricingPrc", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $pricingPrc;

    /*
     * @var string
     *
     * @ORM\Column(name="MngMethod", type="string", length=1, nullable=true)
     */
    //protected $mngMethod;

    /*
     * @var string
     *
     * @ORM\Column(name="ReorderPnt", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $reorderPnt;

    /*
     * @var string
     *
     * @ORM\Column(name="InvntryUom", type="string", length=100, nullable=true)
     */
    //protected $invntryUom;

    /*
     * @var string
     *
     * @ORM\Column(name="PlaningSys", type="string", length=1, nullable=true)
     */
    //protected $planingSys;

    /*
     * @var string
     *
     * @ORM\Column(name="PrcrmntMtd", type="string", length=1, nullable=true)
     */
    //protected $prcrmntMtd;

    /*
     * @var integer
     *
     * @ORM\Column(name="OrdrIntrvl", type="integer", nullable=true)
     */
    //protected $ordrIntrvl;

    /*
     * @var string
     *
     * @ORM\Column(name="OrdrMulti", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $ordrMulti;

    /*
     * @var string
     *
     * @ORM\Column(name="MinOrdrQty", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $minOrdrQty;

    /*
     * @var integer
     *
     * @ORM\Column(name="LeadTime", type="integer", nullable=true)
     */
    //protected $leadTime;

    /*
     * @var string
     *
     * @ORM\Column(name="IndirctTax", type="string", length=1, nullable=true)
     */
    //protected $indirctTax;

    /*
     * @var string
     *
     * @ORM\Column(name="TaxCodeAR", type="string", length=8, nullable=true)
     */
    //protected $taxCodeAR;

    /*
     * @var string
     *
     * @ORM\Column(name="TaxCodeAP", type="string", length=8, nullable=true)
     */
    //protected $taxCodeAP;

    /*
     * @var integer
     *
     * @ORM\Column(name="OSvcCode", type="integer", nullable=true)
     */
    //protected $oSvcCode;

    /*
     * @var integer
     *
     * @ORM\Column(name="ISvcCode", type="integer", nullable=true)
     */
    //protected $iSvcCode;

    /*
     * @var integer
     *
     * @ORM\Column(name="ServiceGrp", type="integer", nullable=true)
     */
    //protected $serviceGrp;

    /*
     * @var integer
     *
     * @ORM\Column(name="NCMCode", type="integer", nullable=true)
     */
    //protected $ncmCode;

    /*
     * @var string
     *
     * @ORM\Column(name="MatType", type="string", length=3, nullable=true)
     */
    //protected $matType;

    /*
     * @var integer
     *
     * @ORM\Column(name="MatGrp", type="integer", nullable=true)
     */
    //protected $matGrp;

    /*
     * @var string
     *
     * @ORM\Column(name="ProductSrc", type="string", length=2, nullable=true)
     */
    //protected $productSrc;

    /*
     * @var integer
     *
     * @ORM\Column(name="ServiceCtg", type="integer", nullable=true)
     */
    //protected $serviceCtg;

    /*
     * @var string
     *
     * @ORM\Column(name="ItemClass", type="string", length=1, nullable=true)
     */
    //protected $itemClass;

    /*
     * @var string
     *
     * @ORM\Column(name="Excisable", type="string", length=1, nullable=true)
     */
    //protected $excisable;

    /*
     * @var integer
     *
     * @ORM\Column(name="ChapterID", type="integer", nullable=true)
     */
    //protected $chapterID;

    /*
     * @var string
     *
     * @ORM\Column(name="NotifyASN", type="string", length=40, nullable=true)
     */
    //protected $notifyASN;

    /*
     * @var string
     *
     * @ORM\Column(name="ProAssNum", type="string", length=20, nullable=true)
     */
    //protected $proAssNum;

    /*
     * @var string
     *
     * @ORM\Column(name="AssblValue", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $assblValue;

    /*
     * @var integer
     *
     * @ORM\Column(name="DNFEntry", type="integer", nullable=true)
     */
    //protected $dnfEntry;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign2", type="integer", nullable=true)
     */
    //protected $userSign2;

    /*
     * @var string
     *
     * @ORM\Column(name="Spec", type="string", length=30, nullable=true)
     */
    //protected $spec;

    /*
     * @var string
     *
     * @ORM\Column(name="TaxCtg", type="string", length=4, nullable=true)
     */
    //protected $taxCtg;

    /*
     * @var integer
     *
     * @ORM\Column(name="Series", type="integer", nullable=true)
     */
    //protected $series;

    /*
     * @var integer
     *
     * @ORM\Column(name="Number", type="integer", nullable=true)
     */
    //protected $number;

    /*
     * @var integer
     *
     * @ORM\Column(name="FuelCode", type="integer", nullable=true)
     */
    //protected $fuelCode;

    /*
     * @var string
     *
     * @ORM\Column(name="BeverTblC", type="string", length=2, nullable=true)
     */
    //protected $beverTblC;

    /*
     * @var string
     *
     * @ORM\Column(name="BeverGrpC", type="string", length=2, nullable=true)
     */
    //protected $beverGrpC;

    /*
     * @var integer
     *
     * @ORM\Column(name="BeverTM", type="integer", nullable=true)
     */
    //protected $beverTM;

    /*
     * @var string
     *
     * @ORM\Column(name="Attachment", type="string", length=1073741823, nullable=true)
     */
    //protected $attachment;

    /*
     * @var integer
     *
     * @ORM\Column(name="AtcEntry", type="integer", nullable=true)
     */
    //protected $atcEntry;

    /*
     * @var integer
     *
     * @ORM\Column(name="ToleranDay", type="integer", nullable=true)
     */
    //protected $toleranDay;

    /*
     * @var integer
     *
     * @ORM\Column(name="UgpEntry", type="integer", nullable=true)
     */
    //protected $ugpEntry;

    /*
     * @var integer
     *
     * @ORM\Column(name="PUoMEntry", type="integer", nullable=true)
     */
    //protected $pUoMEntry;

    /*
     * @var integer
     *
     * @ORM\Column(name="SUoMEntry", type="integer", nullable=true)
     */
    //protected $sUoMEntry;

    /*
     * @var integer
     *
     * @ORM\Column(name="IUoMEntry", type="integer", nullable=true)
     */
    //protected $iUoMEntry;

    /*
     * @var integer
     *
     * @ORM\Column(name="IssuePriBy", type="integer", nullable=true)
     */
    //protected $issuePriBy;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetClass", type="string", length=20, nullable=true)
     */
    //protected $assetClass;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetGroup", type="string", length=15, nullable=true)
     */
    //protected $assetGroup;

    /*
     * @var string
     *
     * @ORM\Column(name="InventryNo", type="string", length=12, nullable=true)
     */
    //protected $inventryNo;

    /*
     * @var integer
     *
     * @ORM\Column(name="Technician", type="integer", nullable=true)
     */
    //protected $technician;

    /*
     * @var integer
     *
     * @ORM\Column(name="Employee", type="integer", nullable=true)
     */
    //protected $employee;

    /*
     * @var integer
     *
     * @ORM\Column(name="Location", type="integer", nullable=true)
     */
    //protected $location;

    /*
     * @var string
     *
     * @ORM\Column(name="StatAsset", type="string", length=1, nullable=true)
     */
    //protected $statAsset;

    /*
     * @var string
     *
     * @ORM\Column(name="Cession", type="string", length=1, nullable=true)
     */
    //protected $cession;

    /*
     * @var string
     *
     * @ORM\Column(name="DeacAftUL", type="string", length=1, nullable=true)
     */
    //protected $deacAftUL;

    /*
     * @var string
     *
     * @ORM\Column(name="AsstStatus", type="string", length=1, nullable=true)
     */
    //protected $asstStatus;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="CapDate", type="datetime", nullable=true)
     */
    //protected $capDate;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="AcqDate", type="datetime", nullable=true)
     */
    //protected $acqDate;

    /*
     * @var \DateTime
     *
     * @ORM\Column(name="RetDate", type="datetime", nullable=true)
     */
    //protected $retDate;

    /*
     * @var string
     *
     * @ORM\Column(name="GLPickMeth", type="string", length=1, nullable=true)
     */
    //protected $gLPickMeth;

    /*
     * @var string
     *
     * @ORM\Column(name="NoDiscount", type="string", length=1, nullable=true)
     */
    //protected $noDiscount;

    /*
     * @var string
     *
     * @ORM\Column(name="MgrByQty", type="string", length=1, nullable=true)
     */
    //protected $mgrByQty;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetRmk1", type="string", length=100, nullable=true)
     */
    //protected $assetRmk1;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetRmk2", type="string", length=100, nullable=true)
     */
    //protected $assetRmk2;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetAmnt1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $assetAmnt1;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetAmnt2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $assetAmnt2;

    /*
     * @var string
     *
     * @ORM\Column(name="DeprGroup", type="string", length=15, nullable=true)
     */
    //protected $deprGroup;

    /*
     * @var string
     *
     * @ORM\Column(name="AssetSerNo", type="string", length=30, nullable=true)
     */
    //protected $assetSerNo;

    /*
     * @var string
     *
     * @ORM\Column(name="CntUnitMsr", type="string", length=100, nullable=true)
     */
    //protected $cntUnitMsr;

    /*
     * @var string
     *
     * @ORM\Column(name="NumInCnt", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $numInCnt;

    /*
     * @var integer
     *
     * @ORM\Column(name="INUoMEntry", type="integer", nullable=true)
     */
    //protected $iNUoMEntry;

    /*
     * @var string
     *
     * @ORM\Column(name="OneBOneRec", type="string", length=1, nullable=true)
     */
    //protected $oneBOneRec;

    /*
     * @var string
     *
     * @ORM\Column(name="RuleCode", type="string", length=2, nullable=true)
     */
    //protected $ruleCode;

    /*
     * @var string
     *
     * @ORM\Column(name="ScsCode", type="string", length=10, nullable=true)
     */
    //protected $scsCode;

    /*
     * @var string
     *
     * @ORM\Column(name="SpProdType", type="string", length=2, nullable=true)
     */
    //protected $spProdType;

    /*
     * @var string
     *
     * @ORM\Column(name="IWeight1", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $iWeight1;

    /*
     * @var integer
     *
     * @ORM\Column(name="IWght1Unit", type="integer", nullable=true)
     */
    //protected $iWght1Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="IWeight2", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $iWeight2;

    /*
     * @var integer
     *
     * @ORM\Column(name="IWght2Unit", type="integer", nullable=true)
     */
    //protected $iWght2Unit;

    /*
     * @var string
     *
     * @ORM\Column(name="CompoWH", type="string", length=1, nullable=true)
     */
    //protected $compoWH;

    /**
     * Constructor
     */
    protected function __construct() {}

    public function __toString()
    {
        return $this->getItemCode();
    }

    /**
     * Get itemCode
     *
     * @return string
     */
    public function getItemCode()
    {
        return $this->itemCode;
    }

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /*
     * Get frgnName
     *
     * @return string
     */
    //public function getFrgnName()
    //{
    //    return $this->frgnName;
    //}

    /*
     * Get itmsGrpCod
     *
     * @return integer
     */
    //public function getItmsGrpCod()
    //{
    //    return $this->itmsGrpCod;
    //}

    /*
     * Get cstGrpCode
     *
     * @return integer
     */
    //public function getCstGrpCode()
    //{
    //    return $this->cstGrpCode;
    //}

    /*
     * Get vatGourpSa
     *
     * @return string
     */
    //public function getVatGourpSa()
    //{
    //    return $this->vatGourpSa;
    //}

    /*
     * Get codeBars
     *
     * @return string
     */
    //public function getCodeBars()
    //{
    //    return $this->codeBars;
    //}

    /*
     * Get vatLiable
     *
     * @return string
     */
    //public function getVatLiable()
    //{
    //    return $this->vatLiable;
    //}

    /*
     * Get prchseItem
     *
     * @return string
     */
    //public function getPrchseItem()
    //{
    //    return $this->prchseItem;
    //}

    /*
     * Get sellItem
     *
     * @return string
     */
    //public function getSellItem()
    //{
    //    return $this->sellItem;
    //}

    /*
     * Get invntItem
     *
     * @return string
     */
    //public function getInvntItem()
    //{
    //    return $this->invntItem;
    //}

    /**
     * Get onHand
     *
     * @return string
     */
    public function getOnHand()
    {
        return $this->onHand;
    }

    /**
     * Get isCommited
     *
     * @return string
     */
    public function getIsCommited()
    {
        return $this->isCommited;
    }

    /**
     * Get isCommited
     *
     * @return string
     */
    public function getIsCommitted()
    {
        return $this->getIsCommited();
    }

    /**
     * Is this item Committed?
     *
     * @return boolean
     */
    public function isCommitted()
    {
        return $this->getIsCommited() == 'Y';
    }

    /**
     * Get onOrder
     *
     * @return string
     */
    public function getOnOrder()
    {
        return $this->onOrder;
    }

    /*
     * Get incomeAcct
     *
     * @return string
     */
    //public function getIncomeAcct()
    //{
    //    return $this->incomeAcct;
    //}

    /*
     * Get exmptIncom
     *
     * @return string
     */
    //public function getExmptIncom()
    //{
    //    return $this->exmptIncom;
    //}

    /**
     * Get maxLevel
     *
     * @return string
     */
    public function getMaxLevel()
    {
        return $this->maxLevel;
    }

    /**
     * Get dfltWH
     *
     * @return string
     */
    public function getDfltWH()
    {
        return $this->dfltWH;
    }

    /**
     * Get cardCode
     *
     * @return string
     */
    public function getCardCode()
    {
        return $this->cardCode;
    }

    /**
     * Get suppCatNum
     *
     * @return string
     */
    public function getSuppCatNum()
    {
        return $this->suppCatNum;
    }

    /*
     * Get buyUnitMsr
     *
     * @return string
     */
    //public function getBuyUnitMsr()
    //{
    //    return $this->buyUnitMsr;
    //}

    /*
     * Get numInBuy
     *
     * @return string
     */
    //public function getNumInBuy()
    //{
    //    return $this->numInBuy;
    //}

    /*
     * Get reorderQty
     *
     * @return string
     */
    //public function getReorderQty()
    //{
    //    return $this->reorderQty;
    //}

    /**
     * Get minLevel
     *
     * @return string
     */
    public function getMinLevel()
    {
        return $this->minLevel;
    }

    /**
     * Get lstEvlPric
     *
     * @return string
     */
    public function getLstEvlPric()
    {
        return $this->lstEvlPric;
    }

    /*
     * Get lstEvlDate
     *
     * @return \DateTime
     */
    //public function getLstEvlDate()
    //{
    //    return $this->lstEvlDate;
    //}

    /*
     * Get customPer
     *
     * @return string
     */
    //public function getCustomPer()
    //{
    //    return $this->customPer;
    //}

    /*
     * Get canceled
     *
     * @return string
     */
    //public function getCanceled()
    //{
    //    return $this->canceled;
    //}

    /*
     * Get mnufctTime
     *
     * @return integer
     */
    //public function getMnufctTime()
    //{
    //    return $this->mnufctTime;
    //}

    /*
     * Get wholSlsTax
     *
     * @return string
     */
    //public function getWholSlsTax()
    //{
    //    return $this->wholSlsTax;
    //}

    /*
     * Get retilrTax
     *
     * @return string
     */
    //public function getRetilrTax()
    //{
    //    return $this->retilrTax;
    //}

    /*
     * Get spcialDisc
     *
     * @return string
     */
    //public function getSpcialDisc()
    //{
    //    return $this->spcialDisc;
    //}

    /*
     * Get dscountCod
     *
     * @return integer
     */
    //public function getDscountCod()
    //{
    //    return $this->dscountCod;
    //}

    /*
     * Get trackSales
     *
     * @return string
     */
    //public function getTrackSales()
    //{
    //    return $this->trackSales;
    //}

    /*
     * Get salUnitMsr
     *
     * @return string
     */
    //public function getSalUnitMsr()
    //{
    //    return $this->salUnitMsr;
    //}

    /*
     * Get numInSale
     *
     * @return string
     */
    //public function getNumInSale()
    //{
    //    return $this->numInSale;
    //}

    /*
     * Get consig
     *
     * @return string
     */
    //public function getConsig()
    //{
    //    return $this->consig;
    //}

    /*
     * Get queryGroup
     *
     * @return integer
     */
    //public function getQueryGroup()
    //{
    //    return $this->queryGroup;
    //}

    /*
     * Get counted
     *
     * @return string
     */
    //public function getCounted()
    //{
    //    return $this->counted;
    //}

    /*
     * Get openBlnc
     *
     * @return string
     */
    //public function getOpenBlnc()
    //{
    //    return $this->openBlnc;
    //}

    /*
     * Get evalSystem
     *
     * @return string
     */
    //public function getEvalSystem()
    //{
    //    return $this->evalSystem;
    //}

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign()
    //{
    //    return $this->userSign;
    //}

    /*
     * Get free
     *
     * @return string
     */
    //public function getFree()
    //{
    //    return $this->free;
    //}

    /*
     * Get picturName
     *
     * @return string
     */
    //public function getPicturName()
    //{
    //    return $this->picturName;
    //}

    /*
     * Get transfered
     *
     * @return string
     */
    //public function getTransfered()
    //{
    //    return $this->transfered;
    //}

    /*
     * Get blncTrnsfr
     *
     * @return string
     */
    //public function getBlncTrnsfr()
    //{
    //    return $this->blncTrnsfr;
    //}

    /*
     * Get userText
     *
     * @return string
     */
    //public function getUserText()
    //{
    //    return $this->userText;
    //}

    /*
     * Get serialNum
     *
     * @return string
     */
    //public function getSerialNum()
    //{
    //    return $this->serialNum;
    //}

    /*
     * Get commisPcnt
     *
     * @return string
     */
    //public function getCommisPcnt()
    //{
    //    return $this->commisPcnt;
    //}

    /*
     * Get commisSum
     *
     * @return string
     */
    //public function getCommisSum()
    //{
    //    return $this->commisSum;
    //}

    /*
     * Get commisGrp
     *
     * @return integer
     */
    //public function getCommisGrp()
    //{
    //    return $this->commisGrp;
    //}

    /*
     * Get treeType
     *
     * @return string
     */
    //public function getTreeType()
    //{
    //    return $this->treeType;
    //}

    /*
     * Get treeQty
     *
     * @return string
     */
    //public function getTreeQty()
    //{
    //    return $this->treeQty;
    //}

    /**
     * Get lastPurPrc
     *
     * @return string
     */
    public function getLastPurPrc()
    {
        return $this->lastPurPrc;
    }

    /*
     * Get lastPurCur
     *
     * @return string
     */
    //public function getLastPurCur()
    //{
    //    return $this->lastPurCur;
    //}

    /*
     * Get lastPurDat
     *
     * @return \DateTime
     */
    //public function getLastPurDat()
    //{
    //    return $this->lastPurDat;
    //}

    /*
     * Get exitCur
     *
     * @return string
     */
    //public function getExitCur()
    //{
    //    return $this->exitCur;
    //}

    /*
     * Get exitPrice
     *
     * @return string
     */
    //public function getExitPrice()
    //{
    //    return $this->exitPrice;
    //}

    /*
     * Get exitWH
     *
     * @return string
     */
    //public function getExitWH()
    //{
    //    return $this->exitWH;
    //}

    /*
     * Get assetItem
     *
     * @return string
     */
    //public function getAssetItem()
    //{
    //    return $this->assetItem;
    //}

    /*
     * Get wasCounted
     *
     * @return string
     */
    //public function getWasCounted()
    //{
    //    return $this->wasCounted;
    //}

    /*
     * Get manSerNum
     *
     * @return string
     */
    //public function getManSerNum()
    //{
    //    return $this->manSerNum;
    //}

    /*
     * Get sHeight1
     *
     * @return string
     */
    //public function getSHeight1()
    //{
    //    return $this->sHeight1;
    //}

    /*
     * Get sHght1Unit
     *
     * @return integer
     */
    //public function getSHght1Unit()
    //{
    //    return $this->sHght1Unit;
    //}

    /*
     * Get sHeight2
     *
     * @return string
     */
    //public function getSHeight2()
    //{
    //    return $this->sHeight2;
    //}

    /*
     * Get sHght2Unit
     *
     * @return integer
     */
    //public function getSHght2Unit()
    //{
    //    return $this->sHght2Unit;
    //}

    /*
     * Get sWidth1
     *
     * @return string
     */
    //public function getSWidth1()
    //{
    //    return $this->sWidth1;
    //}

    /*
     * Get sWdth1Unit
     *
     * @return integer
     */
    //public function getSWdth1Unit()
    //{
    //    return $this->sWdth1Unit;
    //}

    /*
     * Get sWidth2
     *
     * @return string
     */
    //public function getSWidth2()
    //{
    //    return $this->sWidth2;
    //}

    /*
     * Get sWdth2Unit
     *
     * @return integer
     */
    //public function getSWdth2Unit()
    //{
    //    return $this->sWdth2Unit;
    //}

    /*
     * Get sLength1
     *
     * @return string
     */
    //public function getSLength1()
    //{
    //    return $this->sLength1;
    //}

    /*
     * Get sLen1Unit
     *
     * @return integer
     */
    //public function getSLen1Unit()
    //{
    //    return $this->sLen1Unit;
    //}

    /*
     * Get slength2
     *
     * @return string
     */
    //public function getSlength2()
    //{
    //    return $this->slength2;
    //}

    /*
     * Get sLen2Unit
     *
     * @return integer
     */
    //public function getSLen2Unit()
    //{
    //    return $this->sLen2Unit;
    //}

    /*
     * Get sVolume
     *
     * @return string
     */
    //public function getSVolume()
    //{
    //    return $this->sVolume;
    //}

    /*
     * Get sVolUnit
     *
     * @return integer
     */
    //public function getSVolUnit()
    //{
    //    return $this->sVolUnit;
    //}

    /*
     * Get sWeight1
     *
     * @return string
     */
    //public function getSWeight1()
    //{
    //    return $this->sWeight1;
    //}

    /*
     * Get sWght1Unit
     *
     * @return integer
     */
    //public function getSWght1Unit()
    //{
    //    return $this->sWght1Unit;
    //}

    /*
     * Get sWeight2
     *
     * @return string
     */
    //public function getSWeight2()
    //{
    //    return $this->sWeight2;
    //}

    /*
     * Get sWght2Unit
     *
     * @return integer
     */
    //public function getSWght2Unit()
    //{
    //    return $this->sWght2Unit;
    //}

    /*
     * Get bHeight1
     *
     * @return string
     */
    //public function getBHeight1()
    //{
    //    return $this->bHeight1;
    //}

    /*
     * Get bHght1Unit
     *
     * @return integer
     */
    //public function getBHght1Unit()
    //{
    //    return $this->bHght1Unit;
    //}

    /*
     * Get bHeight2
     *
     * @return string
     */
    //public function getBHeight2()
    //{
    //    return $this->bHeight2;
    //}

    /*
     * Get bHght2Unit
     *
     * @return integer
     */
    //public function getBHght2Unit()
    //{
    //    return $this->bHght2Unit;
    //}

    /*
     * Get bWidth1
     *
     * @return string
     */
    //public function getBWidth1()
    //{
    //    return $this->bWidth1;
    //}

    /*
     * Get bWdth1Unit
     *
     * @return integer
     */
    //public function getBWdth1Unit()
    //{
    //    return $this->bWdth1Unit;
    //}

    /*
     * Get bWidth2
     *
     * @return string
     */
    //public function getBWidth2()
    //{
    //    return $this->bWidth2;
    //}

    /*
     * Get bWdth2Unit
     *
     * @return integer
     */
    //public function getBWdth2Unit()
    //{
    //    return $this->bWdth2Unit;
    //}

    /*
     * Get bLength1
     *
     * @return string
     */
    //public function getBLength1()
    //{
    //    return $this->bLength1;
    //}

    /*
     * Get bLen1Unit
     *
     * @return integer
     */
    //public function getBLen1Unit()
    //{
    //    return $this->bLen1Unit;
    //}

    /*
     * Get blength2
     *
     * @return string
     */
    //public function getBlength2()
    //{
    //    return $this->blength2;
    //}

    /*
     * Get bLen2Unit
     *
     * @return integer
     */
    //public function getBLen2Unit()
    //{
    //    return $this->bLen2Unit;
    //}

    /*
     * Get bVolume
     *
     * @return string
     */
    //public function getBVolume()
    //{
    //    return $this->bVolume;
    //}

    /*
     * Get bVolUnit
     *
     * @return integer
     */
    //public function getBVolUnit()
    //{
    //    return $this->bVolUnit;
    //}

    /*
     * Get bWeight1
     *
     * @return string
     */
    //public function getBWeight1()
    //{
    //    return $this->bWeight1;
    //}

    /*
     * Get bWght1Unit
     *
     * @return integer
     */
    //public function getBWght1Unit()
    //{
    //    return $this->bWght1Unit;
    //}

    /*
     * Get bWeight2
     *
     * @return string
     */
    //public function getBWeight2()
    //{
    //    return $this->bWeight2;
    //}

    /*
     * Get bWght2Unit
     *
     * @return integer
     */
    //public function getBWght2Unit()
    //{
    //    return $this->bWght2Unit;
    //}

    /*
     * Get fixCurrCms
     *
     * @return string
     */
    //public function getFixCurrCms()
    //{
    //    return $this->fixCurrCms;
    //}

    /*
     * Get firmCode
     *
     * @return integer
     */
    //public function getFirmCode()
    //{
    //    return $this->firmCode;
    //}

    /*
     * Get lstSalDate
     *
     * @return \DateTime
     */
    //public function getLstSalDate()
    //{
    //    return $this->lstSalDate;
    //}

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /*
     * Get exportCode
     *
     * @return string
     */
    //public function getExportCode()
    //{
    //    return $this->exportCode;
    //}

    /*
     * Get salFactor1
     *
     * @return string
     */
    //public function getSalFactor1()
    //{
    //    return $this->salFactor1;
    //}

    /*
     * Get salFactor2
     *
     * @return string
     */
    //public function getSalFactor2()
    //{
    //    return $this->salFactor2;
    //}

    /*
     * Get salFactor3
     *
     * @return string
     */
    //public function getSalFactor3()
    //{
    //    return $this->salFactor3;
    //}

    /*
     * Get salFactor4
     *
     * @return string
     */
    //public function getSalFactor4()
    //{
    //    return $this->salFactor4;
    //}

    /*
     * Get purFactor1
     *
     * @return string
     */
    //public function getPurFactor1()
    //{
    //    return $this->purFactor1;
    //}

    /*
     * Get purFactor2
     *
     * @return string
     */
    //public function getPurFactor2()
    //{
    //    return $this->purFactor2;
    //}

    /*
     * Get purFactor3
     *
     * @return string
     */
    //public function getPurFactor3()
    //{
    //    return $this->purFactor3;
    //}

    /*
     * Get purFactor4
     *
     * @return string
     */
    //public function getPurFactor4()
    //{
    //    return $this->purFactor4;
    //}

    /*
     * Get salFormula
     *
     * @return string
     */
    //public function getSalFormula()
    //{
    //    return $this->salFormula;
    //}

    /*
     * Get purFormula
     *
     * @return string
     */
    //public function getPurFormula()
    //{
    //    return $this->purFormula;
    //}

    /*
     * Get vatGroupPu
     *
     * @return string
     */
    //public function getVatGroupPu()
    //{
    //    return $this->vatGroupPu;
    //}

    /**
     * Get avgPrice
     *
     * @return string
     */
    public function getAvgPrice()
    {
        return $this->avgPrice;
    }

    /*
     * Get purPackMsr
     *
     * @return string
     */
    //public function getPurPackMsr()
    //{
    //    return $this->purPackMsr;
    //}

    /*
     * Get purPackUn
     *
     * @return string
     */
    //public function getPurPackUn()
    //{
    //    return $this->purPackUn;
    //}

    /*
     * Get salPackMsr
     *
     * @return string
     */
    //public function getSalPackMsr()
    //{
    //    return $this->salPackMsr;
    //}

    /*
     * Get salPackUn
     *
     * @return string
     */
    //public function getSalPackUn()
    //{
    //    return $this->salPackUn;
    //}

    /*
     * Get sCNCounter
     *
     * @return integer
     */
    //public function getSCNCounter()
    //{
    //    return $this->sCNCounter;
    //}

    /*
     * Get manBtchNum
     *
     * @return string
     */
    //public function getManBtchNum()
    //{
    //    return $this->manBtchNum;
    //}

    /*
     * Get manOutOnly
     *
     * @return string
     */
    //public function getManOutOnly()
    //{
    //    return $this->manOutOnly;
    //}

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource()
    //{
    //    return $this->dataSource;
    //}

    /**
     * Get validFor
     *
     * @return string
     */
    public function getValidFor()
    {
        return $this->validFor;
    }

    /**
     * is item valid/active?
     * @return boolean
     */
    public function isValid()
    {
        return $this->getValidFor() == 'Y';
    }

    /**
     * is item valid/active?
     * @return boolean
     */
    public function isActive()
    {
        return $this->getValidFor() == 'Y';
    }

    /*
     * Get validFrom
     *
     * @return \DateTime
     */
    //public function getValidFrom()
    //{
    //    return $this->validFrom;
    //}

    /*
     * Get validTo
     *
     * @return \DateTime
     */
    //public function getValidTo()
    //{
    //    return $this->validTo;
    //}

    /*
     * Get frozenFor
     *
     * @return string
     */
    //public function getFrozenFor()
    //{
    //    return $this->frozenFor;
    //}

    /*
     * Get frozenFrom
     *
     * @return \DateTime
     */
    //public function getFrozenFrom()
    //{
    //    return $this->frozenFrom;
    //}

    /*
     * Get frozenTo
     *
     * @return \DateTime
     */
    //public function getFrozenTo()
    //{
    //    return $this->frozenTo;
    //}

    /*
     * Get blockOut
     *
     * @return string
     */
    //public function getBlockOut()
    //{
    //    return $this->blockOut;
    //}

    /*
     * Get validComm
     *
     * @return string
     */
    //public function getValidComm()
    //{
    //    return $this->validComm;
    //}

    /**
     * Get frozenComm
     *
     * @return string
     */
    public function getFrozenComm()
    {
        return $this->frozenComm;
    }

    /*
     * Get logInstanc
     *
     * @return integer
     */
    //public function getLogInstanc()
    //{
    //    return $this->logInstanc;
    //}

    /*
     * Get objType
     *
     * @return string
     */
    //public function getObjType()
    //{
    //    return $this->objType;
    //}

    /**
     * Get sww
     *
     * @return string
     */
    public function getSww()
    {
        return $this->sww;
    }

    /*
     * Get deleted
     *
     * @return string
     */
    //public function getDeleted()
    //{
    //    return $this->deleted;
    //}

    /*
     * Get docEntry
     *
     * @return integer
     */
    //public function getDocEntry()
    //{
    //    return $this->docEntry;
    //}

    /*
     * Get expensAcct
     *
     * @return string
     */
    //public function getExpensAcct()
    //{
    //    return $this->expensAcct;
    //}

    /*
     * Get frgnInAcct
     *
     * @return string
     */
    //public function getFrgnInAcct()
    //{
    //    return $this->frgnInAcct;
    //}

    /*
     * Get shipType
     *
     * @return integer
     */
    //public function getShipType()
    //{
    //    return $this->shipType;
    //}

    /*
     * Get gLMethod
     *
     * @return string
     */
    //public function getGLMethod()
    //{
    //    return $this->gLMethod;
    //}

    /*
     * Get ecInAcct
     *
     * @return string
     */
    //public function getEcInAcct()
    //{
    //    return $this->ecInAcct;
    //}

    /*
     * Get frgnExpAcc
     *
     * @return string
     */
    //public function getFrgnExpAcc()
    //{
    //    return $this->frgnExpAcc;
    //}

    /*
     * Get eCExpAcc
     *
     * @return string
     */
    //public function getECExpAcc()
    //{
    //    return $this->eCExpAcc;
    //}

    /*
     * Get taxType
     *
     * @return string
     */
    //public function getTaxType()
    //{
    //    return $this->taxType;
    //}

    /*
     * Get byWh
     *
     * @return string
     */
    //public function getByWh()
    //{
    //    return $this->byWh;
    //}

    /*
     * Get wtLiable
     *
     * @return string
     */
    //public function getWtLiable()
    //{
    //    return $this->wtLiable;
    //}

    /*
     * Get itemType
     *
     * @return string
     */
    //public function getItemType()
    //{
    //    return $this->itemType;
    //}

    /*
     * Get warrntTmpl
     *
     * @return string
     */
    //public function getWarrntTmpl()
    //{
    //    return $this->warrntTmpl;
    //}

    /*
     * Get baseUnit
     *
     * @return string
     */
    //public function getBaseUnit()
    //{
    //    return $this->baseUnit;
    //}

    /*
     * Get countryOrg
     *
     * @return string
     */
    //public function getCountryOrg()
    //{
    //    return $this->countryOrg;
    //}

    /*
     * Get stockValue
     *
     * @return string
     */
    //public function getStockValue()
    //{
    //    return $this->stockValue;
    //}

    /*
     * Get phantom
     *
     * @return string
     */
    //public function getPhantom()
    //{
    //    return $this->phantom;
    //}

    /*
     * Get issueMthd
     *
     * @return string
     */
    //public function getIssueMthd()
    //{
    //    return $this->issueMthd;
    //}

    /*
     * Get free1
     *
     * @return string
     */
    //public function getFree1()
    //{
    //    return $this->free1;
    //}

    /*
     * Get pricingPrc
     *
     * @return string
     */
    //public function getPricingPrc()
    //{
    //    return $this->pricingPrc;
    //}

    /*
     * Get mngMethod
     *
     * @return string
     */
    //public function getMngMethod()
    //{
    //    return $this->mngMethod;
    //}

    /*
     * Get reorderPnt
     *
     * @return string
     */
    //public function getReorderPnt()
    //{
    //    return $this->reorderPnt;
    //}

    /*
     * Get invntryUom
     *
     * @return string
     */
    //public function getInvntryUom()
    //{
    //    return $this->invntryUom;
    //}

    /*
     * Get planingSys
     *
     * @return string
     */
    //public function getPlaningSys()
    //{
    //    return $this->planingSys;
    //}

    /*
     * Get prcrmntMtd
     *
     * @return string
     */
    //public function getPrcrmntMtd()
    //{
    //    return $this->prcrmntMtd;
    //}

    /*
     * Get ordrIntrvl
     *
     * @return integer
     */
    //public function getOrdrIntrvl()
    //{
    //    return $this->ordrIntrvl;
    //}

    /*
     * Get ordrMulti
     *
     * @return string
     */
    //public function getOrdrMulti()
    //{
    //    return $this->ordrMulti;
    //}

    /*
     * Get minOrdrQty
     *
     * @return string
     */
    //public function getMinOrdrQty()
    //{
    //    return $this->minOrdrQty;
    //}

    /*
     * Get leadTime
     *
     * @return integer
     */
    //public function getLeadTime()
    //{
    //    return $this->leadTime;
    //}

    /*
     * Get indirctTax
     *
     * @return string
     */
    //public function getIndirctTax()
    //{
    //    return $this->indirctTax;
    //}

    /*
     * Get taxCodeAR
     *
     * @return string
     */
    //public function getTaxCodeAR()
    //{
    //    return $this->taxCodeAR;
    //}

    /*
     * Get taxCodeAP
     *
     * @return string
     */
    //public function getTaxCodeAP()
    //{
    //    return $this->taxCodeAP;
    //}

    /*
     * Get oSvcCode
     *
     * @return integer
     */
    //public function getOSvcCode()
    //{
    //    return $this->oSvcCode;
    //}

    /*
     * Get iSvcCode
     *
     * @return integer
     */
    //public function getISvcCode()
    //{
    //    return $this->iSvcCode;
    //}

    /*
     * Get serviceGrp
     *
     * @return integer
     */
    //public function getServiceGrp()
    //{
    //    return $this->serviceGrp;
    //}

    /*
     * Get ncmCode
     *
     * @return integer
     */
    //public function getNcmCode()
    //{
    //    return $this->ncmCode;
    //}

    /*
     * Get matType
     *
     * @return string
     */
    //public function getMatType()
    //{
    //    return $this->matType;
    //}

    /*
     * Get matGrp
     *
     * @return integer
     */
    //public function getMatGrp()
    //{
    //    return $this->matGrp;
    //}

    /*
     * Get productSrc
     *
     * @return string
     */
    //public function getProductSrc()
    //{
    //    return $this->productSrc;
    //}

    /*
     * Get serviceCtg
     *
     * @return integer
     */
    //public function getServiceCtg()
    //{
    //    return $this->serviceCtg;
    //}

    /*
     * Get itemClass
     *
     * @return string
     */
    //public function getItemClass()
    //{
    //    return $this->itemClass;
    //}

    /*
     * Get excisable
     *
     * @return string
     */
    //public function getExcisable()
    //{
    //    return $this->excisable;
    //}

    /*
     * Get chapterID
     *
     * @return integer
     */
    //public function getChapterID()
    //{
    //    return $this->chapterID;
    //}

    /*
     * Get notifyASN
     *
     * @return string
     */
    //public function getNotifyASN()
    //{
    //    return $this->notifyASN;
    //}

    /*
     * Get proAssNum
     *
     * @return string
     */
    //public function getProAssNum()
    //{
    //    return $this->proAssNum;
    //}

    /*
     * Get assblValue
     *
     * @return string
     */
    //public function getAssblValue()
    //{
    //    return $this->assblValue;
    //}

    /*
     * Get dnfEntry
     *
     * @return integer
     */
    //public function getDnfEntry()
    //{
    //    return $this->dnfEntry;
    //}

    /*
     * Get userSign2
     *
     * @return integer
     */
    //public function getUserSign2()
    //{
    //    return $this->userSign2;
    //}

    /*
     * Get spec
     *
     * @return string
     */
    //public function getSpec()
    //{
    //    return $this->spec;
    //}

    /*
     * Get taxCtg
     *
     * @return string
     */
    //public function getTaxCtg()
    //{
    //    return $this->taxCtg;
    //}

    /*
     * Get series
     *
     * @return integer
     */
    //public function getSeries()
    //{
    //    return $this->series;
    //}

    /*
     * Get number
     *
     * @return integer
     */
    //public function getNumber()
    //{
    //    return $this->number;
    //}

    /*
     * Get fuelCode
     *
     * @return integer
     */
    //public function getFuelCode()
    //{
    //    return $this->fuelCode;
    //}

    /*
     * Get beverTblC
     *
     * @return string
     */
    //public function getBeverTblC()
    //{
    //    return $this->beverTblC;
    //}

    /*
     * Get beverGrpC
     *
     * @return string
     */
    //public function getBeverGrpC()
    //{
    //    return $this->beverGrpC;
    //}

    /*
     * Get beverTM
     *
     * @return integer
     */
    //public function getBeverTM()
    //{
    //    return $this->beverTM;
    //}

    /*
     * Get attachment
     *
     * @return string
     */
    //public function getAttachment()
    //{
    //    return $this->attachment;
    //}

    /*
     * Get atcEntry
     *
     * @return integer
     */
    //public function getAtcEntry()
    //{
    //    return $this->atcEntry;
    //}

    /*
     * Get toleranDay
     *
     * @return integer
     */
    //public function getToleranDay()
    //{
    //    return $this->toleranDay;
    //}

    /*
     * Get ugpEntry
     *
     * @return integer
     */
    //public function getUgpEntry()
    //{
    //    return $this->ugpEntry;
    //}

    /*
     * Get pUoMEntry
     *
     * @return integer
     */
    //public function getPUoMEntry()
    //{
    //    return $this->pUoMEntry;
    //}

    /*
     * Get sUoMEntry
     *
     * @return integer
     */
    //public function getSUoMEntry()
    //{
    //    return $this->sUoMEntry;
    //}

    /*
     * Get iUoMEntry
     *
     * @return integer
     */
    //public function getIUoMEntry()
    //{
    //    return $this->iUoMEntry;
    //}

    /*
     * Get issuePriBy
     *
     * @return integer
     */
    //public function getIssuePriBy()
    //{
    //    return $this->issuePriBy;
    //}

    /*
     * Get assetClass
     *
     * @return string
     */
    //public function getAssetClass()
    //{
    //    return $this->assetClass;
    //}

    /*
     * Get assetGroup
     *
     * @return string
     */
    //public function getAssetGroup()
    //{
    //    return $this->assetGroup;
    //}

    /*
     * Get inventryNo
     *
     * @return string
     */
    //public function getInventryNo()
    //{
    //    return $this->inventryNo;
    //}

    /*
     * Get technician
     *
     * @return integer
     */
    //public function getTechnician()
    //{
    //    return $this->technician;
    //}

    /*
     * Get employee
     *
     * @return integer
     */
    //public function getEmployee()
    //{
    //    return $this->employee;
    //}

    /*
     * Get location
     *
     * @return integer
     */
    //public function getLocation()
    //{
    //    return $this->location;
    //}

    /*
     * Get statAsset
     *
     * @return string
     */
    //public function getStatAsset()
    //{
    //    return $this->statAsset;
    //}

    /*
     * Get cession
     *
     * @return string
     */
    //public function getCession()
    //{
    //    return $this->cession;
    //}

    /*
     * Get deacAftUL
     *
     * @return string
     */
    //public function getDeacAftUL()
    //{
    //    return $this->deacAftUL;
    //}

    /*
     * Get asstStatus
     *
     * @return string
     */
    //public function getAsstStatus()
    //{
    //    return $this->asstStatus;
    //}

    /*
     * Get capDate
     *
     * @return \DateTime
     */
    //public function getCapDate()
    //{
    //    return $this->capDate;
    //}

    /*
     * Get acqDate
     *
     * @return \DateTime
     */
    //public function getAcqDate()
    //{
    //    return $this->acqDate;
    //}

    /*
     * Get retDate
     *
     * @return \DateTime
     */
    //public function getRetDate()
    //{
    //    return $this->retDate;
    //}

    /*
     * Get gLPickMeth
     *
     * @return string
     */
    //public function getGLPickMeth()
    //{
    //    return $this->gLPickMeth;
    //}

    /*
     * Get noDiscount
     *
     * @return string
     */
    //public function getNoDiscount()
    //{
    //    return $this->noDiscount;
    //}

    /*
     * Get mgrByQty
     *
     * @return string
     */
    //public function getMgrByQty()
    //{
    //    return $this->mgrByQty;
    //}

    /*
     * Get assetRmk1
     *
     * @return string
     */
    //public function getAssetRmk1()
    //{
    //    return $this->assetRmk1;
    //}

    /*
     * Get assetRmk2
     *
     * @return string
     */
    //public function getAssetRmk2()
    //{
    //    return $this->assetRmk2;
    //}

    /*
     * Get assetAmnt1
     *
     * @return string
     */
    //public function getAssetAmnt1()
    //{
    //    return $this->assetAmnt1;
    //}

    /*
     * Get assetAmnt2
     *
     * @return string
     */
    //public function getAssetAmnt2()
    //{
    //    return $this->assetAmnt2;
    //}

    /*
     * Get deprGroup
     *
     * @return string
     */
    //public function getDeprGroup()
    //{
    //    return $this->deprGroup;
    //}

    /*
     * Get assetSerNo
     *
     * @return string
     */
    //public function getAssetSerNo()
    //{
    //    return $this->assetSerNo;
    //}

    /*
     * Get cntUnitMsr
     *
     * @return string
     */
    //public function getCntUnitMsr()
    //{
    //    return $this->cntUnitMsr;
    //}

    /*
     * Get numInCnt
     *
     * @return string
     */
    //public function getNumInCnt()
    //{
    //    return $this->numInCnt;
    //}

    /*
     * Get iNUoMEntry
     *
     * @return integer
     */
    //public function getINUoMEntry()
    //{
    //    return $this->iNUoMEntry;
    //}

    /*
     * Get oneBOneRec
     *
     * @return string
     */
    //public function getOneBOneRec()
    //{
    //    return $this->oneBOneRec;
    //}

    /*
     * Get ruleCode
     *
     * @return string
     */
    //public function getRuleCode()
    //{
    //    return $this->ruleCode;
    //}

    /*
     * Get scsCode
     *
     * @return string
     */
    //public function getScsCode()
    //{
    //    return $this->scsCode;
    //}

    /*
     * Get spProdType
     *
     * @return string
     */
    //public function getSpProdType()
    //{
    //    return $this->spProdType;
    //}

    /*
     * Get iWeight1
     *
     * @return string
     */
    //public function getIWeight1()
    //{
    //    return $this->iWeight1;
    //}

    /*
     * Get iWght1Unit
     *
     * @return integer
     */
    //public function getIWght1Unit()
    //{
    //    return $this->iWght1Unit;
    //}

    /*
     * Get iWeight2
     *
     * @return string
     */
    //public function getIWeight2()
    //{
    //    return $this->iWeight2;
    //}

    /*
     * Get iWght2Unit
     *
     * @return integer
     */
    //public function getIWght2Unit()
    //{
    //    return $this->iWght2Unit;
    //}

    /*
     * Get compoWH
     *
     * @return string
     */
    //public function getCompoWH()
    //{
    //    return $this->compoWH;
    //}
}
