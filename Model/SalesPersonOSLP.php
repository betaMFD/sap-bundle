<?php

namespace BetaMFD\SAPBundle\Model;

use BetaMFD\SAPBundle\Model\SalesPersonOSLPInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * SalesPersonOSLP
 *
 * @ORM\Table(name="OSLP")
 * @ORM\Entity(readOnly=true)
 */
abstract class SalesPersonOSLP implements SalesPersonOSLPInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="SlpCode", type="integer")
     * @ORM\Id
     */
    protected $slpCode;

    /**
     * @var string
     *
     * @ORM\Column(name="SlpName", type="string", length=155, nullable=false)
     */
    protected $slpName;

    /*
     * @var string
     *
     * @ORM\Column(name="Memo", type="string", length=50, nullable=true)
     */
    //protected $memo;

    /*
     * @var string
     *
     * @ORM\Column(name="Commission", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $commission;

    /*
     * @var integer
     *
     * @ORM\Column(name="GroupCode", type="integer", nullable=true)
     */
    //protected $groupCode;

    /*
     * @var string
     *
     * @ORM\Column(name="Locked", type="string", length=1, nullable=true)
     */
    //protected $locked;

    /*
     * @var string
     *
     * @ORM\Column(name="DataSource", type="string", length=1, nullable=true)
     */
    //protected $dataSource;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign", type="integer", nullable=true)
     */
    //protected $userSign;

    /*
     * @var integer
     *
     * @ORM\Column(name="EmpID", type="integer", nullable=true)
     */
    //protected $empID;

    /*
     * @var string
     *
     * @ORM\Column(name="Active", type="string", length=1, nullable=true)
     */
    //protected $active;

    /*
     * @var string
     *
     * @ORM\Column(name="Telephone", type="string", length=20, nullable=true)
     */
    //protected $telephone;

    /*
     * @var string
     *
     * @ORM\Column(name="Mobil", type="string", length=50, nullable=true)
     */
    //protected $mobile;

    /*
     * @var string
     *
     * @ORM\Column(name="Fax", type="string", length=20, nullable=true)
     */
    //protected $fax;

    /*
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=100, nullable=true)
     */
    //protected $email;


    /**
     * Constructor
     */
    protected function __construct() {}

    public function __toString()
    {
        return $this->slpName;
    }

    /**
     * Get slpCode
     *
     * @return integer
     */
    public function getSlpCode()
    {
        return $this->slpCode;
    }

    /**
     * Get slpName
     *
     * @return string
     */
    public function getSlpName()
    {
        return $this->slpName;
    }

    /**
     * Get slpName
     *
     * @return string
     */
    public function getName()
    {
        return $this->getSlpName();
    }

    /*
     * Get memo
     *
     * @return string
     */
    //public function getMemo()
    //{
    //    return $this->memo;
    //}

    /*
     * Get commission
     *
     * @return string
     */
    //public function getCommission()
    //{
    //    return $this->commission;
    //}

    /*
     * Get groupCode
     *
     * @return integer
     */
    //public function getGroupCode()
    //{
    //    return $this->groupCode;
    //}

    /*
     * Get locked
     *
     * @return string
     */
    //public function getLocked()
    //{
    //    return $this->locked;
    //}

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource()
    //{
    //    return $this->dataSource;
    //}

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign()
    //{
    //    return $this->userSign;
    //}

    /*
     * Get empID
     *
     * @return integer
     */
    //public function getEmpID()
    //{
    //    return $this->empID;
    //}

    /*
     * Get active
     *
     * @return string
     */
    //public function getActive()
    //{
    //    return $this->active;
    //}

    /*
     * Get telephone
     *
     * @return string
     */
    //public function getTelephone()
    //{
    //    return $this->telephone;
    //}

    /*
     * Get mobil
     *
     * @return string
     */
    //public function getMobile()
    //{
    //    return $this->mobile;
    //}

    /*
     * Get fax
     *
     * @return string
     */
    //public function getFax()
    //{
    //    return $this->fax;
    //}

    /*
     * Get email
     *
     * @return string
     */
    //public function getEmail()
    //{
    //    return $this->email;
    //}
}
