<?php

namespace BetaMFD\SAPBundle\Model;


interface PaymentTermOCTGInterface
{
    public function __toString();

    /**
     * Get groupNum
     *
     * @return integer
     */
    public function getGroupNum();

    /**
     * Get paymentGroup
     *
     * @return string
     */
    public function getPaymentGroup();

    /*
     * Get payDueMonth
     *
     * @return string
     */
    //public function getPayDueMonth();

    /*
     * Get extraMonth
     *
     * @return integer
     */
    //public function getExtraMonth();

    /**
     * Get extraDays
     *
     * @return integer
     */
    public function getExtraDays();

    /*
     * Get paymentsNum
     *
     * @return integer
     */
    //public function getPaymentsNum();

    /*
     * Get creditLimit
     *
     * @return string
     */
    //public function getCreditLimit();

    /*
     * Get volumeDiscount
     *
     * @return string
     */
    //public function getVolumeDiscount();

    /*
     * Get latePaymentCharge
     *
     * @return string
     */
    //public function getLatePaymentCharge();

    /*
     * Get obligLimit
     *
     * @return string
     */
    //public function getObligLimit();

    /*
     * Get listNum
     *
     * @return integer
     */
    //public function getListNum();

    /*
     * Get payments
     *
     * @return string
     */
    //public function getPayments();

    /*
     * Get numOfPayments
     *
     * @return integer
     */
    //public function getNumOfPayments();

    /*
     * Get payment1
     *
     * @return string
     */
    //public function getPayment1();

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource();

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign();

    /*
     * Get openRcpt
     *
     * @return string
     */
    //public function getOpenRcpt();

    /*
     * Get discCode
     *
     * @return string
     */
    //public function getDiscCode();

    /*
     * Get dunningCode
     *
     * @return string
     */
    //public function getDunningCode();

    /*
     * Get baslineDate
     *
     * @return string
     */
    //public function getBaslineDate();

    /*
     * Get instNum
     *
     * @return integer
     */
    //public function getInstNum();

    /*
     * Get tolDays
     *
     * @return integer
     */
    //public function getTolDays();

    /*
     * Get vatFirst
     *
     * @return string
     */
    //public function getVatFirst();

    /*
     * Get creditMethod
     *
     * @return string
     */
    //public function getCreditMethod();
}
