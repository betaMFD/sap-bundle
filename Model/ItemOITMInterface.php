<?php

namespace BetaMFD\SAPBundle\Model;

interface ItemOITMInterface
{
    public function __toString();

    /**
     * Get itemCode
     *
     * @return string
     */
    public function getItemCode();

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName();

    /*
     * Get frgnName
     *
     * @return string
     */
    //public function getFrgnName();

    /*
     * Get itmsGrpCod
     *
     * @return integer
     */
    //public function getItmsGrpCod();

    /*
     * Get cstGrpCode
     *
     * @return integer
     */
    //public function getCstGrpCode();

    /*
     * Get vatGourpSa
     *
     * @return string
     */
    //public function getVatGourpSa();

    /*
     * Get codeBars
     *
     * @return string
     */
    //public function getCodeBars();

    /*
     * Get vatLiable
     *
     * @return string
     */
    //public function getVatLiable();

    /*
     * Get prchseItem
     *
     * @return string
     */
    //public function getPrchseItem();

    /*
     * Get sellItem
     *
     * @return string
     */
    //public function getSellItem();

    /*
     * Get invntItem
     *
     * @return string
     */
    //public function getInvntItem();

    /**
     * Get onHand
     *
     * @return string
     */
    public function getOnHand();

    /**
     * Get isCommited
     *
     * @return string
     */
    public function getIsCommited();

    /**
     * Get isCommited
     *
     * @return string
     */
    public function getIsCommitted();

    /**
     * Is this item Committed?
     *
     * @return boolean
     */
    public function isCommitted();

    /**
     * Get onOrder
     *
     * @return string
     */
    public function getOnOrder();

    /*
     * Get incomeAcct
     *
     * @return string
     */
    //public function getIncomeAcct();

    /*
     * Get exmptIncom
     *
     * @return string
     */
    //public function getExmptIncom();

    /**
     * Get maxLevel
     *
     * @return string
     */
    public function getMaxLevel();

    /**
     * Get dfltWH
     *
     * @return string
     */
    public function getDfltWH();

    /**
     * Get cardCode
     *
     * @return string
     */
    public function getCardCode();

    /**
     * Get suppCatNum
     *
     * @return string
     */
    public function getSuppCatNum();

    /*
     * Get buyUnitMsr
     *
     * @return string
     */
    //public function getBuyUnitMsr();

    /*
     * Get numInBuy
     *
     * @return string
     */
    //public function getNumInBuy();

    /*
     * Get reorderQty
     *
     * @return string
     */
    //public function getReorderQty();

    /**
     * Get minLevel
     *
     * @return string
     */
    public function getMinLevel();

    /**
     * Get lstEvlPric
     *
     * @return string
     */
    public function getLstEvlPric();

    /*
     * Get lstEvlDate
     *
     * @return \DateTime
     */
    //public function getLstEvlDate();

    /*
     * Get customPer
     *
     * @return string
     */
    //public function getCustomPer();

    /*
     * Get canceled
     *
     * @return string
     */
    //public function getCanceled();

    /*
     * Get mnufctTime
     *
     * @return integer
     */
    //public function getMnufctTime();

    /*
     * Get wholSlsTax
     *
     * @return string
     */
    //public function getWholSlsTax();

    /*
     * Get retilrTax
     *
     * @return string
     */
    //public function getRetilrTax();

    /*
     * Get spcialDisc
     *
     * @return string
     */
    //public function getSpcialDisc();

    /*
     * Get dscountCod
     *
     * @return integer
     */
    //public function getDscountCod();

    /*
     * Get trackSales
     *
     * @return string
     */
    //public function getTrackSales();

    /*
     * Get salUnitMsr
     *
     * @return string
     */
    //public function getSalUnitMsr();

    /*
     * Get numInSale
     *
     * @return string
     */
    //public function getNumInSale();

    /*
     * Get consig
     *
     * @return string
     */
    //public function getConsig();

    /*
     * Get queryGroup
     *
     * @return integer
     */
    //public function getQueryGroup();

    /*
     * Get counted
     *
     * @return string
     */
    //public function getCounted();

    /*
     * Get openBlnc
     *
     * @return string
     */
    //public function getOpenBlnc();

    /*
     * Get evalSystem
     *
     * @return string
     */
    //public function getEvalSystem();

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign();

    /*
     * Get free
     *
     * @return string
     */
    //public function getFree();

    /*
     * Get picturName
     *
     * @return string
     */
    //public function getPicturName();

    /*
     * Get transfered
     *
     * @return string
     */
    //public function getTransfered();

    /*
     * Get blncTrnsfr
     *
     * @return string
     */
    //public function getBlncTrnsfr();

    /*
     * Get userText
     *
     * @return string
     */
    //public function getUserText();

    /*
     * Get serialNum
     *
     * @return string
     */
    //public function getSerialNum();

    /*
     * Get commisPcnt
     *
     * @return string
     */
    //public function getCommisPcnt();

    /*
     * Get commisSum
     *
     * @return string
     */
    //public function getCommisSum();

    /*
     * Get commisGrp
     *
     * @return integer
     */
    //public function getCommisGrp();

    /*
     * Get treeType
     *
     * @return string
     */
    //public function getTreeType();

    /*
     * Get treeQty
     *
     * @return string
     */
    //public function getTreeQty();

    /**
     * Get lastPurPrc
     *
     * @return string
     */
    public function getLastPurPrc();

    /*
     * Get lastPurCur
     *
     * @return string
     */
    //public function getLastPurCur();

    /*
     * Get lastPurDat
     *
     * @return \DateTime
     */
    //public function getLastPurDat();

    /*
     * Get exitCur
     *
     * @return string
     */
    //public function getExitCur();

    /*
     * Get exitPrice
     *
     * @return string
     */
    //public function getExitPrice();

    /*
     * Get exitWH
     *
     * @return string
     */
    //public function getExitWH();

    /*
     * Get assetItem
     *
     * @return string
     */
    //public function getAssetItem();

    /*
     * Get wasCounted
     *
     * @return string
     */
    //public function getWasCounted();

    /*
     * Get manSerNum
     *
     * @return string
     */
    //public function getManSerNum();

    /*
     * Get sHeight1
     *
     * @return string
     */
    //public function getSHeight1();

    /*
     * Get sHght1Unit
     *
     * @return integer
     */
    //public function getSHght1Unit();

    /*
     * Get sHeight2
     *
     * @return string
     */
    //public function getSHeight2();

    /*
     * Get sHght2Unit
     *
     * @return integer
     */
    //public function getSHght2Unit();

    /*
     * Get sWidth1
     *
     * @return string
     */
    //public function getSWidth1();

    /*
     * Get sWdth1Unit
     *
     * @return integer
     */
    //public function getSWdth1Unit();

    /*
     * Get sWidth2
     *
     * @return string
     */
    //public function getSWidth2();

    /*
     * Get sWdth2Unit
     *
     * @return integer
     */
    //public function getSWdth2Unit();

    /*
     * Get sLength1
     *
     * @return string
     */
    //public function getSLength1();

    /*
     * Get sLen1Unit
     *
     * @return integer
     */
    //public function getSLen1Unit();

    /*
     * Get slength2
     *
     * @return string
     */
    //public function getSlength2();

    /*
     * Get sLen2Unit
     *
     * @return integer
     */
    //public function getSLen2Unit();

    /*
     * Get sVolume
     *
     * @return string
     */
    //public function getSVolume();

    /*
     * Get sVolUnit
     *
     * @return integer
     */
    //public function getSVolUnit();

    /*
     * Get sWeight1
     *
     * @return string
     */
    //public function getSWeight1();

    /*
     * Get sWght1Unit
     *
     * @return integer
     */
    //public function getSWght1Unit();

    /*
     * Get sWeight2
     *
     * @return string
     */
    //public function getSWeight2();

    /*
     * Get sWght2Unit
     *
     * @return integer
     */
    //public function getSWght2Unit();

    /*
     * Get bHeight1
     *
     * @return string
     */
    //public function getBHeight1();

    /*
     * Get bHght1Unit
     *
     * @return integer
     */
    //public function getBHght1Unit();

    /*
     * Get bHeight2
     *
     * @return string
     */
    //public function getBHeight2();

    /*
     * Get bHght2Unit
     *
     * @return integer
     */
    //public function getBHght2Unit();

    /*
     * Get bWidth1
     *
     * @return string
     */
    //public function getBWidth1();

    /*
     * Get bWdth1Unit
     *
     * @return integer
     */
    //public function getBWdth1Unit();

    /*
     * Get bWidth2
     *
     * @return string
     */
    //public function getBWidth2();

    /*
     * Get bWdth2Unit
     *
     * @return integer
     */
    //public function getBWdth2Unit();

    /*
     * Get bLength1
     *
     * @return string
     */
    //public function getBLength1();

    /*
     * Get bLen1Unit
     *
     * @return integer
     */
    //public function getBLen1Unit();

    /*
     * Get blength2
     *
     * @return string
     */
    //public function getBlength2();

    /*
     * Get bLen2Unit
     *
     * @return integer
     */
    //public function getBLen2Unit();

    /*
     * Get bVolume
     *
     * @return string
     */
    //public function getBVolume();

    /*
     * Get bVolUnit
     *
     * @return integer
     */
    //public function getBVolUnit();

    /*
     * Get bWeight1
     *
     * @return string
     */
    //public function getBWeight1();

    /*
     * Get bWght1Unit
     *
     * @return integer
     */
    //public function getBWght1Unit();

    /*
     * Get bWeight2
     *
     * @return string
     */
    //public function getBWeight2();

    /*
     * Get bWght2Unit
     *
     * @return integer
     */
    //public function getBWght2Unit();

    /*
     * Get fixCurrCms
     *
     * @return string
     */
    //public function getFixCurrCms();

    /*
     * Get firmCode
     *
     * @return integer
     */
    //public function getFirmCode();

    /*
     * Get lstSalDate
     *
     * @return \DateTime
     */
    //public function getLstSalDate();

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate();

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate();

    /*
     * Get exportCode
     *
     * @return string
     */
    //public function getExportCode();

    /*
     * Get salFactor1
     *
     * @return string
     */
    //public function getSalFactor1();

    /*
     * Get salFactor2
     *
     * @return string
     */
    //public function getSalFactor2();

    /*
     * Get salFactor3
     *
     * @return string
     */
    //public function getSalFactor3();

    /*
     * Get salFactor4
     *
     * @return string
     */
    //public function getSalFactor4();

    /*
     * Get purFactor1
     *
     * @return string
     */
    //public function getPurFactor1();

    /*
     * Get purFactor2
     *
     * @return string
     */
    //public function getPurFactor2();

    /*
     * Get purFactor3
     *
     * @return string
     */
    //public function getPurFactor3();

    /*
     * Get purFactor4
     *
     * @return string
     */
    //public function getPurFactor4();

    /*
     * Get salFormula
     *
     * @return string
     */
    //public function getSalFormula();

    /*
     * Get purFormula
     *
     * @return string
     */
    //public function getPurFormula();

    /*
     * Get vatGroupPu
     *
     * @return string
     */
    //public function getVatGroupPu();

    /**
     * Get avgPrice
     *
     * @return string
     */
    public function getAvgPrice();

    /*
     * Get purPackMsr
     *
     * @return string
     */
    //public function getPurPackMsr();

    /*
     * Get purPackUn
     *
     * @return string
     */
    //public function getPurPackUn();

    /*
     * Get salPackMsr
     *
     * @return string
     */
    //public function getSalPackMsr();

    /*
     * Get salPackUn
     *
     * @return string
     */
    //public function getSalPackUn();

    /*
     * Get sCNCounter
     *
     * @return integer
     */
    //public function getSCNCounter();

    /*
     * Get manBtchNum
     *
     * @return string
     */
    //public function getManBtchNum();

    /*
     * Get manOutOnly
     *
     * @return string
     */
    //public function getManOutOnly();

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource();

    /**
     * Get validFor
     *
     * @return string
     */
    public function getValidFor();

    /**
     * is item valid/active?
     * @return boolean
     */
    public function isValid();

    /**
     * is item valid/active?
     * @return boolean
     */
    public function isActive();

    /*
     * Get validFrom
     *
     * @return \DateTime
     */
    //public function getValidFrom();

    /*
     * Get validTo
     *
     * @return \DateTime
     */
    //public function getValidTo();

    /*
     * Get frozenFor
     *
     * @return string
     */
    //public function getFrozenFor();

    /*
     * Get frozenFrom
     *
     * @return \DateTime
     */
    //public function getFrozenFrom();

    /*
     * Get frozenTo
     *
     * @return \DateTime
     */
    //public function getFrozenTo();

    /*
     * Get blockOut
     *
     * @return string
     */
    //public function getBlockOut();

    /*
     * Get validComm
     *
     * @return string
     */
    //public function getValidComm();

    /*
     * Get frozenComm
     *
     * @return string
     */
    //public function getFrozenComm();

    /*
     * Get logInstanc
     *
     * @return integer
     */
    //public function getLogInstanc();

    /*
     * Get objType
     *
     * @return string
     */
    //public function getObjType();

    /**
     * Get sww
     *
     * @return string
     */
    public function getSww();

    /*
     * Get deleted
     *
     * @return string
     */
    //public function getDeleted();

    /*
     * Get docEntry
     *
     * @return integer
     */
    //public function getDocEntry();

    /*
     * Get expensAcct
     *
     * @return string
     */
    //public function getExpensAcct();

    /*
     * Get frgnInAcct
     *
     * @return string
     */
    //public function getFrgnInAcct();

    /*
     * Get shipType
     *
     * @return integer
     */
    //public function getShipType();

    /*
     * Get gLMethod
     *
     * @return string
     */
    //public function getGLMethod();

    /*
     * Get ecInAcct
     *
     * @return string
     */
    //public function getEcInAcct();

    /*
     * Get frgnExpAcc
     *
     * @return string
     */
    //public function getFrgnExpAcc();

    /*
     * Get eCExpAcc
     *
     * @return string
     */
    //public function getECExpAcc();

    /*
     * Get taxType
     *
     * @return string
     */
    //public function getTaxType();

    /*
     * Get byWh
     *
     * @return string
     */
    //public function getByWh();

    /*
     * Get wtLiable
     *
     * @return string
     */
    //public function getWtLiable();

    /*
     * Get itemType
     *
     * @return string
     */
    //public function getItemType();

    /*
     * Get warrntTmpl
     *
     * @return string
     */
    //public function getWarrntTmpl();

    /*
     * Get baseUnit
     *
     * @return string
     */
    //public function getBaseUnit();

    /*
     * Get countryOrg
     *
     * @return string
     */
    //public function getCountryOrg();

    /*
     * Get stockValue
     *
     * @return string
     */
    //public function getStockValue();

    /*
     * Get phantom
     *
     * @return string
     */
    //public function getPhantom();

    /*
     * Get issueMthd
     *
     * @return string
     */
    //public function getIssueMthd();

    /*
     * Get free1
     *
     * @return string
     */
    //public function getFree1();

    /*
     * Get pricingPrc
     *
     * @return string
     */
    //public function getPricingPrc();

    /*
     * Get mngMethod
     *
     * @return string
     */
    //public function getMngMethod();

    /*
     * Get reorderPnt
     *
     * @return string
     */
    //public function getReorderPnt();

    /*
     * Get invntryUom
     *
     * @return string
     */
    //public function getInvntryUom();

    /*
     * Get planingSys
     *
     * @return string
     */
    //public function getPlaningSys();

    /*
     * Get prcrmntMtd
     *
     * @return string
     */
    //public function getPrcrmntMtd();

    /*
     * Get ordrIntrvl
     *
     * @return integer
     */
    //public function getOrdrIntrvl();

    /*
     * Get ordrMulti
     *
     * @return string
     */
    //public function getOrdrMulti();

    /*
     * Get minOrdrQty
     *
     * @return string
     */
    //public function getMinOrdrQty();

    /*
     * Get leadTime
     *
     * @return integer
     */
    //public function getLeadTime();

    /*
     * Get indirctTax
     *
     * @return string
     */
    //public function getIndirctTax();

    /*
     * Get taxCodeAR
     *
     * @return string
     */
    //public function getTaxCodeAR();

    /*
     * Get taxCodeAP
     *
     * @return string
     */
    //public function getTaxCodeAP();

    /*
     * Get oSvcCode
     *
     * @return integer
     */
    //public function getOSvcCode();

    /*
     * Get iSvcCode
     *
     * @return integer
     */
    //public function getISvcCode();

    /*
     * Get serviceGrp
     *
     * @return integer
     */
    //public function getServiceGrp();

    /*
     * Get ncmCode
     *
     * @return integer
     */
    //public function getNcmCode();

    /*
     * Get matType
     *
     * @return string
     */
    //public function getMatType();

    /*
     * Get matGrp
     *
     * @return integer
     */
    //public function getMatGrp();

    /*
     * Get productSrc
     *
     * @return string
     */
    //public function getProductSrc();

    /*
     * Get serviceCtg
     *
     * @return integer
     */
    //public function getServiceCtg();

    /*
     * Get itemClass
     *
     * @return string
     */
    //public function getItemClass();

    /*
     * Get excisable
     *
     * @return string
     */
    //public function getExcisable();

    /*
     * Get chapterID
     *
     * @return integer
     */
    //public function getChapterID();

    /*
     * Get notifyASN
     *
     * @return string
     */
    //public function getNotifyASN();

    /*
     * Get proAssNum
     *
     * @return string
     */
    //public function getProAssNum();

    /*
     * Get assblValue
     *
     * @return string
     */
    //public function getAssblValue();

    /*
     * Get dnfEntry
     *
     * @return integer
     */
    //public function getDnfEntry();

    /*
     * Get userSign2
     *
     * @return integer
     */
    //public function getUserSign2();

    /*
     * Get spec
     *
     * @return string
     */
    //public function getSpec();

    /*
     * Get taxCtg
     *
     * @return string
     */
    //public function getTaxCtg();

    /*
     * Get series
     *
     * @return integer
     */
    //public function getSeries();

    /*
     * Get number
     *
     * @return integer
     */
    //public function getNumber();

    /*
     * Get fuelCode
     *
     * @return integer
     */
    //public function getFuelCode();

    /*
     * Get beverTblC
     *
     * @return string
     */
    //public function getBeverTblC();

    /*
     * Get beverGrpC
     *
     * @return string
     */
    //public function getBeverGrpC();

    /*
     * Get beverTM
     *
     * @return integer
     */
    //public function getBeverTM();

    /*
     * Get attachment
     *
     * @return string
     */
    //public function getAttachment();

    /*
     * Get atcEntry
     *
     * @return integer
     */
    //public function getAtcEntry();

    /*
     * Get toleranDay
     *
     * @return integer
     */
    //public function getToleranDay();

    /*
     * Get ugpEntry
     *
     * @return integer
     */
    //public function getUgpEntry();

    /*
     * Get pUoMEntry
     *
     * @return integer
     */
    //public function getPUoMEntry();

    /*
     * Get sUoMEntry
     *
     * @return integer
     */
    //public function getSUoMEntry();

    /*
     * Get iUoMEntry
     *
     * @return integer
     */
    //public function getIUoMEntry();

    /*
     * Get issuePriBy
     *
     * @return integer
     */
    //public function getIssuePriBy();

    /*
     * Get assetClass
     *
     * @return string
     */
    //public function getAssetClass();

    /*
     * Get assetGroup
     *
     * @return string
     */
    //public function getAssetGroup();

    /*
     * Get inventryNo
     *
     * @return string
     */
    //public function getInventryNo();

    /*
     * Get technician
     *
     * @return integer
     */
    //public function getTechnician();

    /*
     * Get employee
     *
     * @return integer
     */
    //public function getEmployee();

    /*
     * Get location
     *
     * @return integer
     */
    //public function getLocation();

    /*
     * Get statAsset
     *
     * @return string
     */
    //public function getStatAsset();

    /*
     * Get cession
     *
     * @return string
     */
    //public function getCession();

    /*
     * Get deacAftUL
     *
     * @return string
     */
    //public function getDeacAftUL();

    /*
     * Get asstStatus
     *
     * @return string
     */
    //public function getAsstStatus();

    /*
     * Get capDate
     *
     * @return \DateTime
     */
    //public function getCapDate();

    /*
     * Get acqDate
     *
     * @return \DateTime
     */
    //public function getAcqDate();

    /*
     * Get retDate
     *
     * @return \DateTime
     */
    //public function getRetDate();

    /*
     * Get gLPickMeth
     *
     * @return string
     */
    //public function getGLPickMeth();

    /*
     * Get noDiscount
     *
     * @return string
     */
    //public function getNoDiscount();

    /*
     * Get mgrByQty
     *
     * @return string
     */
    //public function getMgrByQty();

    /*
     * Get assetRmk1
     *
     * @return string
     */
    //public function getAssetRmk1();

    /*
     * Get assetRmk2
     *
     * @return string
     */
    //public function getAssetRmk2();

    /*
     * Get assetAmnt1
     *
     * @return string
     */
    //public function getAssetAmnt1();

    /*
     * Get assetAmnt2
     *
     * @return string
     */
    //public function getAssetAmnt2();

    /*
     * Get deprGroup
     *
     * @return string
     */
    //public function getDeprGroup();

    /*
     * Get assetSerNo
     *
     * @return string
     */
    //public function getAssetSerNo();

    /*
     * Get cntUnitMsr
     *
     * @return string
     */
    //public function getCntUnitMsr();

    /*
     * Get numInCnt
     *
     * @return string
     */
    //public function getNumInCnt();

    /*
     * Get iNUoMEntry
     *
     * @return integer
     */
    //public function getINUoMEntry();

    /*
     * Get oneBOneRec
     *
     * @return string
     */
    //public function getOneBOneRec();

    /*
     * Get ruleCode
     *
     * @return string
     */
    //public function getRuleCode();

    /*
     * Get scsCode
     *
     * @return string
     */
    //public function getScsCode();

    /*
     * Get spProdType
     *
     * @return string
     */
    //public function getSpProdType();

    /*
     * Get iWeight1
     *
     * @return string
     */
    //public function getIWeight1();

    /*
     * Get iWght1Unit
     *
     * @return integer
     */
    //public function getIWght1Unit();

    /*
     * Get iWeight2
     *
     * @return string
     */
    //public function getIWeight2();

    /*
     * Get iWght2Unit
     *
     * @return integer
     */
    //public function getIWght2Unit();

    /*
     * Get compoWH
     *
     * @return string
     */
    //public function getCompoWH();
}
