<?php

namespace BetaMFD\SAPBundle\Model;

interface BusinessPartnerOCRDInterface
{

    /**
     * Get cardCode
     *
     * @return string
     */
    public function getCardCode();

    /**
     * Get cardType
     *
     * @return string
     */
    public function getCardType();

    /**
     * isCustomer
     * @return boolean if OCRD is a Customer
     */
    public function isCustomer();

    /**
     * isSupplier
     * @return boolean if OCRD is a Supplier/Vendor
     */
    public function isSupplier();

    /**
     * isVendor
     * @return boolean if OCRD is a Supplier/Vendor
     */
    public function isVendor();

    /**
     * Get groupCode
     *
     * @return integer
     */
    public function getGroupCode();

    /*
     * Get cmpprotected
     *
     * @return string
     */
    //public function getCmpprotected();

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress();

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode();

    /**
     * Get mailAddres
     *
     * @return string
     */
    public function getMailAddres();

    /**
     * Get mailZipCod
     *
     * @return string
     */
    public function getMailZipCod();

    /**
     * Get phone1
     *
     * @return string
     */
    public function getPhone1();

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2();

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax();

    /**
     * Get cntctPrsn
     *
     * @return string
     */
    public function getCntctPrsn();

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes();

    /**
     * Get balance
     *
     * @return string
     */
    public function getBalance();

    /**
     * Get Amount Over Credit Limit
     * @return string
     */
    public function getAmountOverCreditLimit();

    /*
     * Get checksBal
     *
     * @return string
     */
    //public function getChecksBal();

    /*
     * Get dNotesBal
     *
     * @return string
     */
    //public function getDNotesBal();

    /*
     * Get ordersBal
     *
     * @return string
     */
    //public function getOrdersBal();

    /**
     * Get groupNum
     *
     * @return integer
     */
    public function getGroupNum();

    /**
     * Get creditLine
     *
     * @return string
     */
    public function getCreditLine();

    /*
     * Get debtLine
     *
     * @return string
     */
    //public function getDebtLine();

    /*
     * Get discount
     *
     * @return string
     */
    //public function getDiscount();

    /*
     * Get vatStatus
     *
     * @return string
     */
    //public function getVatStatus();

    /*
     * Get licTradNum
     *
     * @return string
     */
    //public function getLicTradNum();

    /*
     * Get ddctStatus
     *
     * @return string
     */
    //public function getDdctStatus();

    /*
     * Get ddctPrcnt
     *
     * @return string
     */
    //public function getDdctPrcnt();

    /*
     * Get validUntil
     *
     * @return \DateTime
     */
    //public function getValidUntil();

    /*
     * Get chrctrstcs
     *
     * @return integer
     */
    //public function getChrctrstcs();

    /*
     * Get exMatchNum
     *
     * @return integer
     */
    //public function getExMatchNum();

    /*
     * Get inMatchNum
     *
     * @return integer
     */
    //public function getInMatchNum();

    /**
     * Get listNum
     *
     * @return integer
     */
    public function getListNum();

    /*
     * Get dNoteBalFC
     *
     * @return string
     */
    //public function getDNoteBalFC();

    /*
     * Get orderBalFC
     *
     * @return string
     */
    //public function getOrderBalFC();

    /*
     * Get dNoteBalSy
     *
     * @return string
     */
    //public function getDNoteBalSy();

    /*
     * Get orderBalSy
     *
     * @return string
     */
    //public function getOrderBalSy();

    /*
     * Get transfered
     *
     * @return string
     */
    //public function getTransfered();

    /*
     * Get balTrnsfrd
     *
     * @return string
     */
    //public function getBalTrnsfrd();

    /*
     * Get intrstRate
     *
     * @return string
     */
    //public function getIntrstRate();

    /*
     * Get commission
     *
     * @return string
     */
    //public function getCommission();

    /*
     * Get commGrCode
     *
     * @return integer
     */
    //public function getCommGrCode();

    /*
     * Get freeText
     *
     * @return string
     */
    //public function getFreeText();

    /**
     * Get slpCode
     *
     * @return integer
     */
    public function getSlpCode();

    /*
     * Get prevYearAc
     *
     * @return string
     */
    //public function getPrevYearAc();

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency();

    /*
     * Get rateDifAct
     *
     * @return string
     */
    //public function getRateDifAct();

    /*
     * Get balanceSys
     *
     * @return string
     */
    //public function getBalanceSys();

    /*
     * Get balanceFC
     *
     * @return string
     */
    //public function getBalanceFC();

    /*
     * Get protected
     *
     * @return string
     */
    //public function getProtected();

    /**
     * Get cellular
     *
     * @return string
     */
    public function getCellular();

    /*
     * Get avrageLate
     *
     * @return integer
     */
    //public function getAvrageLate();

    /**
     * Get city
     *
     * @return string
     */
    public function getCity();

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty();

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry();

    /**
     * Get mailCity
     *
     * @return string
     */
    public function getMailCity();

    /**
     * Get mailCounty
     *
     * @return string
     */
    public function getMailCounty();

    /**
     * Get mailCountr
     *
     * @return string
     */
    public function getMailCountry();

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Get eMail
     *
     * @return string
     */
    public function getE_Mail();

    /*
     * Get picture
     *
     * @return string
     */
    //public function getPicture();

    /*
     * Get dflAccount
     *
     * @return string
     */
    //public function getDflAccount();

    /*
     * Get dflBranch
     *
     * @return string
     */
    //public function getDflBranch();

    /*
     * Get bankCode
     *
     * @return string
     */
    //public function getBankCode();

    /*
     * Get addID
     *
     * @return string
     */
    //public function getAddID();

    /*
     * Get pager
     *
     * @return string
     */
    //public function getPager();

    /*
     * Get fatherCard
     *
     * @return string
     */
    //public function getFatherCard();

    /*
     * Get cardFName
     *
     * @return string
     */
    //public function getCardFName();

    /*
     * Get fatherType
     *
     * @return string
     */
    //public function getFatherType();

    /*
     * Get ddctOffice
     *
     * @return string
     */
    //public function getDdctOffice();

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate();

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate();

    /*
     * Get exportCode
     *
     * @return string
     */
    //public function getExportCode();

    /*
     * Get dscntObjct
     *
     * @return integer
     */
    //public function getDscntObjct();

    /*
     * Get dscntRel
     *
     * @return string
     */
    //public function getDscntRel();

    /*
     * Get sPGCounter
     *
     * @return integer
     */
    //public function getSPGCounter();

    /*
     * Get sPPCounter
     *
     * @return integer
     */
    //public function getSPPCounter();

    /*
     * Get ddctFileNo
     *
     * @return string
     */
    //public function getDdctFileNo();

    /*
     * Get sCNCounter
     *
     * @return integer
     */
    //public function getSCNCounter();

    /*
     * Get minIntrst
     *
     * @return string
     */
    //public function getMinIntrst();

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource();

    /*
     * Get oprCount
     *
     * @return integer
     */
    //public function getOprCount();

    /*
     * Get exemptNo
     *
     * @return string
     */
    //public function getExemptNo();

    /*
     * Get priority
     *
     * @return integer
     */
    //public function getPriority();

    /*
     * Get creditCard
     *
     * @return integer
     */
    //public function getCreditCard();

    /*
     * Get crCardNum
     *
     * @return string
     */
    //public function getCrCardNum();

    /*
     * Get cardValid
     *
     * @return \DateTime
     */
    //public function getCardValid();

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign();

    /*
     * Get locMth
     *
     * @return string
     */
    //public function getLocMth();

    /**
     * Get validFor
     *
     * @return string
     */
    public function getValidFor();

    /*
     * Get validFrom
     *
     * @return \DateTime
     */
    //public function getValidFrom();

    /*
     * Get validTo
     *
     * @return \DateTime
     */
    //public function getValidTo();

    /**
     * Get frozenFor
     *
     * @return string
     */
    public function getFrozenFor();

    /*
     * Get frozenFrom
     *
     * @return \DateTime
     */
    //public function getFrozenFrom();

    /*
     * Get frozenTo
     *
     * @return \DateTime
     */
    //public function getFrozenTo();

    /*
     * Get sEmployed
     *
     * @return string
     */
    //public function getSEmployed();

    /*
     * Get mTHCounter
     *
     * @return integer
     */
    //public function getMTHCounter();

    /*
     * Get bNKCounter
     *
     * @return integer
     */
    //public function getBNKCounter();

    /*
     * Get ddgKey
     *
     * @return integer
     */
    //public function getDdgKey();

    /*
     * Get ddtKey
     *
     * @return integer
     */
    //public function getDdtKey();

    /*
     * Get validComm
     *
     * @return string
     */
    //public function getValidComm();

    /**
     * Get frozenComm
     *
     * @return string
     */
    public function getFrozenComm();

    /*
     * Get chainStore
     *
     * @return string
     */
    //public function getChainStore();

    /*
     * Get discInRet
     *
     * @return string
     */
    //public function getDiscInRet();

    /**
     * Get state1
     *
     * @return string
     */
    public function getState1();

    /**
     * Get state2
     *
     * @return string
     */
    public function getState2();

    /*
     * Get vatGroup
     *
     * @return string
     */
    //public function getVatGroup();

    /*
     * Get logInstanc
     *
     * @return integer
     */
    //public function getLogInstanc();

    /*
     * Get objType
     *
     * @return string
     */
    //public function getObjType();

    /*
     * Get indicator
     *
     * @return string
     */
    //public function getIndicator();

    /*
     * Get shipType
     *
     * @return integer
     */
    //public function getShipType();

    /*
     * Get debPayAcct
     *
     * @return string
     */
    //public function getDebPayAcct();

    /**
     * Get shipToDef
     *
     * @return string
     */
    public function getShipToDef();

    /**
     * Get block
     *
     * @return string
     */
    public function getBlock();

    /**
     * Get mailBlock
     *
     * @return string
     */
    public function getMailBlock();

    /*
     * Get password
     *
     * @return string
     */
    //public function getPassword();

    /*
     * Get eCVatGroup
     *
     * @return string
     */
    //public function getECVatGroup();

    /*
     * Get deleted
     *
     * @return string
     */
    //public function getDeleted();

    /*
     * Get iBAN
     *
     * @return string
     */
    //public function getIBAN();

    /*
     * Get docEntry
     *
     * @return integer
     */
    //public function getDocEntry();

    /*
     * Get formCode
     *
     * @return integer
     */
    //public function getFormCode();

    /*
     * Get box1099
     *
     * @return string
     */
    //public function getBox1099();

    /**
     * Get pymCode
     *
     * @return string
     */
    public function getPaymentMethod();

    /*
     * Get backOrder
     *
     * @return string
     */
    //public function getBackOrder();

    /*
     * Get partDelivr
     *
     * @return string
     */
    //public function getPartDelivr();

    /*
     * Get dunnLevel
     *
     * @return integer
     */
    //public function getDunnLevel();

    /*
     * Get dunnDate
     *
     * @return \DateTime
     */
    //public function getDunnDate();

    /*
     * Get blockDunn
     *
     * @return string
     */
    //public function getBlockDunn();

    /*
     * Get bankCountr
     *
     * @return string
     */
    //public function getBankCountr();

    /*
     * Get collecAuth
     *
     * @return string
     */
    //public function getCollecAuth();

    /*
     * Get dME
     *
     * @return string
     */
    //public function getDME();

    /*
     * Get instrucKey
     *
     * @return string
     */
    //public function getInstrucKey();

    /*
     * Get singlePaym
     *
     * @return string
     */
    //public function getSinglePaym();

    /*
     * Get iSRBillId
     *
     * @return string
     */
    //public function getISRBillId();

    /*
     * Get paymBlock
     *
     * @return string
     */
    //public function getPaymBlock();

    /*
     * Get refDetails
     *
     * @return string
     */
    //public function getRefDetails();

    /**
     * Get houseBank
     *
     * @return string
     */
    public function getHouseBank();

    /*
     * Get ownerIdNum
     *
     * @return string
     */
    //public function getOwnerIdNum();

    /*
     * Get pyBlckDesc
     *
     * @return integer
     */
    //public function getPyBlckDesc();

    /*
     * Get housBnkCry
     *
     * @return string
     */
    //public function getHousBnkCry();

    /*
     * Get housBnkAct
     *
     * @return string
     */
    //public function getHousBnkAct();

    /*
     * Get housBnkBrn
     *
     * @return string
     */
    //public function getHousBnkBrn();

    /*
     * Get projectCod
     *
     * @return string
     */
    //public function getProjectCod();

    /*
     * Get sysMatchNo
     *
     * @return integer
     */
    //public function getSysMatchNo();

    /*
     * Get vatIdUnCmp
     *
     * @return string
     */
    //public function getVatIdUnCmp();

    /*
     * Get agentCode
     *
     * @return string
     */
    //public function getAgentCode();

    /*
     * Get tolrncDays
     *
     * @return integer
     */
    //public function getTolrncDays();

    /*
     * Get selfInvoic
     *
     * @return string
     */
    //public function getSelfInvoic();

    /*
     * Get deferrTax
     *
     * @return string
     */
    //public function getDeferrTax();

    /*
     * Get letterNum
     *
     * @return string
     */
    //public function getLetterNum();

    /*
     * Get maxAmount
     *
     * @return string
     */
    //public function getMaxAmount();

    /*
     * Get fromDate
     *
     * @return \DateTime
     */
    //public function getFromDate();

    /*
     * Get toDate
     *
     * @return \DateTime
     */
    //public function getToDate();

    /*
     * Get wTLiable
     *
     * @return string
     */
    //public function getWTLiable();

    /*
     * Get crtfcateNO
     *
     * @return string
     */
    //public function getCrtfcateNO();

    /*
     * Get expireDate
     *
     * @return \DateTime
     */
    //public function getExpireDate();

    /*
     * Get nINum
     *
     * @return string
     */
    //public function getNINum();

    /*
     * Get accCritria
     *
     * @return string
     */
    //public function getAccCritria();

    /*
     * Get wTCode
     *
     * @return string
     */
    //public function getWTCode();

    /*
     * Get equ
     *
     * @return string
     */
    //public function getEqu();

    /*
     * Get hldCode
     *
     * @return string
     */
    //public function getHldCode();

    /*
     * Get connBP
     *
     * @return string
     */
    //public function getConnBP();

    /*
     * Get mltMthNum
     *
     * @return integer
     */
    //public function getMltMthNum();

    /*
     * Get typWTReprt
     *
     * @return string
     */
    //public function getTypWTReprt();

    /*
     * Get vATRegNum
     *
     * @return string
     */
    //public function getVATRegNum();

    /*
     * Get repName
     *
     * @return string
     */
    //public function getRepName();

    /*
     * Get industry
     *
     * @return string
     */
    //public function getIndustry();

    /*
     * Get business
     *
     * @return string
     */
    //public function getBusiness();

    /*
     * Get wTTaxCat
     *
     * @return string
     */
    //public function getWTTaxCat();

    /*
     * Get isDomestic
     *
     * @return string
     */
    //public function getIsDomestic();

    /*
     * Get isResident
     *
     * @return string
     */
    //public function getIsResident();

    /*
     * Get autoCalBCG
     *
     * @return string
     */
    //public function getAutoCalBCG();

    /*
     * Get otrCtlAcct
     *
     * @return string
     */
    //public function getOtrCtlAcct();

    /*
     * Get aliasName
     *
     * @return string
     */
    //public function getAliasName();

    /*
     * Get building
     *
     * @return string
     */
    //public function getBuilding();

    /*
     * Get mailBuildi
     *
     * @return string
     */
    //public function getMailBuildi();

    /*
     * Get boEPrsnt
     *
     * @return string
     */
    //public function getBoEPrsnt();

    /*
     * Get boEDiscnt
     *
     * @return string
     */
    //public function getBoEDiscnt();

    /*
     * Get boEOnClct
     *
     * @return string
     */
    //public function getBoEOnClct();

    /*
     * Get unpaidBoE
     *
     * @return string
     */
    //public function getUnpaidBoE();

    /*
     * Get iTWTCode
     *
     * @return string
     */
    //public function getITWTCode();

    /*
     * Get dunTerm
     *
     * @return string
     */
    //public function getDunTerm();

    /*
     * Get channlBP
     *
     * @return string
     */
    //public function getChannlBP();

    /*
     * Get dfTcnician
     *
     * @return integer
     */
    //public function getDfTcnician();

    /*
     * Get territory
     *
     * @return integer
     */
    //public function getTerritory();

    /**
     * Get billToDef
     *
     * @return string
     */
    public function getBillToDef();

    /*
     * Get dpmClear
     *
     * @return string
     */
    //public function getDpmClear();

    /**
     * Get intrntSite
     *
     * @return string
     */
    public function getIntrntSite();

    /*
     * Get langCode
     *
     * @return integer
     */
    //public function getLangCode();

    /*
     * Get housActKey
     *
     * @return integer
     */
    //public function getHousActKey();

    /*
     * Get profession
     *
     * @return string
     */
    //public function getProfession();

    /*
     * Get cDPNum
     *
     * @return integer
     */
    //public function getCDPNum();

    /*
     * Get dflBankKey
     *
     * @return integer
     */
    //public function getDflBankKey();

    /*
     * Get bCACode
     *
     * @return string
     */
    //public function getBCACode();

    /*
     * Get useShpdGd
     *
     * @return string
     */
    //public function getUseShpdGd();

    /*
     * Get regNum
     *
     * @return string
     */
    //public function getRegNum();

    /*
     * Get verifNum
     *
     * @return string
     */
    //public function getVerifNum();

    /*
     * Get bankCtlKey
     *
     * @return string
     */
    //public function getBankCtlKey();

    /*
     * Get housCtlKey
     *
     * @return string
     */
    //public function getHousCtlKey();

    /*
     * Get addrType
     *
     * @return string
     */
    //public function getAddrType();

    /*
     * Get insurOp347
     *
     * @return string
     */
    //public function getInsurOp347();

    /*
     * Get mailAddrTy
     *
     * @return string
     */
    //public function getMailAddrTy();

    /*
     * Get streetNo
     *
     * @return string
     */
    //public function getStreetNo();

    /*
     * Get mailStrNo
     *
     * @return string
     */
    //public function getMailStrNo();

    /*
     * Get taxRndRule
     *
     * @return string
     */
    //public function getTaxRndRule();

    /*
     * Get vendTID
     *
     * @return integer
     */
    //public function getVendTID();

    /*
     * Get threshOver
     *
     * @return string
     */
    //public function getThreshOver();

    /*
     * Get surOver
     *
     * @return string
     */
    //public function getSurOver();

    /*
     * Get vendorOcup
     *
     * @return string
     */
    //public function getVendorOcup();

    /*
     * Get opCode347
     *
     * @return string
     */
    //public function getOpCode347();

    /*
     * Get dpmIntAct
     *
     * @return string
     */
    //public function getDpmIntAct();

    /*
     * Get residenNum
     *
     * @return string
     */
    //public function getResidenNum();

    /*
     * Get userSign2
     *
     * @return integer
     */
    //public function getUserSign2();

    /*
     * Get plngGroup
     *
     * @return string
     */
    //public function getPlngGroup();

    /*
     * Get vatIDNum
     *
     * @return string
     */
    //public function getVatIDNum();

    /*
     * Get affiliate
     *
     * @return string
     */
    //public function getAffiliate();

    /*
     * Get mivzExpSts
     *
     * @return string
     */
    //public function getMivzExpSts();

    /*
     * Get hierchDdct
     *
     * @return string
     */
    //public function getHierchDdct();

    /*
     * Get certWHT
     *
     * @return string
     */
    //public function getCertWHT();

    /*
     * Get certBKeep
     *
     * @return string
     */
    //public function getCertBKeep();

    /*
     * Get wHShaamGrp
     *
     * @return string
     */
    //public function getWHShaamGrp();

    /*
     * Get industryC
     *
     * @return integer
     */
    //public function getIndustryC();

    /*
     * Get datevAcct
     *
     * @return integer
     */
    //public function getDatevAcct();

    /*
     * Get datevFirst
     *
     * @return string
     */
    //public function getDatevFirst();

    /*
     * Get gTSRegNum
     *
     * @return string
     */
    //public function getGTSRegNum();

    /*
     * Get gTSBankAct
     *
     * @return string
     */
    //public function getGTSBankAct();

    /*
     * Get gTSBilAddr
     *
     * @return string
     */
    //public function getGTSBilAddr();

    /*
     * Get hsBnkSwift
     *
     * @return string
     */
    //public function getHsBnkSwift();

    /*
     * Get hsBnkIBAN
     *
     * @return string
     */
    //public function getHsBnkIBAN();

    /*
     * Get dflSwift
     *
     * @return string
     */
    //public function getDflSwift();

    /*
     * Get autoPost
     *
     * @return string
     */
    //public function getAutoPost();

    /*
     * Get intrAcc
     *
     * @return string
     */
    //public function getIntrAcc();

    /*
     * Get feeAcc
     *
     * @return string
     */
    //public function getFeeAcc();

    /*
     * Get cpnNo
     *
     * @return integer
     */
    //public function getCpnNo();

    /*
     * Get nTSWebSite
     *
     * @return integer
     */
    //public function getNTSWebSite();

    /*
     * Get dflIBAN
     *
     * @return string
     */
    //public function getDflIBAN();

    /*
     * Get series
     *
     * @return integer
     */
    //public function getSeries();

    /*
     * Get number
     *
     * @return integer
     */
    //public function getNumber();

    /*
     * Get eDocExpFrm
     *
     * @return integer
     */
    //public function getEDocExpFrm();

    /*
     * Get taxIdIdent
     *
     * @return string
     */
    //public function getTaxIdIdent();

    /*
     * Get attachment
     *
     * @return string
     */
    //public function getAttachment();

    /*
     * Get atcEntry
     *
     * @return integer
     */
    //public function getAtcEntry();

    /*
     * Get discRel
     *
     * @return string
     */
    //public function getDiscRel();

    /*
     * Get noDiscount
     *
     * @return string
     */
    //public function getNoDiscount();

    /*
     * Get sCAdjust
     *
     * @return string
     */
    //public function getSCAdjust();

    /*
     * Get dflAgrmnt
     *
     * @return integer
     */
    //public function getDflAgrmnt();

    /*
     * Get glblLocNum
     *
     * @return string
     */
    //public function getGlblLocNum();

    /*
     * Get senderID
     *
     * @return string
     */
    //public function getSenderID();

    /*
     * Get rcpntID
     *
     * @return string
     */
    //public function getRcpntID();

    /*
     * Get mainUsage
     *
     * @return integer
     */
    //public function getMainUsage();

    /*
     * Get sefazCheck
     *
     * @return string
     */
    //public function getSefazCheck();

    /*
     * Get sefazReply
     *
     * @return string
     */
    //public function getSefazReply();

    /*
     * Get sefazDate
     *
     * @return \DateTime
     */
    //public function getSefazDate();

    /*
     * Get dateFrom
     *
     * @return \DateTime
     */
    //public function getDateFrom();

    /*
     * Get dateTill
     *
     * @return \DateTime
     */
    //public function getDateTill();

    /*
     * Get relCode
     *
     * @return string
     */
    //public function getRelCode();

    /*
     * Get oKATO
     *
     * @return string
     */
    //public function getOKATO();

    /*
     * Get oKTMO
     *
     * @return string
     */
    //public function getOKTMO();

    /*
     * Get kBKCode
     *
     * @return string
     */
    //public function getKBKCode();

    /*
     * Get typeOfOp
     *
     * @return string
     */
    //public function getTypeOfOp();

    /*
     * Get ownerCode
     *
     * @return integer
     */
    //public function getOwnerCode();

    /*
     * Get mandateID
     *
     * @return string
     */
    //public function getMandateID();

    /*
     * Get signDate
     *
     * @return \DateTime
     */
    //public function getSignDate();

    /*
     * Get remark1
     *
     * @return integer
     */
    //public function getRemark1();

    /*
     * Get conCerti
     *
     * @return string
     */
    //public function getConCerti();

    /*
     * Get tpCusPres
     *
     * @return integer
     */
    //public function getTpCusPres();

    /*
     * Get roleTypCod
     *
     * @return string
     */
    //public function getRoleTypCod();
}
