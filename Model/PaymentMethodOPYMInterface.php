<?php

namespace BetaMFD\SAPBundle\Model;

interface PaymentMethodOPYMInterface
{
    /**
     * Get the value of Pay Method Code
     *
     * @return string
     */
    public function getPayMethodCode();

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Get the value of Type
     *
     * @return string
     */
    public function getType();

    /**
     * Get the value of Bank Transf
     *
     * @return string
     */
    public function getBankTransf();

    /*
     * Get the value of Address
     *
     * @return string
     */
    //public function getAddress();

    /*
     * Get the value of Cank Det
     *
     * @return string
     */
    //public function getCankDet();

    /*
     * Get the value of Cllct Autor
     *
     * @return string
     */
    //public function getCllctAutor();

    /*
     * Get the value of Frgn Pmnt Bl
     *
     * @return string
     */
    //public function getFrgnPmntBl();

    /*
     * Get the value of Frgn Bnk Bl
     *
     * @return string
     */
    //public function getFrgnBnkBl();

    /*
     * Get the value of Curr Restr
     *
     * @return string
     */
    //public function getCurrRestr();

    /*
     * Get the value of Post Off Bnk
     *
     * @return string
     */
    //public function getPostOffBnk();

    /*
     * Get the value of Max Amount
     *
     * @return string
     */
    //public function getMaxAmount();

    /*
     * Get the value of Min Amount
     *
     * @return string
     */
    //public function getMinAmount();

    /**
     * Get the value of Bank
     *
     * @return string
     */
    public function getBank();

    /*
     * Get the value of User Sign
     *
     * @return integer
     */
    //public function getUserSign();

    /*
     * Get the value of Data Source
     *
     * @return string
     */
    //public function getDataSource();

    /**
     * Get the value of Create Date
     *
     * @return \DateTime
     */
    public function getCreateDate();

    /**
     * Get the value of Bank Country
     *
     * @return mixed
     */
    public function getBankCountry();

    /**
     * Get the value of Default Account
     *
     * @return string
     */
    public function getDefaultAccount();

    /**
     * Get the value of Gl Account
     *
     * @return string
     */
    public function getGlAccount();

    /**
     * Get the value of Branch
     *
     * @return string
     */
    public function getBranch();

    /*
     * Get the value of Key Code
     *
     * @return string
     */
    //public function getKeyCode();

    /*
     * Get the value of Trns Type
     *
     * @return string
     */
    //public function getTrnsType();

    /*
     * Get the value of Format
     *
     * @return string
     */
    //public function getFormat();

    /*
     * Get the value of Agt Collect
     *
     * @return string
     */
    //public function getAgtCollect();

    /*
     * Get the value of Send Accept
     *
     * @return string
     */
    //public function getSendAccept();

    /*
     * Get the value of Grp By Date
     *
     * @return string
     */
    //public function getGrpByDate();

    /*
     * Get the value of Dep Norm
     *
     * @return string
     */
    //public function getDepNorm();

    /*
     * Get the value of Debit Memo
     *
     * @return string
     */
    //public function getDebitMemo();

    /*
     * Get the value of Group Pm Ref
     *
     * @return string
     */
    //public function getGroupPmRef();

    /*
     * Get the value of Group Inv
     *
     * @return string
     */
    //public function getGroupInv();

    /*
     * Get the value of Val Date Sel
     *
     * @return string
     */
    //public function getValDateSel();

    /*
     * Get the value of Paym Terms
     *
     * @return integer
     */
    //public function getPaymTerms();

    /*
     * Get the value of Intrim Acct
     *
     * @return string
     */
    //public function getIntrimAcct();

    /*
     * Get the value of Bnk Act Key
     *
     * @return integer
     */
    //public function getBnkActKey();

    /*
     * Get the value of Doc Type
     *
     * @return string
     */
    //public function getDocType();

    /*
     * Get the value of Accepted
     *
     * @return string
     */
    //public function getAccepted();

    /*
     * Get the value of Ptf
     *
     * @return string
     */
    //public function getPtfID();

    /*
     * Get the value of Ptf Code
     *
     * @return string
     */
    //public function getPtfCode();

    /*
     * Get the value of Ptf Num
     *
     * @return string
     */
    //public function getPtfNum();

    /*
     * Get the value of Cur Code
     *
     * @return string
     */
    //public function getCurCode();

    /*
     * Get the value of Instruct
     *
     * @return string
     */
    //public function getInstruct1();

    /*
     * Get the value of Instruct
     *
     * @return string
     */
    //public function getInstruct2();

    /*
     * Get the value of Paymnt Plc
     *
     * @return string
     */
    //public function getPaymntPlc();

    /*
     * Get the value of Boe Dll
     *
     * @return string
     */
    //public function getBoeDll();

    /*
     * Get the value of Bank Ctl Key
     *
     * @return string
     */
    //public function getBankCtlKey();

    /*
     * Get the value of Bcg Pcnt
     *
     * @return string
     */
    //public function getBcgPcnt();

    /**
     * Get the value of Active
     *
     * @return string
     */
    public function getActive();

    /*
     * Get the value of Grp By Bank
     *
     * @return string
     */
    //public function getGrpByBank();

    /*
     * Get the value of Grp By Cur
     *
     * @return string
     */
    //public function getGrpByCur();

    /*
     * Get the value of Dfl
     *
     * @return string
     */
    //public function getDflIBAN();

    /*
     * Get the value of Dfl Swift
     *
     * @return string
     */
    //public function getDflSwift();

    /*
     * Get the value of Boe Report
     *
     * @return string
     */
    //public function getBoeReport();

    /*
     * Get the value of Canc Instr
     *
     * @return string
     */
    //public function getCancInstr();

    /*
     * Get the value of Occur Code
     *
     * @return string
     */
    //public function getOccurCode();

    /*
     * Get the value of Movmnt Code
     *
     * @return string
     */
    //public function getMovmntCode();

    /*
     * Get the value of Neg Pym Code
     *
     * @return string
     */
    //public function getNegPymCode();

    /*
     * Get the value of Direct Dbt
     *
     * @return string
     */
    //public function getDirectDbt();

    /*
     * Get the value of Issue Indic
     *
     * @return string
     */
    //public function getIssueIndic();

    /*
     * Get the value of Print
     *
     * @return string
     */
    //public function getPrintSEPA();
}
