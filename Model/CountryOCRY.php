<?php

namespace BetaMFD\SAPBundle\Model;

use BetaMFD\SAPBundle\Model\CountryOCRYInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * OCRY
 *
 * @ORM\Table(name="OCRY")
 * @ORM\Entity(readOnly=true)
 */
abstract class CountryOCRY implements CountryOCRYInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="Code", type="string", length=3)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=100, nullable=true)
     */
    protected $name;

    /*
     * @var integer
     *
     * @ORM\Column(name="AddrFormat", type="integer", nullable=true)
     */
    //protected $addrFormat;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign", type="integer", nullable=true)
     */
    //protected $userSign;

    /*
     * @var string
     *
     * @ORM\Column(name="IsEC", type="string", length=1, nullable=true)
     */
    //protected $isEC;

    /*
     * @var string
     *
     * @ORM\Column(name="ReportCode", type="string", length=3, nullable=true)
     */
    //protected $reportCode;

    /*
     * @var integer
     *
     * @ORM\Column(name="TaxIdDigts", type="integer", nullable=true)
     */
    //protected $taxIdDigts;

    /*
     * @var integer
     *
     * @ORM\Column(name="BnkCodDgts", type="integer", nullable=true)
     */
    //protected $bnkCodDgts;

    /*
     * @var integer
     *
     * @ORM\Column(name="BnkBchDgts", type="integer", nullable=true)
     */
    //protected $bnkBchDgts;

    /*
     * @var integer
     *
     * @ORM\Column(name="BnkActDgts", type="integer", nullable=true)
     */
    //protected $bnkActDgts;

    /*
     * @var integer
     *
     * @ORM\Column(name="BnkCtKDgts", type="integer", nullable=true)
     */
    //protected $bnkCtKDgts;

    /*
     * @var string
     *
     * @ORM\Column(name="ValDomAcct", type="string", length=3, nullable=true)
     */
    //protected $valDomAcct;

    /*
     * @var string
     *
     * @ORM\Column(name="ValIban", type="string", length=1, nullable=true)
     */
    //protected $valIban;

    /*
     * @var string
     *
     * @ORM\Column(name="IsBlackLst", type="string", length=1, nullable=true)
     */
    //protected $isBlackLst;

    /*
     * @var string
     *
     * @ORM\Column(name="UICCode", type="string", length=3, nullable=true)
     */
    //protected $uICCode;

    /*
     * @var string
     *
     * @ORM\Column(name="CntCodNum", type="string", length=4, nullable=true)
     */
    //protected $cntCodNum;

    /*
     * @var string
     *
     * @ORM\Column(name="Siscomex", type="string", length=3, nullable=true)
     */
    //protected $siscomex;


    /**
     * Constructor
     */
    protected function __construct() {}

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /*
     * Get addrFormat
     *
     * @return integer
     */
    //public function getAddrFormat()
    //{
    //    return $this->addrFormat;
    //}

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign()
    //{
    //    return $this->userSign;
    //}

    /*
     * Get isEC
     *
     * @return string
     */
    //public function getIsEC()
    //{
    //    return $this->isEC;
    //}

    /*
     * Get reportCode
     *
     * @return string
     */
    //public function getReportCode()
    //{
    //    return $this->reportCode;
    //}

    /*
     * Get taxIdDigts
     *
     * @return integer
     */
    //public function getTaxIdDigts()
    //{
    //    return $this->taxIdDigts;
    //}

    /*
     * Get bnkCodDgts
     *
     * @return integer
     */
    //public function getBnkCodDgts()
    //{
    //    return $this->bnkCodDgts;
    //}

    /*
     * Get bnkBchDgts
     *
     * @return integer
     */
    //public function getBnkBchDgts()
    //{
    //    return $this->bnkBchDgts;
    //}

    /*
     * Get bnkActDgts
     *
     * @return integer
     */
    //public function getBnkActDgts()
    //{
    //    return $this->bnkActDgts;
    //}

    /*
     * Get bnkCtKDgts
     *
     * @return integer
     */
    //public function getBnkCtKDgts()
    //{
    //    return $this->bnkCtKDgts;
    //}

    /*
     * Get valDomAcct
     *
     * @return string
     */
    //public function getValDomAcct()
    //{
    //    return $this->valDomAcct;
    //}

    /*
     * Get valIban
     *
     * @return string
     */
    //public function getValIban()
    //{
    //    return $this->valIban;
    //}

    /*
     * Get isBlackLst
     *
     * @return string
     */
    //public function getIsBlackLst()
    //{
    //    return $this->isBlackLst;
    //}

    /*
     * Get uICCode
     *
     * @return string
     */
    //public function getUICCode()
    //{
    //    return $this->uICCode;
    //}

    /*
     * Get cntCodNum
     *
     * @return string
     */
    //public function getCntCodNum()
    //{
    //    return $this->cntCodNum;
    //}

    /*
     * Get siscomex
     *
     * @return string
     */
    //public function getSiscomex()
    //{
    //    return $this->siscomex;
    //}

}
