<?php

namespace BetaMFD\SAPBundle\Model;

use Doctrine\ORM\Mapping as ORM;


interface CountryOCRYInterface
{
    public function __toString();

    /**
     * Get code
     *
     * @return string
     */
    public function getCode();

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /*
     * Get addrFormat
     *
     * @return integer
     */
    //public function getAddrFormat();

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign();

    /*
     * Get isEC
     *
     * @return string
     */
    //public function getIsEC();

    /*
     * Get reportCode
     *
     * @return string
     */
    //public function getReportCode();

    /*
     * Get taxIdDigts
     *
     * @return integer
     */
    //public function getTaxIdDigts();

    /*
     * Get bnkCodDgts
     *
     * @return integer
     */
    //public function getBnkCodDgts();

    /*
     * Get bnkBchDgts
     *
     * @return integer
     */
    //public function getBnkBchDgts();

    /*
     * Get bnkActDgts
     *
     * @return integer
     */
    //public function getBnkActDgts();

    /*
     * Get bnkCtKDgts
     *
     * @return integer
     */
    //public function getBnkCtKDgts();

    /*
     * Get valDomAcct
     *
     * @return string
     */
    //public function getValDomAcct();

    /*
     * Get valIban
     *
     * @return string
     */
    //public function getValIban();

    /*
     * Get isBlackLst
     *
     * @return string
     */
    //public function getIsBlackLst();

    /*
     * Get uICCode
     *
     * @return string
     */
    //public function getUICCode();

    /*
     * Get cntCodNum
     *
     * @return string
     */
    //public function getCntCodNum();

    /*
     * Get siscomex
     *
     * @return string
     */
    //public function getSiscomex();

}
