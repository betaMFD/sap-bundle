<?php

namespace BetaMFD\SAPBundle\Model;


interface CurrencyOCRNInterface
{
    public function __toString();

    /**
     * Get currCode
     *
     * @return string
     */
    public function getCurrCode();

    /**
     * Get currName
     *
     * @return string
     */
    public function getCurrName();

    /*
     * Get chkName
     *
     * @return string
     */
    //public function getChkName();

    /*
     * Get chk100Name
     *
     * @return string
     */
    //public function getChk100Name();

    /*
     * Get docCurrCod
     *
     * @return string
     */
    //public function getDocCurrCod();

    /*
     * Get frgnName
     *
     * @return string
     */
    //public function getFrgnName();

    /*
     * Get f100Name
     *
     * @return string
     */
    //public function getF100Name();

    /*
     * Get locked
     *
     * @return string
     */
    //public function getLocked();

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource();

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign();

    /*
     * Get roundSys
     *
     * @return integer
     */
    //public function getRoundSys();

    /*
     * Get userSign2
     *
     * @return integer
     */
    //public function getUserSign2();

    /*
     * Get decimals
     *
     * @return integer
     */
    //public function getDecimals();

    /*
     * Get iSRCalc
     *
     * @return string
     */
    //public function getISRCalc();

    /*
     * Get roundPym
     *
     * @return string
     */
    //public function getRoundPym();

    /*
     * Get convUnit
     *
     * @return string
     */
    //public function getConvUnit();

    /*
     * Get baseCurr
     *
     * @return string
     */
    //public function getBaseCurr();

    /*
     * Get factor
     *
     * @return string
     */
    //public function getFactor();

    /*
     * Get chkNamePl
     *
     * @return string
     */
    //public function getChkNamePl();

    /*
     * Get chk100NPl
     *
     * @return string
     */
    //public function getChk100NPl();

    /*
     * Get frgnNamePl
     *
     * @return string
     */
    //public function getFrgnNamePl();

    /*
     * Get f100NamePl
     *
     * @return string
     */
    //public function getF100NamePl();

    /*
     * Get iSOCurrCod
     *
     * @return string
     */
    //public function getISOCurrCod();

    /*
     * Get maxInDiff
     *
     * @return string
     */
    //public function getMaxInDiff();

    /*
     * Get maxOutDiff
     *
     * @return string
     */
    //public function getMaxOutDiff();

    /*
     * Get maxInPcnt
     *
     * @return string
     */
    //public function getMaxInPcnt();

    /*
     * Get maxOutPcnt
     *
     * @return string
     */
    //public function getMaxOutPcnt();

    /*
     * Get iSOCurrNum
     *
     * @return string
     */
    //public function getISOCurrNum();
}
