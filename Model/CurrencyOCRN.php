<?php

namespace BetaMFD\SAPBundle\Model;

use BetaMFD\SAPBundle\Model\CurrencyOCRNInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * CurrencyOCRN
 *
 * @ORM\Table(name="OCRN")
 * @ORM\Entity(readOnly=true)
 */
abstract class CurrencyOCRN implements CurrencyOCRNInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="CurrCode", type="string", length=3)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $currCode;

    /**
     * @var string
     *
     * @ORM\Column(name="CurrName", type="string", length=20, nullable=true)
     */
    protected $currName;

    /*
     * @var string
     *
     * @ORM\Column(name="ChkName", type="string", length=20, nullable=true)
     */
    //protected $chkName;

    /*
     * @var string
     *
     * @ORM\Column(name="Chk100Name", type="string", length=20, nullable=true)
     */
    //protected $chk100Name;

    /*
     * @var string
     *
     * @ORM\Column(name="DocCurrCod", type="string", length=3, nullable=true)
     */
    //protected $docCurrCod;

    /*
     * @var string
     *
     * @ORM\Column(name="FrgnName", type="string", length=20, nullable=true)
     */
    //protected $frgnName;

    /*
     * @var string
     *
     * @ORM\Column(name="F100Name", type="string", length=20, nullable=true)
     */
    //sprotected $f100Name;

    /*
     * @var string
     *
     * @ORM\Column(name="Locked", type="string", length=1, nullable=true)
     */
    //protected $locked;

    /*
     * @var string
     *
     * @ORM\Column(name="DataSource", type="string", length=1, nullable=true)
     */
    //protected $dataSource;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign", type="integer", nullable=true)
     */
    //protected $userSign;

    /*
     * @var integer
     *
     * @ORM\Column(name="RoundSys", type="integer", nullable=true)
     */
    //protected $roundSys;

    /*
     * @var integer
     *
     * @ORM\Column(name="UserSign2", type="integer", nullable=true)
     */
    //protected $userSign2;

    /*
     * @var integer
     *
     * @ORM\Column(name="Decimals", type="integer", nullable=true)
     */
    //protected $decimals;

    /*
     * @var string
     *
     * @ORM\Column(name="ISRCalc", type="string", length=1, nullable=true)
     */
    //protected $iSRCalc;

    /*
     * @var string
     *
     * @ORM\Column(name="RoundPym", type="string", length=1, nullable=true)
     */
    //protected $roundPym;

    /*
     * @var string
     *
     * @ORM\Column(name="ConvUnit", type="string", length=1, nullable=true)
     */
    //protected $convUnit;

    /*
     * @var string
     *
     * @ORM\Column(name="BaseCurr", type="string", length=3, nullable=true)
     */
    //protected $baseCurr;

    /*
     * @var string
     *
     * @ORM\Column(name="Factor", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $factor;

    /*
     * @var string
     *
     * @ORM\Column(name="ChkNamePl", type="string", length=20, nullable=true)
     */
    //protected $chkNamePl;

    /*
     * @var string
     *
     * @ORM\Column(name="Chk100NPl", type="string", length=20, nullable=true)
     */
    //protected $chk100NPl;

    /*
     * @var string
     *
     * @ORM\Column(name="FrgnNamePl", type="string", length=20, nullable=true)
     */
    //protected $frgnNamePl;

    /*
     * @var string
     *
     * @ORM\Column(name="F100NamePl", type="string", length=20, nullable=true)
     */
    //protected $f100NamePl;

    /*
     * @var string
     *
     * @ORM\Column(name="ISOCurrCod", type="string", length=3, nullable=true)
     */
    //protected $iSOCurrCod;

    /*
     * @var string
     *
     * @ORM\Column(name="MaxInDiff", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $maxInDiff;

    /*
     * @var string
     *
     * @ORM\Column(name="MaxOutDiff", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $maxOutDiff;

    /*
     * @var string
     *
     * @ORM\Column(name="MaxInPcnt", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $maxInPcnt;

    /*
     * @var string
     *
     * @ORM\Column(name="MaxOutPcnt", type="decimal", precision=10, scale=6, nullable=true)
     */
    //protected $maxOutPcnt;

    /*
     * @var string
     *
     * @ORM\Column(name="ISOCurrNum", type="string", length=3, nullable=true)
     */
    //protected $iSOCurrNum;



    /**
     * Constructor
     */
    protected function __construct() {}

    public function __toString()
    {
        return $this->currName;
    }

    /**
     * Get currCode
     *
     * @return string
     */
    public function getCurrCode()
    {
        return $this->currCode;
    }

    /**
     * Get currName
     *
     * @return string
     */
    public function getCurrName()
    {
        return $this->currName;
    }

    /*
     * Get chkName
     *
     * @return string
     */
    //public function getChkName()
    //{
    //    return $this->chkName;
    //}

    /*
     * Get chk100Name
     *
     * @return string
     */
    //public function getChk100Name()
    //{
    //    return $this->chk100Name;
    //}

    /*
     * Get docCurrCod
     *
     * @return string
     */
    //public function getDocCurrCod()
    //{
    //    return $this->docCurrCod;
    //}

    /*
     * Get frgnName
     *
     * @return string
     */
    //public function getFrgnName()
    //{
    //    return $this->frgnName;
    //}

    /*
     * Get f100Name
     *
     * @return string
     */
    //public function getF100Name()
    //{
    //    return $this->f100Name;
    //}

    /*
     * Get locked
     *
     * @return string
     */
    //public function getLocked()
    //{
    //    return $this->locked;
    //}

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource()
    //{
    //    return $this->dataSource;
    //}

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign()
    //{
    //    return $this->userSign;
    //}

    /*
     * Get roundSys
     *
     * @return integer
     */
    //public function getRoundSys()
    //{
    //    return $this->roundSys;
    //}

    /*
     * Get userSign2
     *
     * @return integer
     */
    //public function getUserSign2()
    //{
    //    return $this->userSign2;
    //}

    /*
     * Get decimals
     *
     * @return integer
     */
    //public function getDecimals()
    //{
    //    return $this->decimals;
    //}

    /*
     * Get iSRCalc
     *
     * @return string
     */
    //public function getISRCalc()
    //{
    //    return $this->iSRCalc;
    //}

    /*
     * Get roundPym
     *
     * @return string
     */
    //public function getRoundPym()
    //{
    //    return $this->roundPym;
    //}

    /*
     * Get convUnit
     *
     * @return string
     */
    //public function getConvUnit()
    //{
    //    return $this->convUnit;
    //}

    /*
     * Get baseCurr
     *
     * @return string
     */
    //public function getBaseCurr()
    //{
    //    return $this->baseCurr;
    //}

    /*
     * Get factor
     *
     * @return string
     */
    //public function getFactor()
    //{
    //    return $this->factor;
    //}

    /*
     * Get chkNamePl
     *
     * @return string
     */
    //public function getChkNamePl()
    //{
    //    return $this->chkNamePl;
    //}

    /*
     * Get chk100NPl
     *
     * @return string
     */
    //public function getChk100NPl()
    //{
    //    return $this->chk100NPl;
    //}

    /*
     * Get frgnNamePl
     *
     * @return string
     */
    //public function getFrgnNamePl()
    //{
    //    return $this->frgnNamePl;
    //}

    /*
     * Get f100NamePl
     *
     * @return string
     */
    //public function getF100NamePl()
    //{
    //    return $this->f100NamePl;
    //}

    /*
     * Get iSOCurrCod
     *
     * @return string
     */
    //public function getISOCurrCod()
    //{
    //    return $this->iSOCurrCod;
    //}

    /*
     * Get maxInDiff
     *
     * @return string
     */
    //public function getMaxInDiff()
    //{
    //    return $this->maxInDiff;
    //}

    /*
     * Get maxOutDiff
     *
     * @return string
     */
    //public function getMaxOutDiff()
    //{
    //    return $this->maxOutDiff;
    //}

    /*
     * Get maxInPcnt
     *
     * @return string
     */
    //public function getMaxInPcnt()
    //{
    //    return $this->maxInPcnt;
    //}

    /*
     * Get maxOutPcnt
     *
     * @return string
     */
    //public function getMaxOutPcnt()
    //{
    //    return $this->maxOutPcnt;
    //}

    /*
     * Get iSOCurrNum
     *
     * @return string
     */
    //public function getISOCurrNum()
    //{
    //    return $this->iSOCurrNum;
    //}
}
