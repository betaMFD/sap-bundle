<?php

namespace BetaMFD\SAPBundle\Model;


interface SalesPersonOSLPInterface
{
    public function __toString();

    /**
     * Get slpCode
     *
     * @return integer
     */
    public function getSlpCode();

    /**
     * Get slpName
     *
     * @return string
     */
    public function getSlpName();

    /**
     * Get slpName
     *
     * @return string
     */
    public function getName();

    /*
     * Get memo
     *
     * @return string
     */
    //public function getMemo();

    /*
     * Get commission
     *
     * @return string
     */
    //public function getCommission();

    /*
     * Get groupCode
     *
     * @return integer
     */
    //public function getGroupCode();

    /*
     * Get locked
     *
     * @return string
     */
    //public function getLocked();

    /*
     * Get dataSource
     *
     * @return string
     */
    //public function getDataSource();

    /*
     * Get userSign
     *
     * @return integer
     */
    //public function getUserSign();

    /*
     * Get empID
     *
     * @return integer
     */
    //public function getEmpID();

    /*
     * Get active
     *
     * @return string
     */
    //public function getActive();

    /*
     * Get telephone
     *
     * @return string
     */
    //public function getTelephone();

    /*
     * Get mobil
     *
     * @return string
     */
    //public function getMobile();

    /*
     * Get fax
     *
     * @return string
     */
    //public function getFax();

    /*
     * Get email
     *
     * @return string
     */
    //public function getEmail();
}
