<?php

namespace BetaMFD\SAPBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BetaMFDSAPBundle extends Bundle
{
}
